<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Brian2694\Toastr\Facades\Toastr;

class CheckManagerAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::user()) {
            return abort(404);
        }
        if (!checkAdministratorRole(Auth::user())) {
            Toastr::error(trans('lang.admin_permission'));
            return redirect(route('showHome',['lang' => app()->getLocale() ]));
        }

        return $next($request);
    }
}
