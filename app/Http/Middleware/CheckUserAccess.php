<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Brian2694\Toastr\Facades\Toastr;
class CheckUserAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()) {
            if (Auth::user()->status == 3){
                Auth::logout();
                Toastr::error(trans('lang.account_banned'));
                return redirect(route('showHome',['lang' => app()->getLocale() ]));
            } elseif(Auth::user()->status == 0){
                Auth::logout();
                Toastr::error(trans('lang.check_email_validation'));
                return redirect(route('showHome',['lang' => app()->getLocale() ]));
            }

        } else {
            Toastr::error(trans('lang.login_first'));
            return redirect(route('showHome',['lang' => app()->getLocale() ]));
        }

        return $next($request);
    }
}
