<?php

namespace App\Http\Middleware;

use App\Modules\User\Models\User;
use Closure;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Route;

class LanguageHandler
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */

    const ignoredRouteNames = [
        'handleUserCheckInvoiceNaming',
        'handleUserCheckEstimateNaming',
        'handleUserCheckExpenseNaming',
        'handleUserCheckOrderNaming',
        'handleUserCheckPurchasePaymentNaming',
        'handleUserCheckSalePaymentNaming',
        'handleSocialRedirect',
        'handleSlackRedirect',
        'handleSocialCallback'
    ];

    public function handle($request, Closure $next)
    {
        $currentRouteParams = $request->route()->originalParameters();

        if (!in_array(Route::currentRouteName(), self::ignoredRouteNames)) {
            if (isset($currentRouteParams['lang']) and in_array($currentRouteParams['lang'], array_keys(Config::get('app.languages')))) {
                App::setLocale($currentRouteParams['lang']);
                Cookie::queue('applocale', $currentRouteParams['lang']);

                if (Auth::user() and Auth::user()->language != $currentRouteParams['lang']) {
                        $user = User::find(Auth::id());
                        $user->language = $currentRouteParams['lang'];
                        $user->save();
                }

            } else {

                if (Auth::user() and in_array(Auth::user()->language, array_keys(Config::get('app.languages')))) {
                    $locale = Auth::user()->language;
                } elseif (Cookie::has('applocale')) {
                    $locale = Cookie::get('applocale');
                } else {
                    $locale = Config::get('app.locale');
                }

                $currentRouteParams = $request->route()->originalParameters();
                $currentRouteParams['lang'] = $locale;

                return redirect(route($request->route()->getName(), $currentRouteParams));
            }
        }

        return $next($request);
    }
}
