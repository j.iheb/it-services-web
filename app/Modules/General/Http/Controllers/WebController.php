<?php

namespace App\Modules\General\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\General\Models\Contact;
use App\Modules\General\Models\Service;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class WebController extends Controller
{
 
    public function showHome($lang)
    {
        return view('General::frontOffice.home',[
            'lang' => $lang
        ]); 
    }




    public function showManagerHome($lang)
    { 

     //   Toastr::success('hello'); 

        return view('General::backOffice.home', [
            'lang' => $lang
        ]); 

    }


    public function showManagerContact($lang)
    {
        return view('General::backOffice.contacts', [
            'lang' => $lang, 
            'contacts' => Contact::all(), 
        ]); 
    }

    public function showUserContact($lang)
    {
        return view('General::frontOffice.contact',[
            'lang' => $lang, 
        ]); 
    }

    public function handleUserAddContact($lang , Request $request)
    {
         $data = $request->all(); 


         $rules = [
             'name' => 'required', 
             'email' => 'required', 
             'subject' => 'required', 
             'content' => 'required',
         ]; 

         $messages = [
             'name.required' => ucfirst(trans('lang.required', ['string' => trans('lang.name')])),
             'email.required' => ucfirst(trans('lang.required', ['string' => trans('lang.email')])),
             'subject.required' => ucfirst(trans('lang.required', ['string' => trans('lang.description')])),
             'content.required' => ucfirst(trans('lang.required', ['string' => trans('lang.content')])),
         ] ; 


         $contact = Contact::create([
             'name' => $data['name'], 
             'email' => $data['email'], 
             'content' => $data['content'], 
             'subject' => $data['subject'], 
         ]); 
             
         Toastr::success(trans('lang.operation_success')); 
         return redirect()->route('showHome', ['lang' => $lang]); 

    }

    public function showManagerServices($lang)
    {
        return view('General::backOffice.services',[
            'services' => Service::all(), 
            'lang' => $lang, 
        ]); 
    }

    public function showManagerAddService($lang){
        return view('General::backOffice.service', [
            'lang' => $lang
        ]); 
    }


    public function handleManagerAddService($lang, Request $request)
    {
        $data = $request->all(); 

        $rules = [
            'title' => 'required',
            'content' => 'required', 
        ]; 

        $messages = [
            'title.required' => ucfirst(trans('lang.required',['string' => trans('lang.title')])), 
            'content.required' => ucfirst(trans('lang.required', ['string' => trans('lang.content')])), 
        ] ; 


        $validations = Validator::make($data, $rules , $messages) ; 


        if ($validations->fails())
        {
            return back()->withErrors($validations->errors()); 
        }

        $service = Service::create([
            'title' => $data['title'], 
            'content' => $data['content'], 
        ]); 

        Toastr::success(ucfirst(trans('lang.operation_success'))); 
        return redirect()->route('showManagerServices', ['lang' => $lang]); 
    }

    public function showManagerEditService($lang, $id)
    {
        return view('General::backOffice.service',[
            'lang' => $lang , 
            'service' => Service::find($id), 
        ]); 
    }

    public function handleManagerUpdateService($lang, $id, Request $request)
    {
        $data = $request->all(); 

        $rules = [
            'title' => 'required',
            'content' => 'required', 
        ]; 

        $messages = [
            'title.required' => ucfirst(trans('lang.required',['string' => trans('lang.title')])), 
            'content.required' => ucfirst(trans('lang.required', ['string' => trans('lang.content')])), 
        ] ; 


        $validations = Validator::make($data, $rules , $messages) ; 


        if ($validations->fails())
        {
            return back()->withErrors($validations->errors()); 
        }

        $service = Service::find($id); 


        if ($service)
        {
            $service->title = $data['title']; 
            $service->content = $data['content']; 
            $service->update(); 

        }
        

   
        Toastr::success(ucfirst(trans('lang.operation_success'))); 
        return redirect()->route('showManagerServices', ['lang' => $lang]); 
    }


    public function handleManagerDeleteService($lang, $id)
    {
        Service::find($id) -> delete(); 

        Toastr::success(ucfirst(trans('lang.operation_success'))); 
        return back(); 

    }
    
}
