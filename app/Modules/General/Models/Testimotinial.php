<?php

namespace App\Modules\General\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Testimotinial extends Model
{
   
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'testimotinial';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
   
        'content', 
        'link',
        'lang',
        'photo'
   
   
    ];

}
