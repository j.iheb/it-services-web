<?php

namespace App\Modules\General\Models;

use Illuminate\Database\Eloquent\Model;
use Vinkla\Hashids\Facades\Hashids;

class Country extends Model
{

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'countries';

    protected $appends = ['hashed_id', 'hashed_currency_id'];

    public function getHashedIdAttribute()
    {
        return $this->attributes['hashed_id'] = Hashids::encodeHex($this->attributes['id']);
    }

    public function getHashedCurrencyIdAttribute()
    {
        return $this->attributes['hashed_currency_id'] = Hashids::encodeHex($this->attributes['currency_id']);
    }

    protected $hidden = [
        'id', 'currency_id'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'status',
        'iso_02',
        'iso_03',
        'iso_on',
        'currency_id',
        'title_ar'
    ];
    public function currency()
    {
        return $this->belongsTo('App\Modules\Invoice\Models\Currency', 'currency_id','id');
    }

}
