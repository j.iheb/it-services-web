<?php

use Illuminate\Support\Facades\Route; 

Route::group(['module' => 'General', 'middleware' => ['web'], 'prefix' => '{lang}'], function () {


Route::get('/', 'WebController@showHome')->name('showHome');
Route::get('/manager', 'WebController@showManagerHome')->name('showManagerHome')->middleware('auth', 'manager'); 


//contact 
Route::get('/manager/contacts', 'WebController@showManagerContact')->name('showManagerContact')->middleware('auth', 'manager'); 
Route::get('/contact', 'WebController@showUserContact')->name('showUserContact'); 
Route::post('/contact/add', 'WebController@handleUserAddContact')->name('handleUserAddContact'); 


//services  

Route::get('/manager/services', 'WebController@showManagerServices')->name('showManagerServices'); 
Route::get('/manager/service/add', 'WebController@showManagerAddService')->name('showManagerAddService'); 

Route::post('/manager/service/store', 'WebController@handleManagerAddService')->name('handleManagerAddService'); 
Route::get('/manager/service/{id}/edit', 'WebController@showManagerEditService')->name('showManagerEditService'); 


Route::post('/manager/services/update/{id}', 'WebController@handleManagerUpdateService')->name('handleManagerUpdateService'); 
Route::get('/manager/service/{id}/delete', 'WebController@handleManagerDeleteService')->name('handleManagerDeleteService'); 










});


