<!DOCTYPE html>
<html @if($lang =='ar') lang="ar" dir="rtl" @else  lang="zxx" @endif>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    @if($lang =='ar')
      <link rel="stylesheet" href="https://templates.envytheme.com/axolot/rtl/assets/css/bootstrap.rtl.min.css" />
    @else
      <link rel="stylesheet" href="https://templates.envytheme.com/axolot/rtl/assets/css/bootstrap.min.css" />
     @endif
    <link rel="stylesheet" href="https://templates.envytheme.com/axolot/rtl/assets/css/animate.css" />
    <link rel="stylesheet" href="https://templates.envytheme.com/axolot/rtl/assets/css/icofont.min.css" />
    <link rel="stylesheet" href="{{asset('css/icofont/icofont.min.css')}}">
    <link rel="stylesheet" href="https://templates.envytheme.com/axolot/rtl/assets/css/owl.carousel.css" />
    <link rel="stylesheet" href="https://templates.envytheme.com/axolot/rtl/assets/css/owl.theme.default.min.css" />
    <link rel="stylesheet" href="https://templates.envytheme.com/axolot/rtl/assets/css/magnific-popup.css" />
    <link rel="stylesheet" href="https://templates.envytheme.com/axolot/rtl/assets/css/style.css" />
    <link rel="stylesheet" href="https://templates.envytheme.com/axolot/rtl/assets/css/responsive.css" />
    <link rel="stylesheet" href="https://templates.envytheme.com/axolot/rtl/assets/css/color/color-default.css" />
    <link rel="stylesheet" href="https://templates.envytheme.com/axolot/rtl/assets/dist/color-switcher.css" />
    <link rel="stylesheet" href="https://templates.envytheme.com/axolot/rtl/assets/css/rtl.css" />
    <title>Axolot - Startup, SaaS & Software Landing Page Template</title>
    <link rel="icon" type="image/png" href="https://templates.envytheme.com/axolot/rtl/assets/img/favicon.png" />
</head>

<body data-bs-spy="scroll" data-bs-offset="70" id="home">
    <div class="preloader">
        <div class="loader">
            <div class="spinner">
                <div class="double-bounce1"></div>
                <div class="double-bounce2"></div>
            </div>
        </div>
    </div>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
            <a class="navbar-brand" href="index.html">lo<span>go</span></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ms-auto">
                    <li class="nav-item"><a class="nav-link active" href="#home">{{ucfirst(trans('lang.home'))}}</a></li>
                
                    <li class="nav-item"><a class="nav-link" href="#about">{{ucfirst(trans('lang.about_us'))}}</a></li>
                    <li class="nav-item"><a class="nav-link" href="#services">{{ucfirst(trans('lang.services'))}}</a></li>
                    <li class="nav-item"><a class="nav-link" href="#team">{{ucfirst(trans('lang.team'))}}</a></li>
                    <li class="nav-item"><a class="nav-link" href="#faq">{{ucfirst(trans('lang.faq'))}}</a></li>
                
                    <li class="nav-item"><a class="nav-link" href="#">{{ucfirst(trans('lang.contact_us'))}}</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div id="home" class="main-banner bg-gray">
        <div class="pattern-2"></div>
        <div class="bg-top"></div>
        <div class="bg-bottom"></div>
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="row h-100 align-items-center">
                        <div class="col-lg-6 col-md-6">
                            <div class="main-banner-content">
                                <h1>{{ucfirst(trans('lang.home_text'))}}</h1>
                                <p>{{ucfirst(trans('lang.home_text_des'))}}</p>
                                <a href="#about" class="btn btn-primary">{{ucfirst(trans('lang.contact_us'))}}</a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="banner-img wow fadeInUp"><img src="https://templates.envytheme.com/axolot/rtl/assets/img/main-banner.png" alt="img" /></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pattern"></div>
    </div>

    <section id="about" class="about-area ptb-100">
        <div class="container">
            <div class="section-title">
                <span>{{ucfirst(trans('lang.about_us'))}}</span>
                <h3>{{ucfirst(trans('lang.about_text'))}}</h3>
                {{--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>--}}
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="about-content">
                        <h3> {{ucfirst(trans('lang.about_h1'))}} </h3>
                        <p>
                        تعتبر خدمة الصيانة من أكثر الخدامت المطلوبة بشتعل بها أفضل الخبرات بأكثبر من 10 سنوات  من التفاني و الإخلاص
                        </p>
                        <ul class="pull-left">
                            <li><i class="icofont-ui-check"></i>  الصيانة</li>
                            <li><i class="icofont-ui-check"></i>  الشبكات</li>
                            <li><i class="icofont-ui-check"></i>   أمن المعلومات </li>
                            <li><i class="icofont-ui-check"></i> الدعم الفني</li>
                        </ul>
                        <ul>
                        <li><i class="icofont-ui-check"></i>  الصيانة</li>
                            <li><i class="icofont-ui-check"></i>  الشبكات</li>
                            <li><i class="icofont-ui-check"></i>   أمن المعلومات </li>
                            <li><i class="icofont-ui-check"></i> الدعم الفني</li>
                        </ul>
                        <a href="#" class="btn btn-primary"> أطلب خدمة الصيانة</a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="about-img wow fadeInUp"><img src="https://templates.envytheme.com/axolot/rtl/assets/img/about.png" alt="about" /></div>
                </div>
            </div>
            <div class="row mt-100">
                <div class="col-lg-6 col-md-6">
                    <div class="img wow fadeInUp"><img src="https://templates.envytheme.com/axolot/rtl/assets/img/1.png" alt="img" /></div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="about-text mb-0">
                        <span>.01</span>
                        <h3>إبداء العمل معنا</h3>
                        <p>
                        تعتبر خدمة الصيانة من أكثر الخدامت المطلوبة بشتعل بها أفضل الخبرات بأكثبر من 10 سنوات  من التفاني و الإخلاص
                        </p>
                        <ul>
                        <li><i class="icofont-ui-check"></i>  الصيانة</li>
                            <li><i class="icofont-ui-check"></i>  الشبكات</li>
                            <li><i class="icofont-ui-check"></i>   أمن المعلومات </li>
                            <li><i class="icofont-ui-check"></i> الدعم الفني</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row mt-100">
                <div class="col-lg-6 col-md-6">
                    <div class="about-text mt-0">
                        <span>.02</span>
                        <h3>حل مشاكلك بسرعة من أجل تواصل أكثر</h3>
                        <p>
                        تعتبر خدمة الصيانة من أكثر الخدامت المطلوبة بشتعل بها أفضل الخبرات بأكثبر من 10 سنوات  من التفاني و الإخلاص
                        </p>
                        <ul>
                        <li><i class="icofont-ui-check"></i>  الصيانة</li>
                            <li><i class="icofont-ui-check"></i>  الشبكات</li>
                            <li><i class="icofont-ui-check"></i>   أمن المعلومات </li>
                            <li><i class="icofont-ui-check"></i> الدعم الفني</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="img wow fadeInUp"><img src="https://templates.envytheme.com/axolot/rtl/assets/img/2.png" alt="img" /></div>
                </div>
            </div>
            <div class="row mt-100">
                <div class="col-lg-6 col-md-6">
                    <div class="img wow fadeInUp"><img src="https://templates.envytheme.com/axolot/rtl/assets/img/3.png" alt="img" /></div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="about-text mb-0">
                        <span>.03</span>
                        <h3> جميع الخدمات في مقدمة من شركة واحدة </h3>
                        <p>
                        تعتبر خدمة الصيانة من أكثر الخدامت المطلوبة بشتعل بها أفضل الخبرات بأكثبر من 10 سنوات  من التفاني و الإخلاص
                        </p>
                        <blockquote class="blockquote">
                            <p class="mb-0">تعتبر خدمة الصيانة من أكثر الخدامت المطلوبة بشتعل بها أفضل الخبرات بأكثبر من 10 سنوات  من التفاني و الإخلاص</p>
                        </blockquote>
                    </div>
                </div>
            </div>
            <div class="row mt-100">
                <div class="col-lg-6 col-md-6">
                    <div class="about-text mt-0">
                        <span>.04</span>
                        <h3> نوفر أفضل الكفائات من أجلكم</h3>
                        <p>
                        تعتبر خدمة الصيانة من أكثر الخدامت المطلوبة بشتعل بها أفضل الخبرات بأكثبر من 10 سنوات  من التفاني و الإخلاص
                        </p>
                        <blockquote class="blockquote">
                            <p class="mb-0">تعتبر خدمة الصيانة من أكثر الخدامت المطلوبة بشتعل بها أفضل الخبرات بأكثبر من 10 سنوات  من التفاني و الإخلاص</p>
                        </blockquote>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="img wow fadeInUp"><img src="https://templates.envytheme.com/axolot/rtl/assets/img/4.png" alt="img" /></div>
                </div>
            </div>
        </div>
    </section>
    <section id="services" class="services-area bg-gray ptb-100">
        <div class="container">
            <div class="section-title">
                <span>خدمات</span>
                <h3> نوفر أفضل الخدمات </h3>
                <p>تمتع بأكثر من 30 خدمة دات جودة عالية</p>
            </div>
            <div class="row">
                <div class="services-slides owl-carousel owl-theme">
                    <div class="col-lg-12 col-md-12">
                        <div class="single-services">
                            <i class="icofont-ruler-pencil"></i>
                            <h3>صيانة الشبكات</h3>
                            <p> تعتبر خدمة الصيانة من أكثر الخدامت المطلوبة بشتعل بها أفضل الخبرات بأكثبر من 10 سنوات  من التفاني و الإخلاص</p>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12">
                        <div class="single-services">
                            <i class="icofont-laptop-alt"></i>
                            <h3>صيانة الحواسيب</h3>
                            <p> تعتبر خدمة الصيانة من أكثر الخدامت المطلوبة بشتعل بها أفضل الخبرات بأكثبر من 10 سنوات  من التفاني و الإخلاص</p>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12">
                        <div class="single-services">
                            <i class="icofont-brand-designfloat"></i>
                            <h3>صيامة الطوابع الإلكترونية</h3>
                            <p> تعتبر خدمة الصيانة من أكثر الخدامت المطلوبة بشتعل بها أفضل الخبرات بأكثبر من 10 سنوات  من التفاني و الإخلاص</p>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12">
                        <div class="single-services">
                            <i class="icofont-ssl-security"></i>
                            <h3> حماية المعلومات</h3>
                            <p> تعتبر خدمة الصيانة من أكثر الخدامت المطلوبة بشتعل بها أفضل الخبرات بأكثبر من 10 سنوات  من التفاني و الإخلاص</p>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12">
                        <div class="single-services">
                            <i class="icofont-globe-alt"></i>
                            <h3> التسويق الإلكتروني</h3>
                            <p> تعتبر خدمة الصيانة من أكثر الخدامت المطلوبة بشتعل بها أفضل الخبرات بأكثبر من 10 سنوات  من التفاني و الإخلاص</p>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12">
                        <div class="single-services">
                            <i class="icofont-letterbox"></i>
                            <h3> تصميم المنتجات </h3>
                            <p> تعتبر خدمة الصيانة من أكثر الخدامت المطلوبة بشتعل بها أفضل الخبرات بأكثبر من 10 سنوات  من التفاني و الإخلاص</p>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12">
                        <div class="single-services">
                            <i class="icofont-support"></i>
                            <h3>الدعم الفني</h3>
                            <p> تعتبر خدمة الصيانة من أكثر الخدامت المطلوبة بشتعل بها أفضل الخبرات بأكثبر من 10 سنوات  من التفاني و الإخلاص</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="vision-area ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="tab">
                        <ul class="tabs">
                            <li>
                                <a href="#">
                                    <i class="icofont-laptop"></i><br />
                                    صيانة
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="icofont-ruler-pencil-alt-2"></i><br />
                                    حماية المعلومات
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="icofont-marker-alt-1"></i><br />
                                    التسويق الإلكتروني
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="icofont-light-bulb"></i><br />
                                    تطوير 
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="icofont-laptop-alt"></i><br />
                                    دعم قني
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="icofont-infant-nipple"></i><br />
                                    تسويق
                                </a>
                            </li>
                        </ul>
                        <div class="tab_content">
                            <div class="tabs_item">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="tabs_item_content mt-0">
                                            <h3>الصيانة</h3>
                                            <p>
                                               نقدم صيانة بطريقة دورية و دعم متواصل و سرعة في التواجد على عين المكان من أجل تواصل نمو شركتكم 
                                            </p>
                                            <ul>
                                                <li><i class="icofont-ui-check"></i> صيانة الحواسيب</li>
                                                <li><i class="icofont-ui-check"></i> صيانة الشبكات</li>
                                                <li><i class="icofont-ui-check"></i> أمن المعاومات</li>
                                                <li><i class="icofont-ui-check"></i> تغيير المعدات</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="tabs_item_img"><img src="https://templates.envytheme.com/axolot/rtl/assets/img/1.png" alt="img" /></div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs_item">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="tabs_item_img"><img src="https://templates.envytheme.com/axolot/rtl/assets/img/2.png" alt="img" /></div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="tabs_item_content mb-0">
                                            <h3>Product Design</h3>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                                                laboris nisi ut aliquip ex ea commodo.
                                            </p>
                                            <ul>
                                                <li><i class="icofont-ui-check"></i> Creative Design</li>
                                                <li><i class="icofont-ui-check"></i> Retina Ready</li>
                                                <li><i class="icofont-ui-check"></i> Modern Design</li>
                                                <li><i class="icofont-ui-check"></i> Digital Marketing & Branding</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs_item">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="tabs_item_content mt-0">
                                            <h3>Digital Marketing</h3>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                                                laboris nisi ut aliquip ex ea commodo.
                                            </p>
                                            <ul>
                                                <li><i class="icofont-ui-check"></i> Creative Design</li>
                                                <li><i class="icofont-ui-check"></i> Retina Ready</li>
                                                <li><i class="icofont-ui-check"></i> Modern Design</li>
                                                <li><i class="icofont-ui-check"></i> Digital Marketing & Branding</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="tabs_item_img"><img src="https://templates.envytheme.com/axolot/rtl/assets/img/3.png" alt="img" /></div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs_item">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="tabs_item_img"><img src="https://templates.envytheme.com/axolot/rtl/assets/img/1.png" alt="img" /></div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="tabs_item_content mb-0">
                                            <h3>Branding</h3>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                                                laboris nisi ut aliquip ex ea commodo.
                                            </p>
                                            <ul>
                                                <li><i class="icofont-ui-check"></i> Creative Design</li>
                                                <li><i class="icofont-ui-check"></i> Retina Ready</li>
                                                <li><i class="icofont-ui-check"></i> Modern Design</li>
                                                <li><i class="icofont-ui-check"></i> Digital Marketing & Branding</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs_item">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="tabs_item_content mt-0">
                                            <h3>Development</h3>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                                                laboris nisi ut aliquip ex ea commodo.
                                            </p>
                                            <ul>
                                                <li><i class="icofont-ui-check"></i> Creative Design</li>
                                                <li><i class="icofont-ui-check"></i> Retina Ready</li>
                                                <li><i class="icofont-ui-check"></i> Modern Design</li>
                                                <li><i class="icofont-ui-check"></i> Digital Marketing & Branding</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="tabs_item_img"><img src="https://templates.envytheme.com/axolot/rtl/assets/img/2.png" alt="img" /></div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs_item">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="tabs_item_img"><img src="https://templates.envytheme.com/axolot/rtl/assets/img/4.png" alt="img" /></div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="tabs_item_content">
                                            <h3>Marketing</h3>
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                                                laboris nisi ut aliquip ex ea commodo.
                                            </p>
                                            <ul>
                                                <li><i class="icofont-ui-check"></i> Creative Design</li>
                                                <li><i class="icofont-ui-check"></i> Retina Ready</li>
                                                <li><i class="icofont-ui-check"></i> Modern Design</li>
                                                <li><i class="icofont-ui-check"></i> Digital Marketing & Branding</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="funfacts-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <div class="funFact">
                        <i class="icofont-users-alt-5"></i>
                        <h3><span class="count">30</span></h3>
                        <p>خدمة</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3">
                    <div class="funFact">
                        <i class="icofont-download"></i>
                        <h3><span class="count">50</span>K</h3>
                        <p>زيارة عمل</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3">
                    <div class="funFact">
                        <i class="icofont-search-document"></i>
                        <h3><span class="count">80</span></h3>
                        <p>عامل</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3">
                    <div class="funFact">
                        <i class="icofont-star-shape"></i>
                        <h3><span class="count">5.00</span></h3>
                        <p>معدل التقييم</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="join-area ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="join-img wow fadeInUp"><img src="https://templates.envytheme.com/axolot/rtl/assets/img/3.png" alt="img" /></div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="join-content">
                        <h3>إنظم لأكثر من 500 زبون نتعامل معهم و نوفر لهم خدماتنا</h3>
                        <p>
                            لا تتردد في التزاصل و الحصول على حدماتنا و التي تتمثل في خدمات عن بهد و خدمات على أرض الواقع تقدم بكل حرفية و بأعلى سرعة ممكنة
                        </p>
                        <a href="#" class="btn btn-primary">أطلب الخدمة الأن</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="how-works-area bg-gray ptb-100">
        <div class="container">
            <div class="section-title">
                <span>المراحل</span>
                <h3>كيف يتم العمل</h3>
                <p> في هده النقاط الثلاث نعرض عليك مراحل العمل و التواصل معنا</p>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="single-box with-line">
                        <span>01.</span>
                        <h3>طلب الخدمة</h3>
                        <p> المرحلة الأولى تبدأ بتطلب الخدمة من خلال تعمير الفورم الخاص بطلب خدمة </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single-box with-line">
                        <span>02.</span>
                        <h3>النقاش</h3>
                        <p> يتم النقاش حول الخدمة بإعتبار الوقت اللازم و الزمن و السعر و تحديد كل تفاصيل الخدمة و يمكن دلك عبر الهاتف أو عبر الموقع.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 offset-lg-0 offset-md-3">
                    <div class="single-box">
                        <span>03.</span>
                        <h3>البدء بتقديم الخدمة</h3>
                        <p>فور الإنتهاء من النقاش نبداء في تقديم الخدمة </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="team" class="team-area ptb-100">
        <div class="container">
            <div class="section-title">
                <span>الفريق</span>
                <h3>فريق العمل</h3>
                <p>ينكون الفريق من أفضل الخبرات في مجالات متنوعة</p>
            </div>
            <div class="row">
                <div class="team-slides owl-carousel owl-theme">
                    <div class="col-lg-12 col-md-12">
                        <div class="our-team">
                            <div class="pic">
                                <img src="https://templates.envytheme.com/axolot/rtl/assets/img/team-1.jpg" alt="team" />
                                <ul class="social">
                                    <li>
                                        <a href="#"><i class="icofont-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="icofont-twitter"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="icofont-behance"></i></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="team-content">
                                <h3 class="title">محمد عبد الله</h3>
                                <span class="post">شبكات</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12">
                        <div class="our-team">
                            <div class="pic">
                                <img src="https://templates.envytheme.com/axolot/rtl/assets/img/team-2.jpg" alt="team" />
                                <ul class="social">
                                    <li>
                                        <a href="#"><i class="icofont-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="icofont-twitter"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="icofont-behance"></i></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="team-content">
                                <h3 class="title">عزيز</h3>
                                <span class="post">صيامة الحواسيب</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12">
                        <div class="our-team">
                            <div class="pic">
                                <img src="https://templates.envytheme.com/axolot/rtl/assets/img/team-3.jpg" alt="team" />
                                <ul class="social">
                                    <li>
                                        <a href="#"><i class="icofont-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="icofont-twitter"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="icofont-behance"></i></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="team-content">
                                <h3 class="title">هاشم</h3>
                                <span class="post">أمن المعلومات</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12">
                        <div class="our-team">
                            <div class="pic">
                                <img src="https://templates.envytheme.com/axolot/rtl/assets/img/team-4.jpg" alt="team" />
                                <ul class="social">
                                    <li>
                                        <a href="#"><i class="icofont-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="icofont-twitter"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="icofont-behance"></i></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="team-content">
                                <h3 class="title">أحمد</h3>
                                <span class="post">التسويق</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12">
                        <div class="our-team">
                            <div class="pic">
                                <img src="https://templates.envytheme.com/axolot/rtl/assets/img/team-5.jpg" alt="team" />
                                <ul class="social">
                                    <li>
                                        <a href="#"><i class="icofont-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="icofont-twitter"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="icofont-behance"></i></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="team-content">
                                <h3 class="title">طارق </h3>
                                <span class="post">الدعم الفني</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="testimonials-area bg-gray ptb-100">
        <div class="bg-top"></div>
        <div class="bg-bottom"></div>
        <div class="container">
            <div class="section-title">
                <span>قالو عنا</span>
                <h3>مادا قال عنا العملاء</h3>
                <p></p>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="testimonials-item">
                        <div class="client-info">
                            <div class="img wow fadeInUp"><img src="https://templates.envytheme.com/axolot/rtl/assets/img/client-avatar1.jpg" alt="client" /></div>
                            <div class="client-title">
                                <h4> عبد الرحمان</h4>
                                <span> مدير شركة </span>
                            </div>
                        </div>
                        <p> التعامل كان رائع جدا سرعة في التنقل و تقديم الخدمة شكرا لكم </p>
                        <i class="icofont-quote-left"></i>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="testimonials-item">
                        <div class="client-info">
                            <div class="img wow fadeInUp"><img src="https://templates.envytheme.com/axolot/rtl/assets/img/client-avatar1.jpg" alt="client" /></div>
                            <div class="client-title">
                                <h4> عبد الرحمان</h4>
                                <span> مدير شركة </span>
                            </div>
                        </div>
                        <p> التعامل كان رائع جدا سرعة في التنقل و تقديم الخدمة شكرا لكم </p>
                        <i class="icofont-quote-left"></i>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="testimonials-item">
                        <div class="client-info">
                            <div class="img wow fadeInUp"><img src="https://templates.envytheme.com/axolot/rtl/assets/img/client-avatar1.jpg" alt="client" /></div>
                            <div class="client-title">
                                <h4> عبد الرحمان</h4>
                                <span> مدير شركة </span>
                            </div>
                        </div>
                        <p> التعامل كان رائع جدا سرعة في التنقل و تقديم الخدمة شكرا لكم </p>
                        <i class="icofont-quote-left"></i>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="testimonials-item">
                        <div class="client-info">
                            <div class="img wow fadeInUp"><img src="https://templates.envytheme.com/axolot/rtl/assets/img/client-avatar1.jpg" alt="client" /></div>
                            <div class="client-title">
                                <h4> عبد الرحمان</h4>
                                <span> مدير شركة </span>
                            </div>
                        </div>
                        <p> التعامل كان رائع جدا سرعة في التنقل و تقديم الخدمة شكرا لكم </p>
                        <i class="icofont-quote-left"></i>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="testimonials-item">
                        <div class="client-info">
                            <div class="img wow fadeInUp"><img src="https://templates.envytheme.com/axolot/rtl/assets/img/client-avatar1.jpg" alt="client" /></div>
                            <div class="client-title">
                                <h4> عبد الرحمان</h4>
                                <span> مدير شركة </span>
                            </div>
                        </div>
                        <p> التعامل كان رائع جدا سرعة في التنقل و تقديم الخدمة شكرا لكم </p>
                        <i class="icofont-quote-left"></i>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="testimonials-item">
                        <div class="client-info">
                            <div class="img wow fadeInUp"><img src="https://templates.envytheme.com/axolot/rtl/assets/img/client-avatar1.jpg" alt="client" /></div>
                            <div class="client-title">
                                <h4> عبد الرحمان</h4>
                                <span> مدير شركة </span>
                            </div>
                        </div>
                        <p> التعامل كان رائع جدا سرعة في التنقل و تقديم الخدمة شكرا لكم </p>
                        <i class="icofont-quote-left"></i>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="faq" class="faq-area ptb-100">
        <div class="container">
            <div class="section-title">
                <span>الأسئلة الشائعة</span>
                <h3>الأسئلة ألأكثر تكرار </h3>
                <p>.</p>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="faq">
                        <ul class="accordion">
                            <li class="accordion-item">
                                <a class="accordion-title active" href="javascript:void(0)">كيف أحصل على خدمة الصيانة بعقد طويل</a>
                                <p class="accordion-content show">
                                    يمكنك الحصول على خدمة الصيانة من خلال الإتصال على رقم الواتساب أو الدخول على الرابط و طلب الخدمة عبر مراحل 
                                </p>
                            </li>
                            <li class="accordion-item">
                                <a class="accordion-title" href="javascript:void(0)">كيف أحصل على خدمة عن بعد</a>
                                <p class="accordion-content">يمكنك الحصول على خدمة الصيانة من خلال الإتصال على رقم الواتساب أو الدخول على الرابط و طلب الخدمة عبر مراحل </p>
                            </li>
                            <li class="accordion-item">
                                <a class="accordion-title" href="javascript:void(0)">كيف أحصل على خدمة عن بعد</a>
                                <p class="accordion-content">يمكنك الحصول على خدمة الصيانة من خلال الإتصال على رقم الواتساب أو الدخول على الرابط و طلب الخدمة عبر مراحل </p>
                            </li>
                            <li class="accordion-item">
                                <a class="accordion-title" href="javascript:void(0)">كيف أحصل على خدمة عن بعد</a>
                                <p class="accordion-content">يمكنك الحصول على خدمة الصيانة من خلال الإتصال على رقم الواتساب أو الدخول على الرابط و طلب الخدمة عبر مراحل </p>
                            </li>
                            <li class="accordion-item">
                                <a class="accordion-title" href="javascript:void(0)">كيف أحصل على خدمة عن بعد</a>
                                <p class="accordion-content">يمكنك الحصول على خدمة الصيانة من خلال الإتصال على رقم الواتساب أو الدخول على الرابط و طلب الخدمة عبر مراحل </p>
                            </li>
                            <li class="accordion-item">
                                <a class="accordion-title" href="javascript:void(0)">كيف أحصل على خدمة عن بعد</a>
                                <p class="accordion-content">يمكنك الحصول على خدمة الصيانة من خلال الإتصال على رقم الواتساب أو الدخول على الرابط و طلب الخدمة عبر مراحل </p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="faq-img wow fadeInUp"><img src="https://templates.envytheme.com/axolot/rtl/assets/img/about.png" alt="faq" /></div>
                </div>
            </div>
        </div>
    </section>

    <div class="partner-area ptb-100">
        <div class="container">
            <div class="row">
                <div class="partner-slides owl-carousel owl-theme">
                    <div class="col-lg-12 col-md-12">
                        <a href="#"><img src="https://templates.envytheme.com/axolot/rtl/assets/img/partner-1.jpg" alt="partner" /></a>
                    </div>
                    <div class="col-lg-12 col-md-12">
                        <a href="#"><img src="https://templates.envytheme.com/axolot/rtl/assets/img/partner-2.jpg" alt="partner" /></a>
                    </div>
                    <div class="col-lg-12 col-md-12">
                        <a href="#"><img src="https://templates.envytheme.com/axolot/rtl/assets/img/partner-3.jpg" alt="partner" /></a>
                    </div>
                    <div class="col-lg-12 col-md-12">
                        <a href="#"><img src="https://templates.envytheme.com/axolot/rtl/assets/img/partner-4.jpg" alt="partner" /></a>
                    </div>
                    <div class="col-lg-12 col-md-12">
                        <a href="#"><img src="https://templates.envytheme.com/axolot/rtl/assets/img/partner-5.jpg" alt="partner" /></a>
                    </div>
                    <div class="col-lg-12 col-md-12">
                        <a href="#"><img src="https://templates.envytheme.com/axolot/rtl/assets/img/partner-6.jpg" alt="partner" /></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer class="footer-area ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="single-footer">
                        <h4 class="logo">
                            <a href="#">lo<span>go</span></a>
                        </h4>
                        <p>نقدم أكثر من 30 خدمة  عن بعد و على الأرض الواقع  بسرعة كبيرة  و إتقان </p>
               
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-footer">
                        <h3>روابط</h3>
                        <ul>
                            <li>
                                <a href="#"><i class="icofont-double-right"></i> الرئيسية</a>
                            </li>
                       
                            <li>
                                <a href="#"><i class="icofont-double-right"></i> من نحن</a>
                            </li>
                            <li>
                                <a href="#"><i class="icofont-double-right"></i> خدماتنا</a>
                            </li>
                     
                            <li>
                                <a href="#"><i class="icofont-double-right"></i> إتصل بنا</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-footer">
                        <h3>الدعم</h3>
                        <ul>
                            <li>
                                <a href="#"><i class="icofont-double-right"></i> الدعم الفني</a>
                            </li>
                        
                            <li>
                                <a href="#"><i class="icofont-double-right"></i> الأسئلة الشائعة</a>
                            </li>
                            <li>
                                <a href="#"><i class="icofont-double-right"></i> إتصل بنا</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-footer">
                        <h3>معلومات التواصل</h3>
                        <p></p>
                        <ul class="contact-info">
                            <li><i class="icofont-google-map"></i> تونس </li>
                            <li><i class="icofont-phone"></i> + 21 241 545 845</li>
                            <li><i class="icofont-envelope">   </i> hello@gmail.com</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-md-7">
                    <p> حقوق الطبع والنشر <i class = "icofont-copyright"> </i> 2021 جميع الحقوق محفوظة. </p>
                    </div>
                    <div class="col-lg-5 col-md-5">
                        <ul>
                            <li><a href="#" class="icofont-facebook"></a></li>
                            <li><a href="#" class="icofont-twitter"></a></li>
                            <li><a href="#" class="icofont-instagram"></a></li>
                            <li><a href="#" class="icofont-linkedin"></a></li>
                            <li><a href="#" class="icofont-vimeo"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div class="go-top"><i class="icofont-stylish-up"></i></div>
    <script src="https://templates.envytheme.com/axolot/rtl/assets/js/jquery.min.js"></script>
    <script src="https://templates.envytheme.com/axolot/rtl/assets/js/popper.min.js"></script>
    <script src="https://templates.envytheme.com/axolot/rtl/assets/js/bootstrap.min.js"></script>
    <script src="https://templates.envytheme.com/axolot/rtl/assets/js/owl.carousel.min.js"></script>
    <script src="https://templates.envytheme.com/axolot/rtl/assets/js/jquery.magnific-popup.min.js"></script>
    <script src="https://templates.envytheme.com/axolot/rtl/assets/js/jquery.mixitup.min.js"></script>
    <script src="https://templates.envytheme.com/axolot/rtl/assets/js/wow.min.js"></script>
    <script src="https://templates.envytheme.com/axolot/rtl/assets/js/form-validator.min.js"></script>
    <script src="https://templates.envytheme.com/axolot/rtl/assets/js/contact-form-script.js"></script>
    <script src="https://templates.envytheme.com/axolot/rtl/assets/js/jquery.ajaxchimp.min.js"></script>
    <script src="https://templates.envytheme.com/axolot/rtl/assets/js/main.js"></script>

</body>

</html>