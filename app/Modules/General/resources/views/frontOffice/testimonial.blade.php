@extends('frontOffice.layout')

@section('head')
@include('frontOffice.inc.head')
@endsection

@section('header')
@include('frontOffice.inc.header')
@endsection



@section('content')


<section class="breadcrumb-section wow fadeIn">
    <div class="container">
        <div class="breadcrumb-wrap">
            <a href="{{route('showHome',['lang' => $lang])}}"> {{ucfirst(trans('lang.home'))}} </a>
            <i>/</i>
            <span>
                <span id="ctl00_ContentPlaceHolder1_lblTitle"> {{ucfirst(trans('lang.said_about_us'))}} </span></span>
        </div>
    </div>
</section>
<section class="inner-page-section wow fadeIn">
    <div class="container">
        <div class="section-title wow fadeInDown text-center" style="visibility: visible; animation-name: fadeInDown;">
            <h2>
                <span id="ctl00_ContentPlaceHolder1_lblTitle1">  {{ucfirst(trans('lang.said_about_us'))}}</span>
            </h2>
        </div>

        @foreach($sayes as $saye)
      <div>
        <div class="page-img text-center wow fadeInDown">
        @if($saye->link)
         <iframe src="{{$saye->link}}" allowfullscreen="allowfullscreen" frameborder="0" loading="lazy"  style="width:100%;" height="400px%"></iframe>
        @endif 
        @if($saye->photo)
        <img src="{{asset($saye->photo)}}"  style="width:100%;" height="400px%">
       
        @endif
        </div>
        <div class="page-text wow fadeInDown">
     
           {!! $saye->content !!}
        </div>

        <hr>
        </div>

        @endforeach
    </div>
</section>

@endsection