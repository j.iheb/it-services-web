@extends('frontOffice.layout')

@section('head')
@include('frontOffice.inc.head')
@endsection

@section('header')
@include('frontOffice.inc.header')
@endsection



@section('content')

<style>
        body {
            overflow-x: hidden;
        }

        h1 {
            color: #396;
            font-weight: 100;
            margin: 40px 0px 20px;
        }

        #clockdiv {
            font-family: sans-serif;
            color: #fff;
            display: inline-block;
            font-weight: 100;
            text-align: center;
            font-size: 30px;
        }

        #clockdiv>div {
            padding: 10px;
            border-radius: 3px;
            background: #00BF96;
            display: inline-block;
        }

        #clockdiv div>span {
            padding: 15px;
            border-radius: 3px;
            background: #00816A;
            display: inline-block;
        }

        .smalltext {
            padding-top: 5px;
            font-size: 16px;
        }
    </style>



<section class="breadcrumb-section wow fadeIn">
    <div class="container">
        <div class="breadcrumb-wrap">
            <a href="/"> {{ucfirst(trans('lang.home'))}} </a>
            <i>/</i>
            <span id="ctl00_ContentPlaceHolder1_lblTitle"> {{ucfirst(trans('lang.programs_and_offers'))}} </span>
        </div>
    </div>
</section>
<section class="inner-page-section wow fadeIn">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="single-blog-content">
                    <h1 class="title"> {{$programm->title}} </h1>
                    <div class="main-img text-center">
                        <img src="{{asset($programm->photo)}}" />
                    </div>
                    <div class="meta">
                        <div class="date-time">
                            <a href="#" class="item"><i class="fa fa-tags"></i><span>  {{ucfirst(trans('lang.programs_and_offers'))}}   </span></a>
                            <span class="item"><i class="fa fa-calendar-o"></i><span> {{$programm->created_at->format('Y-m-d')}}  </span></span>
                        </div>
                      

                    
                    </div>
                    <div class="blog-text">
                       {!! $programm->content !!}
                    </div>
                </div>
                <div class="text-center">
                
                    <div id="ctl00_ContentPlaceHolder1_offerTimer">
                        <div id="clockdiv" class="">
                            <div>
                                <span class="seconds"></span>
                                <div class="smalltext wow">ثانية</div>
                            </div>
                            <div>
                                <span class="minutes"></span>
                                <div class="smalltext wow">دقيقة</div>
                            </div>
                            <div>
                                <span class="hours"></span>
                                <div class="smalltext wow">ساعة</div>
                            </div>
                            <div>
                                <span class="days"></span>
                                <div class="smalltext wow">يوم</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="ctl00_ContentPlaceHolder1_Up">
                    <div class="text-center">
                        <br />
                        <br />
                        <!--<input type="submit" name="ctl00$ContentPlaceHolder1$btnGetOffers" value="الحصول على العرض" id="ctl00_ContentPlaceHolder1_btnGetOffers" class="contact-btn btn btn-bordered" />
                      -->  <h2 id="ctl00_ContentPlaceHolder1_lblMsg2" class="wow" style="color: forestgreen;"></h2>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="sidebar-wrapper">
                    <div class="sidebar-item">
                        <div class="sidebar-box">
                            <div class="sidebar-title wow fadeInUp">آخر العروض </div>
                            
                            @foreach($programms as $program)
                            <div class="small-news wow fadeInUp">
                                <a href="{{route('showUserProgramm',['lang' => $lang, 'id' => $program->id])}}" title=" {{$program->title}} ">
                                    <img src="{{asset($program->photo)}}" alt=" {{$program->title}} " title=" {{$program->title}} " />
                                    <span> {{$program->title}} </span>
                                </a>
                            </div>
                            @endforeach

                         
                        
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
</section>


<input type="hidden" name="ctl00$ContentPlaceHolder1$hdnTime" id="ctl00_ContentPlaceHolder1_hdnTime" value="{{$endDate}}" />


<script async src="https://static.addtoany.com/menu/page.js"></script>
<script>
        function getTimeRemaining(endtime) {
            var t = Date.parse(endtime) - Date.parse(new Date());
            var seconds = Math.floor((t / 1000) % 60);
            var minutes = Math.floor((t / 1000 / 60) % 60);
            var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
            var days = Math.floor(t / (1000 * 60 * 60 * 24));
            return {
                'total': t,
                'days': days,
                'hours': hours,
                'minutes': minutes,
                'seconds': seconds
            };
        }

        function initializeClock(id, endtime) {
            var clock = document.getElementById(id);
            var daysSpan = clock.querySelector('.days');
            var hoursSpan = clock.querySelector('.hours');
            var minutesSpan = clock.querySelector('.minutes');
            var secondsSpan = clock.querySelector('.seconds');

            function updateClock() {
                var t = getTimeRemaining(endtime);

                daysSpan.innerHTML = t.days;
                hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
                minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
                secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

                if (t.total <= 0) {
                    clearInterval(timeinterval);
                }
            }

            updateClock();
            var timeinterval = setInterval(updateClock, 1000);
        }

        var hdnTime = $("[id$=hdnTime]").val();
        var deadline = new Date(Date.parse(new Date(parseFloat(hdnTime))));
        initializeClock('clockdiv', deadline);



    </script>
<script>
        $(function () {
            $("[id$=TxtEmail]").change(function () {
                var curr = $(this);
                var val = $(this).val();
                var a = val.split(" "), i;
                for (i = 0; i < a.length; i++) {
                    curr.val($(this).val().replace(" ", ""));
                }
            });
        });
    </script>
<script>
        jQuery(document).ready(function () {

            if ($("[id$=hfPayent]").val() == "1") {
                $("[href=#tab_4]").click();
                if ($("[id$=hfMsg]").val() == "1") {
                    $("[id$=lblUpMsg]").text("تم رفع الصورة بنجاح");
                    $("[id$=lblUpMsg]").attr("style", "font-size: large; color: black; background: lightgreen; padding: 0px 10px 0px 10px;");
                }
                else {
                    $("[id$=lblUpMsg]").text("الرجاء اختيار نوع امتداد صحيح للصور مثل jpg,gif,png");
                    $("[id$=lblUpMsg]").attr("style", "font-size: large; color: black; background: red; padding: 0px 10px 0px 10px;");
                }
            }
        });
    </script>

@endsection