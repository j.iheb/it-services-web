<!DOCTYPE html>
<html @if($lang=='ar' ) lang="ar" dir="rtl" @else lang="zxx" @endif>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    @if($lang =='ar')
    <link rel="stylesheet" href="https://templates.envytheme.com/axolot/rtl/assets/css/bootstrap.rtl.min.css" />
    @else
    <link rel="stylesheet" href="https://templates.envytheme.com/axolot/rtl/assets/css/bootstrap.min.css" />
    @endif
    <link rel="stylesheet" href="https://templates.envytheme.com/axolot/rtl/assets/css/animate.css" />
    <link rel="stylesheet" href="https://templates.envytheme.com/axolot/rtl/assets/css/icofont.min.css" />
    <link rel="stylesheet" href="{{asset('css/icofont/icofont.min.css')}}">
    <link rel="stylesheet" href="https://templates.envytheme.com/axolot/rtl/assets/css/owl.carousel.css" />
    <link rel="stylesheet" href="https://templates.envytheme.com/axolot/rtl/assets/css/owl.theme.default.min.css" />
    <link rel="stylesheet" href="https://templates.envytheme.com/axolot/rtl/assets/css/magnific-popup.css" />
    <link rel="stylesheet" href="https://templates.envytheme.com/axolot/rtl/assets/css/style.css" />
    <link rel="stylesheet" href="https://templates.envytheme.com/axolot/rtl/assets/css/responsive.css" />
    <link rel="stylesheet" href="https://templates.envytheme.com/axolot/rtl/assets/css/color/color-default.css" />
    <link rel="stylesheet" href="https://templates.envytheme.com/axolot/rtl/assets/dist/color-switcher.css" />
    <link rel="stylesheet" href="https://templates.envytheme.com/axolot/rtl/assets/css/rtl.css" />
    <title>Axolot - Startup, SaaS & Software Landing Page Template</title>
    <link rel="icon" type="image/png" href="https://templates.envytheme.com/axolot/rtl/assets/img/favicon.png" />
</head>

<body data-bs-spy="scroll" data-bs-offset="70" id="home">
    <div class="preloader">
        <div class="loader">
            <div class="spinner">
                <div class="double-bounce1"></div>
                <div class="double-bounce2"></div>
            </div>
        </div>
    </div>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
            <a class="navbar-brand" href="index.html">lo<span>go</span></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ms-auto">
                    <li class="nav-item"><a class="nav-link active" href="#home">{{ucfirst(trans('lang.home'))}}</a></li>

                    <li class="nav-item"><a class="nav-link" href="#about">{{ucfirst(trans('lang.about_us'))}}</a></li>
                    <li class="nav-item"><a class="nav-link" href="#services">{{ucfirst(trans('lang.services'))}}</a></li>
                    <li class="nav-item"><a class="nav-link" href="#team">{{ucfirst(trans('lang.team'))}}</a></li>
                    <li class="nav-item"><a class="nav-link" href="#faq">{{ucfirst(trans('lang.faq'))}}</a></li>

                    <li class="nav-item"><a class="nav-link" href="#">{{ucfirst(trans('lang.contact_us'))}}</a></li>
                </ul>
            </div>
        </div>
    </nav>


    <div class="page-title">
        <div class="pattern-2"></div>
        <div class="bg-top"></div>
        <div class="bg-bottom"></div>
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="page-title-content">
                        <h3>{{ucfirst(trans('lang.contact_us'))}}</h3>
                        <ul>
                            <li><a href="index.html">{{ucfirst(trans('lang.home'))}}</a></li>
                            <li><i class="icofont-thin-right"></i></li>
                            <li>{{ucfirst(trans('lang.contact_us'))}}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <section class="contact-area bg-gray ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12">
                    <div class="contact-box">
                        <div class="icon">
                            <i class="icofont-phone"></i>
                        </div>
                        <div class="content">
                            <h4>{{ucfirst(trans('lang.phone'))}}</h4>
                            <p><a href="#">(+021) 245522455</a></p>
                            <p><a href="#">(+000) 245522455</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="contact-box">
                        <div class="icon">
                            <i class="icofont-envelope"></i>
                        </div>
                        <div class="content">
                            <h4>{{ucfirst(trans('lang.email'))}}</h4>
                            <p><a href="#"> hello@gmail.com  </a></p>
                            <p><a href="#"> hello@gmail.com </a></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="contact-box">
                        <div class="icon">
                            <i class="icofont-google-map"></i>
                        </div>
                        <div class="content">
                            <h4>{{ucfirst(trans('lang.location'))}}</h4>
                            <p>غزة <span>فلسطين</span></p>
                            <p>غزة <span>فلسطين</span></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12">
                    <div id="map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2646.8158792277713!2d-123.36243578415748!3d48.44087343796659!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x548f7381102ff519%3A0xc03e5586418eb3f2!2s2750%20Quadra%20St%2C%20Victoria%2C%20BC%20V8T%204E8%2C%20Canada!5e0!3m2!1sen!2sbd!4v1620711829369!5m2!1sen!2sbd"></iframe>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="contact-text">
                        <h3>{{ucfirst(trans('lang.contact_us'))}}</h3>
                        <p>  {{ucfirst(trans('lang.contact_text'))}} </p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <form  action="{{route('handleUserAddContact',['lang' => $lang])}}" method="POST">
                    @csrf
                        <div class="row">
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <input type="text" name="name" id="name" class="form-control" required data-error="Please enter your name" placeholder="{{ucfirst(trans('lang.name'))}}">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <input type="email" id="email" name="email" class="form-control" required data-error="Please enter your email" placeholder="{{ucfirst(trans('lang.email'))}}">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12">
                                <div class="form-group">
                                    <input type="text" name="subject" id="msg_subject" class="form-control" required placeholder="{{ucfirst(trans('lang.description'))}}">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12">
                                <div class="form-group">
                                    <textarea name="content" class="form-control" id="message" cols="30" rows="4" required data-error="Write your message" placeholder="{{ucfirst(trans('lang.content'))}}"></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12">
                                <button type="submit" class="btn btn-primary">{{ucfirst(trans('lang.send'))}}</button>
                            
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>





    <footer class="footer-area ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="single-footer">
                        <h4 class="logo">
                            <a href="#">lo<span>go</span></a>
                        </h4>
                        <p>نقدم أكثر من 30 خدمة عن بعد و على الأرض الواقع بسرعة كبيرة و إتقان </p>

                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-footer">
                        <h3>روابط</h3>
                        <ul>
                            <li>
                                <a href="#"><i class="icofont-double-right"></i> الرئيسية</a>
                            </li>

                            <li>
                                <a href="#"><i class="icofont-double-right"></i> من نحن</a>
                            </li>
                            <li>
                                <a href="#"><i class="icofont-double-right"></i> خدماتنا</a>
                            </li>

                            <li>
                                <a href="#"><i class="icofont-double-right"></i> إتصل بنا</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-footer">
                        <h3>الدعم</h3>
                        <ul>
                            <li>
                                <a href="#"><i class="icofont-double-right"></i> الدعم الفني</a>
                            </li>

                            <li>
                                <a href="#"><i class="icofont-double-right"></i> الأسئلة الشائعة</a>
                            </li>
                            <li>
                                <a href="#"><i class="icofont-double-right"></i> إتصل بنا</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="single-footer">
                        <h3>معلومات التواصل</h3>
                        <p></p>
                        <ul class="contact-info">
                            <li><i class="icofont-google-map"></i> تونس </li>
                            <li><i class="icofont-phone"></i> + 21 241 545 845</li>
                            <li><i class="icofont-envelope"> </i> hello@gmail.com</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-md-7">
                        <p> حقوق الطبع والنشر <i class="icofont-copyright"> </i> 2021 جميع الحقوق محفوظة. </p>
                    </div>
                    <div class="col-lg-5 col-md-5">
                        <ul>
                            <li><a href="#" class="icofont-facebook"></a></li>
                            <li><a href="#" class="icofont-twitter"></a></li>
                            <li><a href="#" class="icofont-instagram"></a></li>
                            <li><a href="#" class="icofont-linkedin"></a></li>
                            <li><a href="#" class="icofont-vimeo"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div class="go-top"><i class="icofont-stylish-up"></i></div>
    <script src="https://templates.envytheme.com/axolot/rtl/assets/js/jquery.min.js"></script>
    <script src="https://templates.envytheme.com/axolot/rtl/assets/js/popper.min.js"></script>
    <script src="https://templates.envytheme.com/axolot/rtl/assets/js/bootstrap.min.js"></script>
    <script src="https://templates.envytheme.com/axolot/rtl/assets/js/owl.carousel.min.js"></script>
    <script src="https://templates.envytheme.com/axolot/rtl/assets/js/jquery.magnific-popup.min.js"></script>
    <script src="https://templates.envytheme.com/axolot/rtl/assets/js/jquery.mixitup.min.js"></script>
    <script src="https://templates.envytheme.com/axolot/rtl/assets/js/wow.min.js"></script>
    <script src="https://templates.envytheme.com/axolot/rtl/assets/js/form-validator.min.js"></script>
    <script src="https://templates.envytheme.com/axolot/rtl/assets/js/contact-form-script.js"></script>
    <script src="https://templates.envytheme.com/axolot/rtl/assets/js/jquery.ajaxchimp.min.js"></script>
    <script src="https://templates.envytheme.com/axolot/rtl/assets/js/main.js"></script>

</body>

</html>