@extends('frontOffice.layout')

@section('head')
@include('frontOffice.inc.head')
@endsection

@section('header')
@include('frontOffice.inc.header')
@endsection



@section('content')


<section class="breadcrumb-section wow fadeIn">
    <div class="container">
        <div class="breadcrumb-wrap">
            <a href="{{route('showHome',['lang' => $lang])}}"> {{ucfirst(trans('lang.home'))}} </a>
            <i>/</i>
            <span>
                <span id="ctl00_ContentPlaceHolder1_lblTitle"> {{ucfirst(trans('lang.our_services'))}} </span></span>
        </div>
    </div>
</section>
{{-- 
<section class="inner-page-section wow fadeIn">
    <div class="container">
        <div class="section-title wow fadeInDown text-center" style="visibility: visible; animation-name: fadeInDown;">
            <h2>
                <span id="ctl00_ContentPlaceHolder1_lblTitle1"> {{ucfirst(trans('lang.our_services'))}}</span>
            </h2>
        </div>
        <div class="page-img text-center wow fadeInDown">

            @if($service->photo)
            <img src="{{asset($service->photo)}}" allowfullscreen="allowfullscreen" frameborder="0" loading="lazy" style="width:50%;" height="400px%">

            @endif
        </div>
        <div class="page-img text-center wow fadeInDown">
            <a href="{{route('showAddFreeConsultation',['lang' => $lang,])}}" class="btn btn-primary service-details"> {{ucfirst(trans('lang.free_consultations'))}} </a>
            <a href="{{route('showAddConsultation',['lang' => $lang])}}" class="btn btn-primary service-details"> {{ucfirst(trans('lang.service_request'))}} </a>
        </div>

        <div class="page-text wow fadeInDown">


            {!! $service->description !!}

        </div>
    </div>
</section>

--}}




<section class="inner-page-section wow fadeIn">
    <div class="container">
        <div class="row about-text-slider">
            <div class="col-md-4">
                <p class="c-title c-font-uppercase c-theme-on-hover c-font-bold"></p>
                <p style="line-height: normal;">
                </p>
                <div class="about-text" style="text-align: center;">
                    <h1 class="wow fadeInDown">{{$service->title}}</h1>
                    <p class="wow fadeInDown"> {!! $service->short_description !!} </p>
                    <a href="{{route('showAddConsultation',['lang' => $lang])}}" class='btn btn-bordered'>{{ucfirst(trans('lang.service_request'))}} </a>
                    <a href="{{route('showAddFreeConsultation',['lang' => $lang,])}}" class='btn btn-bordered'> {{ucfirst(trans('lang.free_consultations'))}} </a>
         
                </div>
            </div>
            <div class="col-md-8">
                <div class="slider-wrapper wow fadeIn">
                    <div class="slider-title wow fadeInDown">
                        <span id="ctl00_ContentPlaceHolder1_lblTitle2">{{$service->title}}</span>
                    </div>
                    <div class="single-slide-carousel owl-theme owl-carousel">
                        @foreach($slides as $slide)
                        <div class="img-item">
                            <img src="{{asset($slide->photo)}}" />
                        </div>
                        @endforeach

                     
                    </div>
                </div>
            </div>
        </div>
        <div class="about-full-text wow fadeIn">
            <div dir="ltr" style="text-align: left;">
                <p style="text-align:right">{!!$service->description!!}</p>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript" src="{{ asset('js/frontOffice/jquery-1.9.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/frontOffice/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/frontOffice/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/frontOffice/wow.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/frontOffice/script.js?c=1') }}"></script>




@endsection