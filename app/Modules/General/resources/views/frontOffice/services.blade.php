@extends('frontOffice.layout')

@section('head')
    @include('frontOffice.inc.head')
@endsection

@section('header')
    @include('frontOffice.inc.header')
@endsection



@section('content')


<div 

@if ($lang == "en")
  dir ="ltr"
@else 
 dir="rtl"
 
@endif 

>

<section class="breadcrumb-section wow fadeIn">
    <div class="container">
        <div class="breadcrumb-wrap">
            <a href="#">{{ucfirst(trans('lang.home'))}}</a>
            <i>/</i>
            <span>{{ucfirst(trans('lang.our_services'))}}</span>
        </div>
    </div>
</section>

<section class="services wow fadeIn">
    <div class="container">
        <div class="section-title text-center wow fadeInUp">
            <h1> {{ ucfirst(trans('lang.our_services')) }} </h1>
        </div>
        <div class="row">

          @foreach($services as $service)

            <div class="col-xs-6 col-md-3">
                <div class="service-item wow fadeInUp" data-wow-delay="0.0s">
                    <img src="{{asset($service->photo)}}" style="height: 390px;" />
                    <div class="service-caption">
                        <div class="service-caption-wrap">
                            <h3> {{$service->title}} </h3>
                            <p> {{$service->short_description}}</p>
                            <a href="{{route('showAddFreeConsultation',['lang' => $lang])}}" class="btn btn-bordered service-details"> {{ucfirst(trans('lang.free_consultations'))}} </a>
                            <a href="{{route('showAddConsultation',['lang' => $lang])}}" class="btn btn-bordered service-details"> {{ucfirst(trans('lang.service_request'))}} </a>
                           <a href="{{route('showSingleService',['lang'=>$lang, 'id' => $service->id])}}" class="btn btn-bordered service-details"> {{ucfirst(trans('lang.details'))}} </a> 
                        </div>
                    </div>
                </div>
            </div>

        

            @endforeach

        </div>
    </div>
</section>

</div>

@endsection