@extends('frontOffice.layout')

@section('head')
@include('frontOffice.inc.head')
@endsection

@section('header')
@include('frontOffice.inc.header')
@endsection



@section('content')


<section class="breadcrumb-section wow fadeIn">
    <div class="container">
        <div class="breadcrumb-wrap">
            <a href="./"> {{ucfirst(trans('lang.home'))}} </a>
            <i>/</i>
            <span>{{ucfirst(trans('lang.about_us'))}} </span>
        </div>
    </div>
</section>






<section class="about-section wow fadeIn">
    <div class="container">
        <div class="row about-text-slider">
            <div class="col-md-4">
                <div class="about-text">
                    <h1 class="wow fadeInDown"> </h1>
                    <p class="wow fadeInDown">
                        {!!$about->short_description!!}
                    </p>
                </div>
            </div>


            <div class="col-md-8">
                <div class="slider-wrapper wow fadeIn">
                    <div class="slider-title wow fadeInDown">{{ ucfirst(trans('lang.about_company')) }} </div>
                    <div class="single-slide-carousel owl-theme owl-carousel">
                     
                     @foreach($slides as $slide)
                        <div class="img-item">
                            <img src="{{asset($slide->photo)}}" />
                        </div>
                     @endforeach   
                     
                    </div>
                </div>
            </div>


        </div>
        <div class="about-full-text wow fadeIn">
            <div dir="ltr" style="text-align: left;">
                <p style="text-align:right">{!!$about->description!!}</p>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript" src="{{ asset('js/frontOffice/jquery-1.9.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/frontOffice/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/frontOffice/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/frontOffice/wow.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/frontOffice/script.js?c=1') }}"></script>



@endsection