@extends('frontOffice.layout')

@section('head')
    @include('frontOffice.inc.head')
@endsection

@section('header')
    @include('frontOffice.inc.header')
@endsection



@section('content')


<section class="team-section wow fadeIn">
    <div class="container">
        <div class="section-title text-center wow fadeInUp">
            <h2> {{ucfirst(trans('lang.advisory_body'))}}  </h2>
        </div>

        <div class="owl-theme owl-carousel three-slide-carousel team-carousel">

        @foreach($users as $user)
        <div class="team-item">
                <div class="team-img">
                    <img src="{{asset($user->photo)}}" />
                </div>


                <div class="team-content">
                    @if ($lang == 'en')
                    <h3> {{$user->name}} </h3>
                    <span> {{$user->title}} </span>
                    <p>
                       {{$user->short_description}}
                    </p>
                    @else
                    <h3> {{$user->name_ar}} </h3>
                    <span> {{$user->title_ar}} </span>
                    <p>
                       {{$user->short_description_ar}}
                    </p>
                    @endif
              </div>
          </div>

        @endforeach
     


         
         
        
        </div>
    </div>
</section>


<script type="text/javascript" src="{{ asset('js/frontOffice/jquery-1.9.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/frontOffice/bootstrap.min.js') }}"></script>
     <script type="text/javascript" src="{{ asset('js/frontOffice/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/frontOffice/wow.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/frontOffice/script.js?c=1') }}"></script>

@endsection