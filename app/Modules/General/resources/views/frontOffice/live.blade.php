@extends('frontOffice.layout')

@section('head')
@include('frontOffice.inc.head',
['title' => ' - Administration',
'description' => 'Espace Administratif - '
])
@endsection

@section('header')
@include('frontOffice.inc.header')
@endsection






@section('content')

<section class="breadcrumb-section wow fadeIn">
    <div class="container">
        <div class="breadcrumb-wrap">
            <a href="{{route('showHome',['lang' => $lang])}}">{{ucfirst(trans('lang.home'))}}</a>
            <i>/</i>
            <span> {{ucfirst(trans('lang.live_stream'))}} </span>
        </div>
    </div>
</section>

<section class="inner-page-section wow fadeIn">
    <div class="container">
        <div class="section-title wow fadeInDown text-center" style="visibility: visible; animation-name: fadeInDown;">
            <h2>
                <span id="ctl00_ContentPlaceHolder1_lblTitle1"> {{ucfirst(trans('lang.live_stream'))}} </span>
            </h2>
        </div>
        <div class="col-md-12">
            <center>
                <iframe src="https://www.youtube.com/embed/tgbNymZ7vqY" width="900" height="500" frameborder="0" allowfullscreen></iframe>
            </center>
        </div>
    </div>
</section>


@endsection