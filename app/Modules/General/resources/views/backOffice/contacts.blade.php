@extends('backOffice.layout')

@section('head')
@include('backOffice.inc.head',
['title' => 'Dashboard',
'description' => 'Espace Administratif - '
])
@endsection

@section('header')
@include('backOffice.inc.header')
@endsection

@section('sidebar')
@include('backOffice.inc.sidebar', [
'current' => 'services'
])
@endsection

@section('content')

<div class="page-content">


    <link rel="stylesheet" type="text/css" href="{{ asset('plugins') }}/datatable/datatable.css">
    <script type="text/javascript" src="{{ asset('plugins') }}/datatable/datatable.js"></script>

    <script type="text/javascript">
        /* Datatables responsive */

        $(document).ready(function() {
            $('#datatable-responsive').DataTable({
                responsive: true,
                language: {
                    url: "{{ asset('plugins/datatable/lang/'.\Illuminate\Support\Facades\Session::get('youfors_applocale').'.js') }}"
                }
            });
            $('.dataTables_filter input').attr("placeholder", "{{ trans('lang.search') }}...");
        });
    </script>








    <nav class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">{{ trans('lang.dashboard') }}</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ trans('lang.messages') }}</li>
        </ol>
    </nav>

    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="breadcrumb">
                                <h4>{{ trans('lang.messages') }} </h4>
                                
                            </div>
                        </div>
                    
                    </div>

                    <div class="table-responsive">
                        <table id="dataTableExample" id="datatable-responsive" class="table">
                            <thead>
                                <tr>
                                    <th>{{ trans('lang.full_name') }}</th>
                                    <th>{{ trans('lang.email') }}</th>
                                    <th>{{ trans('lang.subject') }}</th>
                                    <th>{{ trans('lang.content') }}</th>
                                    <th>{{ trans('lang.date') }}</th>
                             
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($contacts as $contact)
                                <tr>
                                    <td>{{ $contact->name }}</td>
                                    <td>{{ $contact->email }}</td>
                                    <td>{{ $contact->subject}}  </td>
                                    <td> {{ $contact->content }} </td>
                                    <td>{{ $contact->created_at->format('d-M-Y H:m:s') }}</td>
                           
                                </tr>
                                @endforeach


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection