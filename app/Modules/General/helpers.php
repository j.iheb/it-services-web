<?php

use App\Modules\User\Models\User ; 

/**
 *	General Helper
 */
if (!function_exists('checkAdministratorRole')) {
    /**
     * @param \App\Modules\User\Models\User $user
     * @return bool
     */
    function checkAdministratorRole($user)
    {
       $user = User::find($user->id); 
       
        foreach ($user->roles as $role) {
            if ($role->id == 2) {
                return true;
            }
        }
        return false;
    }
}



if (!function_exists('checkOfficerRole')) {
    /**
     * @param \App\Modules\User\Models\User $user
     * @return bool
     */
    function checkOfficerRole($user)
    {
       $user = User::find($user->id); 
       
        foreach ($user->roles as $role) {
            if ($role->id == 3) {
                return true;
            }
        }
        return false;
    }
}



if (!function_exists('checkAdviserRole')) {
    /**
     * @param \App\Modules\User\Models\User $user
     * @return bool
     */
    function checkAdviserRole($user)
    {
       $user = User::find($user->id); 
       
        foreach ($user->roles as $role) {
            if ($role->id == 4 ) {
                return true;
            }
        }
        return false;
    }
}



/**
 *	General Helper
 */

if (!function_exists('changeLocaleInRoute')) {
    /**
     * @param Illuminate\Routing\Route $route
     * @param string $lang
     * @return string
     */
    function changeLocaleInRoute($route, $lang){
        $parameters = $route->parameters();
        $parameters['lang'] = $lang;
        $name = $route->getName();

        return route($name,$parameters);
    }
}

if (!function_exists('setupLocaleForNonLocaledRoutes')) {
    /**
     * @return string
     */
    function setupLocaleForNonLocaledRoutes(){
        if (\Illuminate\Support\Facades\Auth::user() and in_array(\Illuminate\Support\Facades\Auth::user()->language, array_keys(\Illuminate\Support\Facades\Config::get('app.languages')))) {
            $locale = \Illuminate\Support\Facades\Auth::user()->language;
        } elseif (\Illuminate\Support\Facades\Cookie::has('applocale')) {
            $locale = \Illuminate\Support\Facades\Cookie::get('applocale');
        } else {
            $locale = \Illuminate\Support\Facades\Config::get('app.locale');
        }

        app()->setLocale($locale);

        return $locale;
    }
}

if (!function_exists('transformAmountsToCurrencySpecificStrings')) {
    /**
     * @param float $nonFormattedAmount
     * @param \App\Modules\General\Models\Currency $currency
     * @param boolean $withSymbol
     * @return string
     */
    function transformAmountsToCurrencySpecificStrings($currency, $nonFormattedAmount, $withSymbol = true){
        $formattedAmount = (string) number_format($nonFormattedAmount, strlen($currency->subunit_to_unit) - 1, $currency->decimal_mark, $currency->thousands_separator);
        if($withSymbol){
            $formattedAmount = $formattedAmount . ' ' . $currency->symbol;
        }
        return $formattedAmount;
    }
}


if (!function_exists('detectBase64ImageMimeType')) {
    /**
     * @param string $base64
     * @return string
     */
    function detectBase64ImageMimeType($base64){
        return substr($base64, 5, strpos($base64, ';')-5);
    }
}


if (!function_exists('readableBytes')) {
    /**
     * @param int $bytes
     * @return string
     */
    function readableBytes($bytes) {
        $i = floor(log($bytes) / log(1024));

        $sizes = array('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');

        $division = pow(1024, $i);
        return sprintf('%.02F', $bytes / ($division == 0 ? 1 : $division)) * 1 . ' ' . $sizes[$i];
    }
}




