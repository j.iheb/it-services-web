<?php

namespace App\Modules\Note\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Note\Models\Note;
use App\Modules\User\Models\User;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Mockery\Matcher\Not;
use PhpParser\Node\Expr\FuncCall;

class WebController extends Controller
{


    public function showManagerNotes($lang)
    {
        return view('Note::backOffice.list', [
            'notes' => Note::all(), 
            'lang' => $lang, 
        ]); 
    }

    public function showManagerAddNote($lang )
    {
        return view('Note::backOffice.new', [
            
            'lang' => $lang, 
            'users' => User::all(),
        ]); 
    }

    public function handleManagerAddNote($lang, Request $request)
    {
        $data = $request->all(); 


        $rules = [
            'title' => 'required', 
            'content' => 'required', 
            'user_id' => 'required', 
        ]; 

        $messages = [
            'title.required' => ucfirst(trans('lang.required', ['string' => trans('lang.title')])), 
            'content.required' => ucfirst(trans('lang.required', ['string' => trans('lang.content')])), 
            'user_id.required' => ucfirst(trans('lang.required', ['string' => trans('lang.user')])), 
            
        ] ; 

        $valdations = Validator::make($data, $rules, $messages); 

        if ($valdations->fails())
        {
            return back()->withErrors($valdations->errors()); 
        }

        $note = Note::create([
            'title' => $data['title'], 
            'content' => $data['content'], 
            'user_id' => $data['user_id']
        ]); 

        Toastr::success(ucfirst(trans('lang.operation_success'))); 
        return redirect()->route('showManagerNotes',['lang' => $lang]); 


    }


    public function showManagerEditNote($lang, $id)
    {
        return view('Note::backOffice.new',[
            'note' => Note::find($id), 
            'lang' => $lang, 
            'users' => User::all(),
            
        ]); 
    }

    public function handleManagerUpdateNote($lang, $id, Request $request)
    {
        $data = $request->all(); 


        $rules = [
            'title' => 'required', 
            'content' => 'required', 
            'user_id' => 'required', 
        ]; 

        $messages = [
            'title.required' => ucfirst(trans('lang.required', ['string' => trans('lang.title')])), 
            'content.required' => ucfirst(trans('lang.required', ['string' => trans('lang.content')])), 
            'user_id.required' => ucfirst(trans('lang.required', ['string' => trans('lang.user')])), 
            
        ] ; 

        $valdations = Validator::make($data, $rules, $messages); 

        if ($valdations->fails())
        {
            return back()->withErrors($valdations->errors()); 
        }

       $note = Note::find($id); 

       if ($note)
       {
           $note->title = $data['title']; 
           $note->content = $data['content']; 
           $note->update(); 
       }
        Toastr::success(ucfirst(trans('lang.operation_success'))); 
        return redirect()->route('showManagerNotes',['lang' => $lang]); 


    }

    public function handleManagerDeleteNote($lang, $id)
    {
        Note::find($id)-> delete(); 
        Toastr::success(ucfirst(trans('lang.operation_success'))); 
        return redirect()->route('showManagerNotes',['lang' => $lang]); 

    }
}
