@extends('backOffice.layout')

@section('head')
@include('backOffice.inc.head',
['title' => 'Dashboard',
'description' => 'Espace Administratif - '
])
@endsection

@section('header')
@include('backOffice.inc.header')
@endsection

@section('sidebar')
@include('backOffice.inc.sidebar', [
'current' => 'services'
])
@endsection

@section('content')


<div class="page-content">

    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                <div class="row">
                        <div class="col-md-9">
                            <div class="breadcrumb">
                                <h4>{{ ucfirst( trans('lang.notes'))}}</h4>
                                
                            </div>
                        </div>
                        <div class="col-md-3 pull-right">
                            <div class="float-right">
                                <a href="{{route('showManagerNotes',['lang' => $lang])}}" class="btn btn-primary">{{ ucwords(trans('lang.notes')) }}</a>
                            </div>
                        </div>
                    </div>

  
    <form id="form"  method="POST"
    @isset($note)
     action="{{ route('handleManagerUpdateNote',['lang' => $lang, 'id' => $note->id]) }}"
    @else
    action="{{ route('handleManagerAddNote',['lang' => $lang]) }}"
    @endisset 
      enctype="multipart/form-data">
        {{ csrf_field() }} 
        <div class="row mt-5" >

            <div class="col-md-6 form-group mb-3">
                <label for="title">{{ucfirst( trans('lang.title'))}}</label>
                <input class="form-control" type="text"  name="title" id="title"  placeholder="{{trans('lang.title')}}"  @isset($note) value="{{$note->title}}" @endisset  required>
              
            </div>
            <div class="col-md-6 form-group mb-3">
                <label for="description">{{ucfirst( trans ('lang.user'))}}</label>
                <select name="user_id" id="">
                    @foreach($users as $user)
                   <option value="{{$user->id}}">{{$user->name}}</option>
                   @endforeach
                </select>
            </div>

           <div class="col-md-6 form-group mb-3">
                <label for="description">{{ucfirst( trans ('lang.content'))}}</label>
                <textarea class="form-control" name="content" id="short_description"  rows="5"  placeholder="{{trans('lang.content')}}" >@isset($note) {{$note->content}} @endisset</textarea>
            </div>

             <div class="col-md-12">
                <button type="submit" class="btn btn-primary"> {{ucfirst(trans('lang.send'))}} </button>
            </div>
        </div>
    </form>
</div>
            </div>
        </div></div>
<script>
$("#form").validate({
    ignore: ":hidden",
    errorClass: "danger is-invalid",
    validClass: "success is-valid",
    highlight: function (element, errorClass, validClass) {
        $(element).addClass(errorClass).removeClass(validClass);
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass(errorClass).addClass(validClass);
    },
    errorPlacement: function (i, e) {
        i.insertAfter(e)
    },
    rules: {
        "name": {
            required: true,
            minlength: 3,
           
        },
   
        "featured_picture": {
            extension: "jpg|jpeg|png",
            fileSizeMax: 2048000
        }
    },
    messages: {
        "name": {
            required: "{{ trans('lang.required', ['string' =>  ucfirst(trans('shared.name'))]) }}",
            minlength: "{{ trans('lang.min_string', ['string' =>   ucfirst(trans('shared.name')), 'min' => 3]) }}",
        },
  
    }
});
</script>
</div>
@endsection
