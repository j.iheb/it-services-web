
<?php


Route::group(['module' => 'Note', 'middleware' => ['web'], 'prefix' => '{lang}'], function () {


Route::get('/manager/note/new', 'WebController@showManagerAddNote')->name('showManagerAddNote'); 
Route::get('/manager/notes', 'WebController@showManagerNotes')->name('showManagerNotes'); 
Route::post('/manager/note/add', 'WebController@handleManagerAddNote')->name('handleManagerAddNote'); 
Route::get('/manager/note/{id}/delete', 'WebController@handleManagerDeleteNote')->name('handleManagerDeleteNote'); 
Route::get('/manager/note/{id}', 'WebController@showManagerEditNote')->name('showManagerEditNote'); 
Route::post('/manager/note/{id}/update', 'WebController@handleManagerUpdateNote')->name('handleManagerUpdateNote'); 









}); 