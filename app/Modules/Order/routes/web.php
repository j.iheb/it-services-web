
<?php


Route::group(['module' => 'Order', 'middleware' => ['web'], 'prefix' => '{lang}'], function () {


Route::get('/manager/order/new', 'WebController@showManagerAddOrder')->name('showManagerAddOrder'); 
Route::get('/manager/orders', 'WebController@showManagerOrders')->name('showManagerOrders'); 
Route::post('/manager/orders/add', 'WebController@handleManagerAddOrder')->name('handleManagerAddOrder'); 
Route::get('/manager/order/{id}/delete', 'WebController@handleManagerDeleteOrder')->name('handleManagerDeleteOrder'); 
Route::get('/manager/order/{id}', 'WebController@showManagerEditOrder')->name('showManagerEditOrder'); 
Route::post('/manager/orders/{id}/update', 'WebController@handleManagerUpdateOrder')->name('handleManagerUpdateOrder'); 

Route::get('/manager/order/{id}/status/change', 'WebController@handleManagerChangeOrderStatus')->name('handleManagerChangeOrderStatus'); 



}); 