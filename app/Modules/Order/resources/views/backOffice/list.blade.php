@extends('backOffice.layout')

@section('head')
@include('backOffice.inc.head',
['title' => 'Dashboard',
'description' => 'Espace Administratif - '
])
@endsection

@section('header')
@include('backOffice.inc.header')
@endsection

@section('sidebar')
@include('backOffice.inc.sidebar', [
'current' => 'services'
])
@endsection

@section('content')

<div class="page-content">


    <link rel="stylesheet" type="text/css" href="{{ asset('plugins') }}/datatable/datatable.css">
    <script type="text/javascript" src="{{ asset('plugins') }}/datatable/datatable.js"></script>

    <script type="text/javascript">
        /* Datatables responsive */

        $(document).ready(function() {
            $('#datatable-responsive').DataTable({
                responsive: true,
                language: {
                    url: "{{ asset('plugins/datatable/lang/'.\Illuminate\Support\Facades\Session::get('youfors_applocale').'.js') }}"
                }
            });
            $('.dataTables_filter input').attr("placeholder", "{{ trans('lang.search') }}...");
        });
    </script>








    <nav class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">{{ trans('lang.dashboard') }}</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ trans('lang.orders') }}</li>
        </ol>
    </nav>

    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="breadcrumb">
                                <h4>{{ trans('lang.orders') }} </h4>
                                
                            </div>
                        </div>
                        <div class="col-md-3 pull-right">
                            <div class="float-right">
                                <a href="{{route('showManagerAddOrder',['lang' => $lang])}}" class="btn btn-primary">{{ ucwords(trans('lang.new')) }}</a>
                            </div>
                        </div>
                    
                    </div>

                    <div class="table-responsive">
                        <table id="dataTableExample" id="datatable-responsive" class="table">
                            <thead>
                                <tr>
                                    <th>{{ trans('lang.full_name') }}</th>
                                    <th>{{ trans('lang.subject') }}</th>
                                    <th>{{ trans('lang.content') }}</th>
                                    <th>{{ trans('lang.amount') }}</th>
                                    <th>{{ trans('lang.status')}}</th>
                                    <th>{{ trans('lang.date') }}</th>
                                    <th>{{ trans('lang.action') }}</th>
                             
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($orders as $order)
                                <tr>
                                    <td>{{ $order->user->name }}</td>
                                    <td>{{ $order->subject }}</td>
                                    <td>{{ $order->content}}  </td>
                                    <td> {{ $order->amount }} </td>
                                    <td>
                                      @if($order->status == 0)
                                        <span class="badge badge-success text-white">{{ucfirst(trans('lang.active'))}}</span>
                                      @else 
                                      <span class="badge badge-secondary text-white">{{ucfirst(trans('lang.closed'))}}</span>
                                      @endif
                                    </td>
                                    <td>{{ $order->created_at->format('d-M-Y H:m:s') }}</td>
                                    <td>
                                    <span class="badge badge-pill badge-danger p-2 m-1"  onclick="location='{{ route('handleManagerDeleteOrder', ['lang' => $lang,'id' => $order->id]) }}';"><i class="link-icon" data-feather="trash"></i>  </span>
                                        <span class="badge badge-pill badge-primary p-2 m-1"  onclick="location='{{ route('showManagerEditOrder', ['lang' => $lang,'id' => $order->id]) }}';"><i class="link-icon" data-feather="edit"></i>  </span>
                                        <span class="badge badge-pill badge-secondary p-2 m-1"  onclick="location='{{ route('handleManagerChangeOrderStatus', ['lang' => $lang,'id' => $order->id]) }}';"><i class="link-icon" data-feather="rotate-cw"></i>  </span>
                                        
                                    </td>
                           
                                </tr>
                                @endforeach


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection