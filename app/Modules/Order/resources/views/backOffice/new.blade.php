@extends('backOffice.layout')

@section('head')
@include('backOffice.inc.head',
['title' => 'Dashboard',
'description' => 'Espace Administratif - '
])
@endsection

@section('header')
@include('backOffice.inc.header')
@endsection

@section('sidebar')
@include('backOffice.inc.sidebar', [
'current' => 'services'
])
@endsection

@section('content')


<div class="page-content">

    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                <div class="row">
                        <div class="col-md-9">
                            <div class="breadcrumb">
                                <h4>{{ ucfirst( trans('lang.orders'))}}</h4>
                                
                            </div>
                        </div>
                        <div class="col-md-3 pull-right">
                            <div class="float-right">
                                <a href="{{route('showManagerOrders',['lang' => $lang])}}" class="btn btn-primary">{{ ucwords(trans('lang.orders')) }}</a>
                            </div>
                        </div>
                    </div>

  
    <form id="form"  method="POST" 
     @isset($order)
       action="{{ route('handleManagerUpdateOrder',['lang' => $lang, 'id' => $order->id]) }}" 
     @else 
       action="{{ route('handleManagerAddOrder',['lang' => $lang]) }}" 
     @endisset  
     enctype="multipart/form-data">
        {{ csrf_field() }} 
        <div class="row mt-5" >

            <div class="col-md-6 form-group mb-3">
                <label for="title">{{ucfirst( trans('lang.subject'))}}</label>
                <input class="form-control" type="text"  name="subject" id="title"  placeholder="{{trans('lang.subject')}}"  @isset($order) value="{{$order->subject}}" @endisset  required>
              
            </div>

          

            <div class="col-md-6 form-group mb-3">
                <label for="title">{{ucfirst( trans('lang.amount'))}}</label>
                <input class="form-control" type="number"  name="amount" id="title"  placeholder="{{trans('lang.amount')}}" @isset($order) value="{{$order->amount}}" @endisset required >
              
            </div>

         

            <div class="col-md-6 form-group mb-3">
                <label for="title">{{ucfirst( trans('lang.service'))}}</label>
                <select name="service_id" class="form-control" id="">
                   @foreach($services as $service)
                     <option value="{{$service->id}}">{{$service->title}}</option>
                   @endforeach
                 </select>
            </div>


            <div class="col-md-6 form-group mb-3">
                <label for="title">{{ucfirst( trans('lang.users'))}}</label>
                <select name="user_id" class="form-control" id="">
                   @foreach($users as $user)
                     <option value="{{$user->id}}">{{$user->name}}</option>
                   @endforeach
                 </select>
            </div>




            <div class="col-md-6 form-group mb-3">
                <label for="title">{{ucfirst( trans('lang.type'))}}</label>
                <select name="type" class="form-control" id="">
                <option value="1">  {{ucfirst(trans('lang.on_the_place'))}} </option>
                     <option value="2">  {{ucfirst(trans('lang.remotely'))}}</option>
                
                 </select>
            </div>


        

            <div class="col-md-6 form-group mb-3">
                <label for="title">{{ucfirst( trans('lang.start_at'))}}</label>
                <input class="form-control" type="date"  name="start_at" id="title"  placeholder="{{trans('lang.start_at')}}"  >
              
            </div>

            <div class="col-md-6 form-group mb-3">
                <label for="title">{{ucfirst( trans('lang.end_at'))}}</label>
                <input class="form-control" type="date"  name="end_at" id="title"  placeholder="{{trans('lang.end_at')}}"  >
              
            </div>




            <div class="col-md-6 form-group mb-3">
                <label for="title">{{ucfirst( trans('lang.address'))}}</label>
                <input class="form-control" type="text"  name="address" id="title"  placeholder="{{trans('lang.address')}}" @isset($user) value="{{$user->address}}" @endisset >
              
            </div>
         

            <div class="col-md-6 form-group mb-3">
                <label for="description">{{ucfirst( trans ('lang.content'))}}</label>
                <textarea class="form-control" name="content" id="short_description"  rows="5"  placeholder="{{trans('lang.content')}}" >@isset($order) {{$order->content}} @endisset</textarea>
            </div>


           


         
         
          
            <div class="col-md-12">
                
                 <button type="submit" class="btn btn-primary"> {{ucfirst(trans('lang.send'))}} </button>
            </div>
        </div>
    </form>
</div>
            </div>
        </div></div>
<script>
$("#form").validate({
    ignore: ":hidden",
    errorClass: "danger is-invalid",
    validClass: "success is-valid",
    highlight: function (element, errorClass, validClass) {
        $(element).addClass(errorClass).removeClass(validClass);
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass(errorClass).addClass(validClass);
    },
    errorPlacement: function (i, e) {
        i.insertAfter(e)
    },
    rules: {
        "name": {
            required: true,
            minlength: 3,
           
        },
   
        "featured_picture": {
            extension: "jpg|jpeg|png",
            fileSizeMax: 2048000
        }
    },
    messages: {
        "name": {
            required: "{{ trans('lang.required', ['string' =>  ucfirst(trans('shared.name'))]) }}",
            minlength: "{{ trans('lang.min_string', ['string' =>   ucfirst(trans('shared.name')), 'min' => 3]) }}",
        },
  
    }
});
</script>
</div>
@endsection
