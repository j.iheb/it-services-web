<?php

namespace App\Modules\Order\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\General\Models\Service;
use App\Modules\Order\Models\Order;
use App\Modules\User\Models\User;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class WebController extends Controller
{

    /**
     * Display the module welcome screen
     *
     * @return \Illuminate\Http\Response
     */
    public function welcome()
    {
        return view("Order::welcome");
    }

    public function showManagerAddOrder($lang)
    {
        return view('Order::backOffice.new',[
            'lang' => $lang, 
            'services' => Service::all(), 
            'users' => User::all(), 
        ]) ; 
    }

    public function showManagerOrders($lang)
    {
      return view('Order::backOffice.list', [
          'lang' => $lang, 
          'orders' => Order::all(), 
      ]); 
    }

    public function handleManagerAddOrder($lang, Request $request){
        $data = $request->all(); 


        $rules = [ 
            'subject' => 'required', 
            'content' => 'required', 
            'amount' => 'required', 
            'start_at' => 'required', 
            'end_at' => 'required', 
            'user_id' => 'required', 
            'service_id' => 'required', 

        ]; 


        $messages = [
            'subject.required' =>  ucfirst(trans('lang.required' , ['string' => trans('lang.description')])), 
            'content.required' => ucfirst(trans('lang.required' , ['string' => trans('lang.content')])), 
            'amount.required' => ucfirst(trans('lang.required' , ['string' => trans('lang.amount')])), 
            'start_at.required' => ucfirst(trans('lang.required' , ['string' => trans('lang.start_at')])), 
            'end_at.required' => ucfirst(trans('lang.required' , ['string' => trans('lang.end_at')])), 
            'user_id.required' => ucfirst(trans('lang.required' , ['string' => trans('lang.user')])), 
            'service_id.required' => ucfirst(trans('lang.required' , ['string' => trans('lang.service')])), 
        ] ; 


        $validations =  Validator::make($data , $rules, $messages) ; 

        if ($validations-> fails())
        {
             return back()->withErrors($validations->errors()); 
        }

        $order = Order::create([
            'subject' => $data['subject'], 
            'content' => $data['content'], 
            'start_at' => $data['start_at'], 
            'end_at' => $data['end_at'], 
            'amount' => $data['amount'], 
            'user_id' => $data['user_id'], 
            'service_id' => $data['service_id'], 
            'status' => 0, 
            'address' => $data['address'], 
            'type' => 1,
        ]) ; 


        Toastr::success(ucfirst(trans('lang.operation_success'))); 
        return redirect()->route('showManagerOrders', ['lang' =>$lang]); 
    }


    public function showManagerEditOrder($lang, $id)
    { 
        return view('Order::backOffice.new', [
            'lang' => $lang, 
            'order' => Order::find($id), 
            'services' => Service::all(),
            'users' => User::all(), 
        ]); 

    }

    public function handleManagerChangeOrderStatus($lang, $id )
    {
        $order = Order::find($id); 
        if ($order->status == 0)
        {
            $order->status = 1 ; 
        }else 
        {
            $order->status = 0; 
        }
        $order->update() ; 
        Toastr::success(ucfirst(trans('lang.operation_success'))); 
        return back(); 

    }

    public function handleManagerUpdateOrder($lang, $id, Request $request)
    {

        $data = $request->all(); 


        $rules = [ 
            'subject' => 'required', 
            'content' => 'required', 
            'amount' => 'required', 
      
            'user_id' => 'required', 
            'service_id' => 'required', 

        ]; 


        $messages = [
            'subject.required' =>  ucfirst(trans('lang.required' , ['string' => trans('lang.description')])), 
            'content.required' => ucfirst(trans('lang.required' , ['string' => trans('lang.content')])), 
            'amount.required' => ucfirst(trans('lang.required' , ['string' => trans('lang.amount')])), 
            'user_id.required' => ucfirst(trans('lang.required' , ['string' => trans('lang.user')])), 
            'service_id.required' => ucfirst(trans('lang.required' , ['string' => trans('lang.service')])), 
        ] ; 


        $validations =  Validator::make($data , $rules, $messages) ; 

        if ($validations-> fails())
        {
             return back()->withErrors($validations->errors()); 
        }

        $order = Order::find($id); 
        if($order)
        {
            $order->subject = $data['subject']; 
            $order->content = $data['content']; 
            $order->amount = $data['amount']; 

            $order->service_id = $data['service_id']; 
            $order->start_at = isset($data['start_at'])? $data['start_at']: $order->start_at ; 
            $order->end_at = isset($data['end_at'])? $data['end_at']: $order->end_at ;
            
            $order->update(); 
            Toastr::success(ucfirst(trans('lang.operation_success'))); 

           return redirect()->route('showManagerOrders', ['lang' => $lang]); 

            
         }

        
    }

    public function handleManagerDeleteOrder($lang, $id)
    {
        Order::find($id)->delete(); 
        Toastr::success('lang.operation_success'); 
        return back(); 
    }
    
    public function showUserOrders($lang)
    {
        return view('Order::frontOffice.orders',[
        'orders' => Order::where('user_id', Auth::id())->get(), 
         'lang' => $lang
        ]); 
    }
}
