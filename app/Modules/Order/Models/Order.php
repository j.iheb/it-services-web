<?php

namespace App\Modules\Order\Models;

use App\Models\User;
use App\Modules\General\Models\Service;
use App\Modules\User\Models\User as ModelsUser;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;


        /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'order';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'subject',
        'contnt',
        'status',
        'content',
        'start_at', 
        'end_at', 
        'amount', 
        'user_id', 
        'service_id', 
        'created_at',
        'type', 
        
   
    ];


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id'); 
    }

    public function service()
    {
        return $this->belongsTo(Service::class, 'service_id'); 
    }

}
