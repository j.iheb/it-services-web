@extends('backOffice.layout')

@section('head')
@include('backOffice.inc.head',
['title' => 'Dashboard',
'description' => 'Espace Administratif - '
])
@endsection


@section('content')
<div class="page-wrapper full-page">
			<div class="page-content d-flex align-items-center justify-content-center">

				<div class="row w-100 mx-0 auth-page">
					<div class="col-md-8 col-xl-6 mx-auto">
						<div class="card">
							<div class="row">
                <div class="col-md-4 pr-md-0">
                  <div class="auth-left-wrapper" style="background-image: url('https://image.freepik.com/free-vector/80-style-background-with-geometric-shapes_23-2148250712.jpg');">
               
                  </div>
                </div>
                <div class="col-md-8 pl-md-0">
                  <div class="auth-form-wrapper px-4 py-5">
                    <a href="#" class="noble-ui-logo d-block mb-2">Lo<span>go</span></a>
                    <h5 class="text-muted font-weight-normal mb-4">{{ucfirst(trans('lang.login_text'))}}</h5>
                    <form class="forms-sample" action="{{route('handleUserLogin',['lang' => $lang])}}" method="POST">
                    @csrf
                      <div class="form-group">
                        <label for="exampleInputEmail1">{{ucfirst(trans('lang.email'))}}</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email" name="email">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">{{ucfirst(trans('lang.password'))}}</label>
                        <input type="password" class="form-control" id="exampleInputPassword1" autocomplete="current-password" name="password" placeholder="Password">
                      </div>
                     
                      <div class="mt-3">
                        <button class="btn btn-primary mr-2 mb-2 mb-md-0 text-white" type="submit">{{ucfirst(trans('lang.login'))}}</button>

                       
                      </div>
               
                    </form>
                  </div>
                </div>
              </div>
						</div>
					</div>
				</div>

			</div>
		</div>
@endsection