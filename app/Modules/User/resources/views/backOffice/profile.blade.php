@extends('backOffice.layout')

@section('head')
@include('backOffice.inc.head',
['title' => 'Dashboard',
'description' => 'Espace Administratif - '
])
@endsection

@section('header')
@include('backOffice.inc.header')
@endsection

@section('sidebar')
@include('backOffice.inc.sidebar', [
'current' => 'services'
])
@endsection

@section('content')


<div class="page-content">

    <div class="profile-page tx-13">
        <div class="row">
            <div class="col-12 grid-margin">
                <div class="profile-header">
                    <div class="cover">
                        <div class="gray-shade"></div>
                        <figure>
                            <img src="https://www.nobleui.com/html/template/assets/images/profile-cover.jpg" class="img-fluid" alt="profile cover">
                        </figure>
                        <div class="cover-body d-flex justify-content-between align-items-center"  style="margin-top: -70px;">
                            <div>
                                <img class="profile-pic" src="https://ui-avatars.com/api/?name={{$user->name}}" alt="profile" >
                                <span class="profile-name"> {{$user->name}} </span>
                            </div>
                            <div class="d-none d-md-block">
                                <button class="btn btn-primary btn-icon-text btn-edit-profile">
                                    <i data-feather="edit" class="btn-icon-prepend"></i> تعديل
                                </button>
                            </div>
                        </div>
                    </div>
                  
                </div>
            </div>
        </div>
        <div class="row profile-body">
            <!-- left wrapper start -->
            <div class="d-none d-md-block col-md-4 col-xl-3 left-wrapper">
                <div class="card rounded">
                    <div class="card-body">
                        <div class="d-flex align-items-center justify-content-between mb-2">
                            <h6 class="card-title mb-0"> معلومات</h6>
                            <div class="dropdown">
                                <button class="btn p-0" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="icon-lg text-muted pb-3px" data-feather="more-horizontal"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="edit-2" class="icon-sm ml-2"></i> <span class="">Edit</span></a>
                                    <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="git-branch" class="icon-sm ml-2"></i> <span class="">Update</span></a>
                                    <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="eye" class="icon-sm ml-2"></i> <span class="">View all</span></a>
                                </div>
                            </div>
                        </div>
                        <p> إيهاب هو مطور البرمجيات المسوول عن تطوير الموقع </p>
                        <div class="mt-3">
                            <label class="tx-11 font-weight-bold mb-0 text-uppercase">تاريخ التسجيل:</label>
                            <p class="text-muted">November 15, 2015</p>
                        </div>
                        <div class="mt-3">
                            <label class="tx-11 font-weight-bold mb-0 text-uppercase">السكن:</label>
                            <p class="text-muted">باجة, تونس</p>
                        </div>
                        <div class="mt-3">
                            <label class="tx-11 font-weight-bold mb-0 text-uppercase">البريد:</label>
                            <p class="text-muted">{{$user->email}}</p>
                        </div>
                        <div class="mt-3">
                            <label class="tx-11 font-weight-bold mb-0 text-uppercase">الوقغ:</label>
                            <p class="text-muted">www.iheb.tn</p>
                        </div>
                       {{--  <div class="mt-3 d-flex social-links">
                            <a href="javascript:;" class="btn d-flex align-items-center justify-content-center border ml-2 btn-icon github">
                                <i data-feather="github" data-toggle="tooltip" title="github.com/nobleui"></i>
                            </a>
                            <a href="javascript:;" class="btn d-flex align-items-center justify-content-center border ml-2 btn-icon twitter">
                                <i data-feather="twitter" data-toggle="tooltip" title="twitter.com/nobleui"></i>
                            </a>
                            <a href="javascript:;" class="btn d-flex align-items-center justify-content-center border ml-2 btn-icon instagram">
                                <i data-feather="instagram" data-toggle="tooltip" title="instagram.com/nobleui"></i>
                            </a>
                        </div>

                        --}}

                    </div>
                </div>
            </div>
            <!-- left wrapper end -->
            <!-- middle wrapper start -->
            <div class="col-md-8 col-xl-6 middle-wrapper">

                <div class="row">

                <div class="col-md-12 stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex justify-content-between align-items-baseline mb-2">
                                    <h6 class="card-title mb-0"> طلبات</h6>
                                    <div class="dropdown mb-2">
                                        <button class="btn p-0" type="button" id="dropdownMenuButton7" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="icon-lg text-muted pb-3px" data-feather="more-horizontal"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton7">
                                            <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="eye" class="icon-sm ml-2"></i> <span class="">View</span></a>
                                            <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="edit-2" class="icon-sm ml-2"></i> <span class="">Edit</span></a>
                                            <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="trash" class="icon-sm ml-2"></i> <span class="">Delete</span></a>
                                            <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="printer" class="icon-sm ml-2"></i> <span class="">Print</span></a>
                                            <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="download" class="icon-sm ml-2"></i> <span class="">Download</span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-hover mb-0">
                                        <thead>
                                            <tr>
                                                <th class="pt-0">#</th>
                                                <th class="pt-0">عناون الخدمة</th>
                                                <th class="pt-0">يوم البداية</th>
                                                <th class="pt-0">يوم النهاية</th>
                                                <th class="pt-0">الحالة</th>
                                                <th class="pt-0">صاحب الخدمة</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>الصيانة</td>
                                                <td>01/01/2021</td>
                                                <td>26/04/2021</td>
                                                <td><span class="badge badge-danger">منتهيت</span></td>
                                                <td>عبد الرحمان</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>الدعم</td>
                                                <td>01/01/2021</td>
                                                <td>26/04/2021</td>
                                                <td><span class="badge badge-success">قيد التقييم</span></td>
                                                <td>عبد الرحمان</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>أمن المعلومات</td>
                                                <td>01/05/2021</td>
                                                <td>10/09/2021</td>
                                                <td><span class="badge badge-info-muted">معطلة</span></td>
                                                <td>عبد الرحمان</td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>شبكات</td>
                                                <td>01/01/2021</td>
                                                <td>31/11/2021</td>
                                                <td><span class="badge badge-warning">تحت التنفيد</span>
                                                </td>
                                                <td>عبد الرحمان</td>
                                            </tr>
                                            <tr>
                                                <td>5</td>
                                                <td>دعم فني</td>
                                                <td>01/01/2021</td>
                                                <td>31/12/2021</td>
                                                <td><span class="badge badge-danger-muted text-white">قريبا</span></td>
                                                <td>عبد الرحمان</td>
                                            </tr>
                                            <tr>
                                                <td>6</td>
                                                <td>أمن معلومات</td>
                                                <td>01/01/2021</td>
                                                <td>31/12/2021</td>
                                                <td><span class="badge badge-primary">قريبا</span></td>
                                                <td>عبد الرحمان</td>
                                            </tr>
                                            <tr>
                                                <td class="border-bottom">3</td>
                                                <td class="border-bottom">صيانة</td>
                                                <td class="border-bottom">01/05/2021</td>
                                                <td class="border-bottom">10/11/2021</td>
                                                <td class="border-bottom"><span class="badge badge-info-muted">معطل</span></td>
                                                <td class="border-bottom">عبد الرحمان</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>



                <div class="col-md-12 stretch-card mt-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex justify-content-between align-items-baseline mb-2">
                                    <h6 class="card-title mb-0">  الملاحظات</h6>
                                    <div class="dropdown mb-2">
                                        <button class="btn p-0" type="button" id="dropdownMenuButton7" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="icon-lg text-muted pb-3px" data-feather="more-horizontal"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton7">
                                            <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="eye" class="icon-sm ml-2"></i> <span class="">View</span></a>
                                            <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="edit-2" class="icon-sm ml-2"></i> <span class="">Edit</span></a>
                                            <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="trash" class="icon-sm ml-2"></i> <span class="">Delete</span></a>
                                            <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="printer" class="icon-sm ml-2"></i> <span class="">Print</span></a>
                                            <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="download" class="icon-sm ml-2"></i> <span class="">Download</span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-hover mb-0">
                                        <thead>
                                            <tr>
                                                <th class="pt-0">#</th>
                                                <th class="pt-0">  العنوان</th>
                                                <th class="pt-0">  الوصف</th>
                                                <th class="pt-0">تاريخ </th>
                                              
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>   زبون مهتم أكثر لجودة العمل   </td>
                                                <td>   يجب تقديم خدمة دات جودة عالية لهدا الزبون  </td>
                                                <td>01/01/2021</td>
                                            
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>   زبون مهتم أكثر لجودة العمل   </td>
                                                <td>   يجب تقديم خدمة دات جودة عالية لهدا الزبون  </td>
                                                <td>01/01/2021</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>   زبون مهتم أكثر لجودة العمل   </td>
                                                <td>   يجب تقديم خدمة دات جودة عالية لهدا الزبون  </td>
                                                <td>01/01/2021</td>
                                            </tr>
                                          
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>









                </div>
            </div>
            <!-- middle wrapper end -->
            <!-- right wrapper start -->
            <div class="d-none d-xl-block col-xl-3 right-wrapper">
                <div class="row">

                <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex justify-content-between align-items-baseline mb-2">
                                    <h6 class="card-title mb-0"> إحصاء طلب الخدمات في الشهر</h6>
                                    <div class="dropdown mb-2">
                                        <button class="btn p-0" type="button" id="dropdownMenuButton4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="icon-lg text-muted pb-3px" data-feather="more-horizontal"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton4">
                                            <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="eye" class="icon-sm ml-2"></i> <span class="">View</span></a>
                                            <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="edit-2" class="icon-sm ml-2"></i> <span class="">Edit</span></a>
                                            <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="trash" class="icon-sm ml-2"></i> <span class="">Delete</span></a>
                                            <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="printer" class="icon-sm ml-2"></i> <span class="">Print</span></a>
                                            <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="download" class="icon-sm ml-2"></i> <span class="">Download</span></a>
                                        </div>
                                    </div>
                                </div>
                                <p class="text-muted mb-4"></p>
                                <div class="monthly-sales-chart-wrapper">
                                    <canvas id="monthly-sales-chart"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-12 grid-margin stretch-card">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="d-flex justify-content-between align-items-baseline">
                                            <h6 class="card-title mb-0"> قيمة المدفعات</h6>
                                            <div class="dropdown mb-2">
                                                <button class="btn p-0" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="icon-lg text-muted pb-3px" data-feather="more-horizontal"></i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                                    <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="eye" class="icon-sm ml-2"></i> <span class="">View</span></a>
                                                    <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="edit-2" class="icon-sm ml-2"></i> <span class="">Edit</span></a>
                                                    <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="trash" class="icon-sm ml-2"></i> <span class="">Delete</span></a>
                                                    <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="printer" class="icon-sm ml-2"></i> <span class="">Print</span></a>
                                                    <a class="dropdown-item d-flex align-items-center" href="#"><i data-feather="download" class="icon-sm ml-2"></i> <span class="">Download</span></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6 col-md-12 col-xl-5">
                                                <h3 class="mb-2">5k</h3>
                                                <div class="d-flex align-items-baseline">
                                                    <p class="text-success">
                                                        <span>+21.4%</span>
                                                        <i data-feather="arrow-up" class="icon-sm mb-1"></i>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-6 col-md-12 col-xl-7">
                                                <div id="apexChart1" class="mt-md-3 mt-xl-0"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>



                  
                </div>
            </div>
            <!-- right wrapper end -->
        </div>
    </div>

</div>

@endsection