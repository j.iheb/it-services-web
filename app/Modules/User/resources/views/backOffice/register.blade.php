@extends('backOffice.layout')

@section('head')
@include('backOffice.inc.head',
['title' => 'Dashboard',
'description' => 'Espace Administratif - '
])
@endsection

@section('header')
@include('backOffice.inc.header')
@endsection

@section('sidebar')
@include('backOffice.inc.sidebar', [
'current' => 'services'
])
@endsection

@section('content')


<div class="page-content">

    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                <div class="row">
                        <div class="col-md-9">
                            <div class="breadcrumb">
                                <h4>{{ ucfirst( trans('lang.add_client'))}}</h4>
                                
                            </div>
                        </div>
                        <div class="col-md-3 pull-right">
                            <div class="float-right">
                                <a href="{{route('showManagerUsers',['lang' => $lang])}}" class="btn btn-primary">{{ ucwords(trans('lang.users')) }}</a>
                            </div>
                        </div>
                    </div>

  
    <form id="form"  method="POST" action="{{ route('handleUserRegister',['lang' => $lang]) }}" enctype="multipart/form-data">
        {{ csrf_field() }} 
        <div class="row mt-5" >

            <div class="col-md-6 form-group mb-3">
                <label for="title">{{ucfirst( trans('lang.full_name'))}}</label>
                <input class="form-control" type="text"  name="name" id="title"  placeholder="{{trans('lang.full_name')}}"  @isset($user) value="{{$user->name}}" @endisset  required>
              
            </div>

          

            <div class="col-md-6 form-group mb-3">
                <label for="title">{{ucfirst( trans('lang.email'))}}</label>
                <input class="form-control" type="email"  name="email" id="title"  placeholder="{{trans('lang.email')}}" @isset($user) value="{{$user->email}}" @endisset required >
              
            </div>

         

            <div class="col-md-6 form-group mb-3">
                <label for="title">{{ucfirst( trans('lang.phone'))}}</label>
                <input class="form-control" type="phone"  name="phone" id="title"  placeholder="{{trans('lang.phone')}}" @isset($user) value="{{$user->phone}}" @endisset required >
              
            </div>

        

            <div class="col-md-6 form-group mb-3">
                <label for="title">{{ucfirst( trans('lang.title'))}}</label>
                <input class="form-control" type="text"  name="title" id="title"  placeholder="{{trans('lang.title')}}"  @isset($user) value="{{$user->title}}" @endisset >
              
            </div>




            <div class="col-md-6 form-group mb-3">
                <label for="title">{{ucfirst( trans('lang.address'))}}</label>
                <input class="form-control" type="text"  name="address" id="title"  placeholder="{{trans('lang.address')}}" @isset($user) value="{{$user->address}}" @endisset >
              
            </div>
         

            <div class="col-md-6 form-group mb-3">
                <label for="description">{{ucfirst( trans ('lang.short_description'))}}</label>
                <textarea class="form-control" name="description" id="short_description"  rows="5"  placeholder="{{trans('lang.description')}}" >@isset($user) {{$user->description}} @endisset</textarea>
            </div>


           


         
         
            <div class="col-md-6 form-group mb-3">
                <label for="cover">{{ucfirst( trans ('lang.featured_picture'))}}</label>
                <input class="form-control-file" type="file" name="photo" id="cover" accept=".jpg,.jpeg,.png" >
            </div>
            <div class="col-md-12">
                
                 <button type="submit" class="btn btn-primary"> {{ucfirst(trans('lang.send'))}} </button>
            </div>
        </div>
    </form>
</div>
            </div>
        </div></div>
<script>
$("#form").validate({
    ignore: ":hidden",
    errorClass: "danger is-invalid",
    validClass: "success is-valid",
    highlight: function (element, errorClass, validClass) {
        $(element).addClass(errorClass).removeClass(validClass);
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass(errorClass).addClass(validClass);
    },
    errorPlacement: function (i, e) {
        i.insertAfter(e)
    },
    rules: {
        "name": {
            required: true,
            minlength: 3,
           
        },
   
        "featured_picture": {
            extension: "jpg|jpeg|png",
            fileSizeMax: 2048000
        }
    },
    messages: {
        "name": {
            required: "{{ trans('lang.required', ['string' =>  ucfirst(trans('shared.name'))]) }}",
            minlength: "{{ trans('lang.min_string', ['string' =>   ucfirst(trans('shared.name')), 'min' => 3]) }}",
        },
  
    }
});
</script>
</div>
@endsection
