@extends('backOffice.layout')

@section('head')
@include('backOffice.inc.head',
['title' => 'Dashboard',
'description' => 'Espace Administratif - '
])
@endsection

@section('header')
@include('backOffice.inc.header')
@endsection

@section('sidebar')
@include('backOffice.inc.sidebar', [
'current' => 'services'
])
@endsection

@section('content')

<div class="page-content">


    <link rel="stylesheet" type="text/css" href="{{ asset('plugins') }}/datatable/datatable.css">
    <script type="text/javascript" src="{{ asset('plugins') }}/datatable/datatable.js"></script>

    <script type="text/javascript">
        /* Datatables responsive */

        $(document).ready(function() {
            $('#datatable-responsive').DataTable({
                responsive: true,
                language: {
                    url: "{{ asset('plugins/datatable/lang/'.\Illuminate\Support\Facades\Session::get('youfors_applocale').'.js') }}"
                }
            });
            $('.dataTables_filter input').attr("placeholder", "{{ trans('lang.search') }}...");
        });
    </script>








    <nav class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">{{ trans('lang.dashboard') }}</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ trans('lang.users') }}</li>
        </ol>
    </nav>

    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="breadcrumb">
                                <h4>{{ trans('lang.users') }} </h4>
                                
                            </div>
                        </div>
                        <div class="col-md-3 pull-right">
                            <div class="float-right">
                                <a href="{{route('showManagerAddUser',['lang' => $lang])}}" class="btn btn-primary">{{ ucwords(trans('lang.register')) }}</a>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table id="dataTableExample" id="datatable-responsive" class="table">
                            <thead>
                                <tr>
                                    <th>{{ trans('lang.full_name') }}</th>
                                    <th>{{ trans('lang.email') }}</th>
                                    <th>{{ trans('lang.status') }}</th>
                                    <th>{{ trans('lang.roles') }}</th>
                                    <th>{{ trans('lang.date') }}</th>
                                    <th>{{ trans('lang.action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>
                                        <span class="badge badge-pill badge badge-success p-2 m-1">{{ transformUserStatus($user->status) }}</span>
                                    </td>
                                    <td>
                                        @foreach($user->roles as $role)
                                        <span class="badge badge-pill p-2 m-1
                                                 @if($role->id == 1)
                                                badge-outline-primary
                                                @elseif($role->id == 2)
                                                badge-outline-info
                                                @else
                                                badge-outline-danger
                                                @endif
                                                ">{{ transformRoleStatus($role->id) }}
                                        </span> &nbsp;
                                        @endforeach
                                    </td>
                                    <td>{{ $user->created_at->format('d-M-Y H:m:s') }}</td>
                                    <td>
                                        @if($user->status == 1)
                                        <span class="badge badge-pill badge-danger p-2 m-1" title="{{ trans('lang.user_ban_action') }}" onclick="location='{{ route('handleManagerBanUser', [ 'lang' => $lang, 'id' => $user->id]) }}';"> <i class="link-icon" data-feather="lock"></i ></span>
                                        @else
                                        <span class="badge badge-pill badge-info p-2 m-1" title="{{ trans('lang.user_authorize_action') }}" onclick="location='{{ route('handleManagerAllowUser', [ 'lang' => $lang,'id' => $user->id]) }}';"><i class="link-icon" data-feather="unlock"></i > </span>
                                        @endif
                                        @if(checkAdministratorRole($user))
                                        <span class="badge badge-pill badge-secondary p-2 m-1" title="{{ trans('lang.user_revoke_action') }}" onclick="location='{{ route('handleManagerRevokeManager', [ 'lang' => $lang,'id' => $user->id]) }}';"> <i class="link-icon" data-feather="minus-circle"></i> </span>
                                        @else
                                        <span class="badge badge-pill badge-secondary p-2 m-1" title="{{ trans('lang.user_admin_action') }}" onclick="location='{{ route('handleManagerAuthorizeManager', ['lang' => $lang, 'id' => $user->id]) }}';"> <i class="link-icon" data-feather="plus-circle"></i ></span>
                                        @endif




                                        <span class="badge badge-pill badge-primary p-2 m-1" title="revoke adviser" onclick="location='{{ route('showManagerProfile', ['lang' => $lang,'id' => $user->id]) }}';"><i class="link-icon" data-feather="eye"></i>  </span>
                                        <span class="badge badge-pill badge-primary p-2 m-1" title="revoke adviser" onclick="location='{{ route('showManagerEditUser', ['lang' => $lang,'id' => $user->id]) }}';"><i class="link-icon" data-feather="edit"></i>  </span>
                                        




                                    </td>
                                </tr>
                                @endforeach


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection