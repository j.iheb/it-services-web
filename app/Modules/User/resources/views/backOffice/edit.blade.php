@extends('backOffice.layout')

@section('head')
@include('backOffice.inc.head',
['title' => 'Dashboard',
'description' => 'Espace Administratif - '
])
@endsection

@section('header')
@include('backOffice.inc.header')
@endsection

@section('sidebar')
@include('backOffice.inc.sidebar', [
'current' => 'consultations'
])
@endsection

@section('content')

<div class="row">
        <div class="col-md-8">
            <div class="breadcrumb">
                <h1>{{ucfirst( trans('lang.user')) }}</h1>
                <ul>
                    <li><a href="#">{{ucfirst(trans('lang.dashboard')) }} </a></li>
                    <li>{{ucfirst( trans('lang.users')) }}</li>
                </ul>
            </div>
        </div>
        <div class="col-md-4">
            <div style="" class="btn btn-primary float-right">
                <a style="color: white"  href="{{route('handleManagerEditUser', ['lang' => $lang])}}">{{ucfirst(trans('lang.about_us')) }} </a>

            </div>
        </div>
    </div>

<div class="card-body">
    <div class="card-title mb-3">{{ ucfirst( trans('lang.add_service'))}}</div>
    <form id="form"  method="POST" action="{{route('handleManagerEditUser',['lang' => $lang])}}" enctype="multipart/form-data">
        {{ csrf_field() }} 
        <div class="row" >

            <input type="hidden" name="id" value="{{$user->id}}">
            
            <div class="col-md-6 form-group mb-3">
                <label for="title">{{ucfirst( trans('lang.name'))}}</label>
                <input class="form-control" type="text"  name="name" id="title"  placeholder="{{trans('lang.tile_placeholder')}}" value="{{$user->name}}" required>
              
            </div>

            <div class="col-md-6 form-group mb-3">
                <label for="title">{{ucfirst( trans('lang.name_ar'))}}</label>
                <input class="form-control" type="text"  name="name_ar" id="title"  placeholder="{{trans('lang.tile_placeholder')}}" value="{{$user->name_ar}}" >
              
            </div>

            <div class="col-md-6 form-group mb-3">
                <label for="title">{{ucfirst( trans('lang.title'))}}</label>
                <input class="form-control" type="text"  name="title" id="title"  placeholder="{{trans('lang.tile_placeholder')}}" value="{{$user->title}}" >
              
            </div>

            <div class="col-md-6 form-group mb-3">
                <label for="title">{{ucfirst( trans('lang.title_ar'))}}</label>
                <input class="form-control" type="text"  name="title_ar" id="title"  placeholder="{{trans('lang.tile_placeholder')}}" value="{{$user->title_ar}}" >
              
            </div>


            <div class="col-md-6 form-group mb-3">
                <label for="description">{{ucfirst( trans ('lang.short_description'))}}</label>
                <textarea class="form-control" name="short_description" id="short_description"  rows="5"  placeholder="{{trans('lang.description_placeholder')}}" > {{$user->short_description}}</textarea>
            </div>

            <div class="col-md-6 form-group mb-3">
                <label for="description">{{ucfirst( trans ('lang.short_description_ar'))}}</label>
                <textarea class="form-control" name="short_description_ar" id="short_description"  rows="5"  placeholder="{{trans('lang.description_placeholder')}}" > {{$user->short_description_ar}}</textarea>
            </div>

           


         
         
            <div class="col-md-6 form-group mb-3">
                <label for="cover">{{ucfirst( trans ('lang.featured_picture'))}}</label>
                <input class="form-control-file" type="file" name="photo" id="cover" accept=".jpg,.jpeg,.png" >
            </div>
            <div class="col-md-12">
                <input type="submit" class="btn btn-primary" value="Submit">
            </div>
        </div>
    </form>
</div>

<script>
$("#form").validate({
    ignore: ":hidden",
    errorClass: "danger is-invalid",
    validClass: "success is-valid",
    highlight: function (element, errorClass, validClass) {
        $(element).addClass(errorClass).removeClass(validClass);
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass(errorClass).addClass(validClass);
    },
    errorPlacement: function (i, e) {
        i.insertAfter(e)
    },
    rules: {
        "name": {
            required: true,
            minlength: 3,
           
        },
   
        "featured_picture": {
            extension: "jpg|jpeg|png",
            fileSizeMax: 2048000
        }
    },
    messages: {
        "name": {
            required: "{{ trans('lang.required', ['string' =>  ucfirst(trans('shared.name'))]) }}",
            minlength: "{{ trans('lang.min_string', ['string' =>   ucfirst(trans('shared.name')), 'min' => 3]) }}",
        },
  
    }
});
</script>
@endsection
