<?php

/**
 *	User Helper
 */
if (!function_exists('transformUserStatus')) {
    /**
     * @param int $status
     * @return string
     */
    function transformUserStatus($status)
    {
        switch ($status){
            case 0 : return trans('lang.user_status_validation');
            case 1 : return trans('lang.user_status_valid');
            case 3 : return trans('lang.user_status_banned');
            default : return null;
        }
    }
}


if (!function_exists('transformRoleStatus')) {
    /**
     * @param int $status
     * @return string
     */
    function transformRoleStatus($status)
    {
        switch ($status){
            case 1 : return trans('lang.user');
            case 2 : return trans('lang.administrateur');
            case 4 : return trans('lang.adviser');
            case 3 : return trans('lang.officer');
            default : return null;
        }
    }
}
