<?php

namespace Database\Seeders;


use App\Modules\General\Models\Country;
use App\Modules\General\Models\Currency;

use App\Modules\User\Models\Role;
use App\Modules\User\Models\User;

use Illuminate\Database\Seeder;

class GeneralSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     

        Currency::create(['priority' => '100',
            'iso_code' => 'AED',
            'title' => 'United Arab Emirates Dirham',
            'symbol' => 'د.إ',
            'subunit' => 'Fils',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '784']);

        Currency::create(['priority' => '100',
            'iso_code' => 'AFN',
            'title' => 'Afghan Afghani',
            'symbol' => '؋',
            'subunit' => 'Pul',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '971']);

        Currency::create(['priority' => '100',
            'iso_code' => 'ALL',
            'title' => 'Albanian Lek',
            'symbol' => 'L',
            'subunit' => 'Qintar',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '008']);

        Currency::create(['priority' => '100',
            'iso_code' => 'AMD',
            'title' => 'Armenian Dram',
            'symbol' => 'դր.',
            'subunit' => 'Luma',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '051']);

        Currency::create(['priority' => '100',
            'iso_code' => 'ANG',
            'title' => 'Netherlands Antillean Gulden',
            'symbol' => 'ƒ',
            'subunit' => 'Cent',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '&#x0192;',
            'decimal_mark' => ',',
            'thousands_separator' => '.',
            'iso_numeric' => '532']);

        Currency::create(['priority' => '100',
            'iso_code' => 'AOA',
            'title' => 'Angolan Kwanza',
            'symbol' => 'Kz',
            'subunit' => 'Cêntimo',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '973']);

        Currency::create(['priority' => '100',
            'iso_code' => 'ARS',
            'title' => 'Argentine Peso',
            'symbol' => '$',
            'subunit' => 'Centavo',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '&#x20B1;',
            'decimal_mark' => ',',
            'thousands_separator' => '.',
            'iso_numeric' => '032']);

        Currency::create(['priority' => '4',
            'iso_code' => 'AUD',
            'title' => 'Australian Dollar',
            'symbol' => '$',
            'subunit' => 'Cent',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '$',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '036']);

        Currency::create(['priority' => '100',
            'iso_code' => 'AWG',
            'title' => 'Aruban Florin',
            'symbol' => 'ƒ',
            'subunit' => 'Cent',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '&#x0192;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '533']);

        Currency::create(['priority' => '100',
            'iso_code' => 'AZN',
            'title' => 'Azerbaijani Manat',
            'symbol' => 'null',
            'subunit' => 'Qəpik',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '944']);

        Currency::create(['priority' => '100',
            'iso_code' => 'BAM',
            'title' => 'Bosnia and Herzegovina Convertible Mark',
            'symbol' => 'КМ',
            'subunit' => 'Fening',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '977']);

        Currency::create(['priority' => '100',
            'iso_code' => 'BBD',
            'title' => 'Barbadian Dollar',
            'symbol' => '$',
            'subunit' => 'Cent',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '$',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '052']);

        Currency::create(['priority' => '100',
            'iso_code' => 'BDT',
            'title' => 'Bangladeshi Taka',
            'symbol' => '৳',
            'subunit' => 'Paisa',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '050']);

        Currency::create(['priority' => '100',
            'iso_code' => 'BGN',
            'title' => 'Bulgarian Lev',
            'symbol' => 'лв',
            'subunit' => 'Stotinka',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '975']);
        Currency::create(['priority' => '100',
            'iso_code' => 'BHD',
            'title' => 'Bahraini Dinar',
            'symbol' => 'ب.د',
            'subunit' => 'Fils',
            'subunit_to_unit' => '1000',
            'symbol_first' => true,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '048']);
        Currency::create(['priority' => '100',
            'iso_code' => 'BIF',
            'title' => 'Burundian Franc',
            'symbol' => 'Fr',
            'subunit' => 'Centime',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '108']);
        Currency::create(['priority' => '100',
            'iso_code' => 'BMD',
            'title' => 'Bermudian Dollar',
            'symbol' => '$',
            'subunit' => 'Cent',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '$',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '060']);
        Currency::create(['priority' => '100',
            'iso_code' => 'BND',
            'title' => 'Brunei Dollar',
            'symbol' => '$',
            'subunit' => 'Sen',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '$',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '096']);
        Currency::create(['priority' => '100',
            'iso_code' => 'BOB',
            'title' => 'Bolivian Boliviano',
            'symbol' => 'Bs.',
            'subunit' => 'Centavo',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '068']);
        Currency::create(['priority' => '100',
            'iso_code' => 'BRL',
            'title' => 'Brazilian Real',
            'symbol' => 'R$',
            'subunit' => 'Centavo',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => 'R$',
            'decimal_mark' => ',',
            'thousands_separator' => '.',
            'iso_numeric' => '986']);
        Currency::create(['priority' => '100',
            'iso_code' => 'BSD',
            'title' => 'Bahamian Dollar',
            'symbol' => '$',
            'subunit' => 'Cent',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '$',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '044']);
        Currency::create(['priority' => '100',
            'iso_code' => 'BTN',
            'title' => 'Bhutanese Ngultrum',
            'symbol' => 'Nu.',
            'subunit' => 'Chertrum',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '064']);
        Currency::create(['priority' => '100',
            'iso_code' => 'BWP',
            'title' => 'Botswana Pula',
            'symbol' => 'P',
            'subunit' => 'Thebe',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '072']);
        Currency::create(['priority' => '100',
            'iso_code' => 'BYR',
            'title' => 'Belarusian Ruble',
            'symbol' => 'Br',
            'subunit' => 'Kapyeyka',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '974']);
        Currency::create(['priority' => '100',
            'iso_code' => 'BZD',
            'title' => 'Belize Dollar',
            'symbol' => '$',
            'subunit' => 'Cent',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '$',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '084']);
        Currency::create(['priority' => '5',
            'iso_code' => 'CAD',
            'title' => 'Canadian Dollar',
            'symbol' => '$',
            'subunit' => 'Cent',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '$',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '124']);
        Currency::create(['priority' => '100',
            'iso_code' => 'CDF',
            'title' => 'Congolese Franc',
            'symbol' => 'Fr',
            'subunit' => 'Centime',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '976']);
        Currency::create(['priority' => '100',
            'iso_code' => 'CHF',
            'title' => 'Swiss Franc',
            'symbol' => 'Fr',
            'subunit' => 'Rappen',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '756']);
        Currency::create(['priority' => '100',
            'iso_code' => 'CLF',
            'title' => 'Unidad de Fomento',
            'symbol' => 'UF',
            'subunit' => 'Peso',
            'subunit_to_unit' => '1',
            'symbol_first' => true,
            'html_entity' => '&#x20B1;',
            'decimal_mark' => ',',
            'thousands_separator' => '.',
            'iso_numeric' => '990']);

        Currency::create(['priority' => '100',
            'iso_code' => 'CLP',
            'title' => 'Chilean Peso',
            'symbol' => '$',
            'subunit' => 'Peso',
            'subunit_to_unit' => '1',
            'symbol_first' => true,
            'html_entity' => '&#36;',
            'decimal_mark' => ',',
            'thousands_separator' => '.',
            'iso_numeric' => '152']);

        Currency::create(['priority' => '100',
            'iso_code' => 'CNY',
            'title' => 'Chinese Renminbi Yuan',
            'symbol' => '¥',
            'subunit' => 'Fen',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '&#20803;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '156']);
        Currency::create(['priority' => '100',
            'iso_code' => 'COP',
            'title' => 'Colombian Peso',
            'symbol' => '$',
            'subunit' => 'Centavo',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '&#x20B1;',
            'decimal_mark' => ',',
            'thousands_separator' => '.',
            'iso_numeric' => '170']);
        Currency::create(['priority' => '100',
            'iso_code' => 'CRC',
            'title' => 'Costa Rican Colón',
            'symbol' => '₡',
            'subunit' => 'Céntimo',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '&#x20A1;',
            'decimal_mark' => ',',
            'thousands_separator' => '.',
            'iso_numeric' => '188']);
        Currency::create(['priority' => '100',
            'iso_code' => 'CUC',
            'title' => 'Cuban Convertible Peso',
            'symbol' => '$',
            'subunit' => 'Centavo',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '931']);
        Currency::create(['priority' => '100',
            'iso_code' => 'CUP',
            'title' => 'Cuban Peso',
            'symbol' => '$',
            'subunit' => 'Centavo',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '&#x20B1;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '192']);
        Currency::create(['priority' => '100',
            'iso_code' => 'CVE',
            'title' => 'Cape Verdean Escudo',
            'symbol' => '$',
            'subunit' => 'Centavo',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '132']);
        Currency::create(['priority' => '100',
            'iso_code' => 'CZK',
            'title' => 'Czech Koruna',
            'symbol' => 'Kč',
            'subunit' => 'Haléř',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '',
            'decimal_mark' => ',',
            'thousands_separator' => '.',
            'iso_numeric' => '203']);
        Currency::create(['priority' => '100',
            'iso_code' => 'DJF',
            'title' => 'Djiboutian Franc',
            'symbol' => 'Fdj',
            'subunit' => 'Centime',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '262']);
        Currency::create(['priority' => '100',
            'iso_code' => 'DKK',
            'title' => 'Danish Krone',
            'symbol' => 'kr',
            'subunit' => 'Øre',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => ',',
            'thousands_separator' => '.',
            'iso_numeric' => '208']);
        Currency::create(['priority' => '100',
            'iso_code' => 'DOP',
            'title' => 'Dominican Peso',
            'symbol' => '$',
            'subunit' => 'Centavo',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '&#x20B1;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '214']);
        Currency::create(['priority' => '100',
            'iso_code' => 'DZD',
            'title' => 'Algerian Dinar',
            'symbol' => 'د.ج',
            'subunit' => 'Centime',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '012']);
        Currency::create(['priority' => '100',
            'iso_code' => 'EGP',
            'title' => 'Egyptian Pound',
            'symbol' => 'ج.م',
            'subunit' => 'Piastre',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '&#x00A3;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '818']);
        Currency::create(['priority' => '100',
            'iso_code' => 'ERN',
            'title' => 'Eritrean Nakfa',
            'symbol' => 'Nfk',
            'subunit' => 'Cent',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '232']);
        Currency::create(['priority' => '100',
            'iso_code' => 'ETB',
            'title' => 'Ethiopian Birr',
            'symbol' => 'Br',
            'subunit' => 'Santim',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '230']);
        Currency::create(['priority' => '2',
            'iso_code' => 'EUR',
            'title' => 'Euro',
            'symbol' => '€',
            'subunit' => 'Cent',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '&#x20AC;',
            'decimal_mark' => ',',
            'thousands_separator' => '.',
            'iso_numeric' => '978']);
        Currency::create(['priority' => '100',
            'iso_code' => 'FJD',
            'title' => 'Fijian Dollar',
            'symbol' => '$',
            'subunit' => 'Cent',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '$',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '242']);
        Currency::create(['priority' => '100',
            'iso_code' => 'FKP',
            'title' => 'Falkland Pound',
            'symbol' => '£',
            'subunit' => 'Penny',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '&#x00A3;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '238']);
        Currency::create(['priority' => '3',
            'iso_code' => 'GBP',
            'title' => 'British Pound',
            'symbol' => '£',
            'subunit' => 'Penny',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '&#x00A3;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '826']);

        Currency::create(['priority' => '100',
            'iso_code' => 'GEL',
            'title' => 'Georgian Lari',
            'symbol' => 'ლ',
            'subunit' => 'Tetri',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '981']);

        Currency::create(['priority' => '100',
            'iso_code' => 'GHS',
            'title' => 'Ghanaian Cedi',
            'symbol' => '₵',
            'subunit' => 'Pesewa',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '&#x20B5;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '936']);
        Currency::create(['priority' => '100',
            'iso_code' => 'GIP',
            'title' => 'Gibraltar Pound',
            'symbol' => '£',
            'subunit' => 'Penny',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '&#x00A3;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '292']);
        Currency::create(['priority' => '100',
            'iso_code' => 'GMD',
            'title' => 'Gambian Dalasi',
            'symbol' => 'D',
            'subunit' => 'Butut',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '270']);
        Currency::create(['priority' => '100',
            'iso_code' => 'GNF',
            'title' => 'Guinean Franc',
            'symbol' => 'Fr',
            'subunit' => 'Centime',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '324']);
        Currency::create(['priority' => '100',
            'iso_code' => 'GTQ',
            'title' => 'Guatemalan Quetzal',
            'symbol' => 'Q',
            'subunit' => 'Centavo',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '320']);
        Currency::create(['priority' => '100',
            'iso_code' => 'GYD',
            'title' => 'Guyanese Dollar',
            'symbol' => '$',
            'subunit' => 'Cent',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '$',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '328']);
        Currency::create(['priority' => '100',
            'iso_code' => 'HKD',
            'title' => 'Hong Kong Dollar',
            'symbol' => '$',
            'subunit' => 'Cent',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '$',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '344']);
        Currency::create(['priority' => '100',
            'iso_code' => 'HNL',
            'title' => 'Honduran Lempira',
            'symbol' => 'L',
            'subunit' => 'Centavo',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '340']);
        Currency::create(['priority' => '100',
            'iso_code' => 'HRK',
            'title' => 'Croatian Kuna',
            'symbol' => 'kn',
            'subunit' => 'Lipa',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '',
            'decimal_mark' => ',',
            'thousands_separator' => '.',
            'iso_numeric' => '191']);
        Currency::create(['priority' => '100',
            'iso_code' => 'HTG',
            'title' => 'Haitian Gourde',
            'symbol' => 'G',
            'subunit' => 'Centime',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '332']);
        Currency::create(['priority' => '100',
            'iso_code' => 'HUF',
            'title' => 'Hungarian Forint',
            'symbol' => 'Ft',
            'subunit' => 'Fillér',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => ',',
            'thousands_separator' => '.',
            'iso_numeric' => '348']);
        Currency::create(['priority' => '100',
            'iso_code' => 'IDR',
            'title' => 'Indonesian Rupiah',
            'symbol' => 'Rp',
            'subunit' => 'Sen',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '',
            'decimal_mark' => ',',
            'thousands_separator' => '.',
            'iso_numeric' => '360']);
        Currency::create(['priority' => '100',
            'iso_code' => 'ILS',
            'title' => 'Israeli New Sheqel',
            'symbol' => '₪',
            'subunit' => 'Agora',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '&#x20AA;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '376']);
        Currency::create(['priority' => '100',
            'iso_code' => 'INR',
            'title' => 'Indian Rupee',
            'symbol' => '₹',
            'subunit' => 'Paisa',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '&#x20b9;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '356']);
        Currency::create(['priority' => '100',
            'iso_code' => 'IQD',
            'title' => 'Iraqi Dinar',
            'symbol' => 'ع.د',
            'subunit' => 'Fils',
            'subunit_to_unit' => '1000',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '368']);
        Currency::create(['priority' => '100',
            'iso_code' => 'IRR',
            'title' => 'Iranian Rial',
            'symbol' => '﷼',
            'subunit' => 'Dinar',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '&#xFDFC;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '364']);
        Currency::create(['priority' => '100',
            'iso_code' => 'ISK',
            'title' => 'Icelandic Króna',
            'symbol' => 'kr',
            'subunit' => 'Eyrir',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '',
            'decimal_mark' => ',',
            'thousands_separator' => '.',
            'iso_numeric' => '352']);
        Currency::create(['priority' => '100',
            'iso_code' => 'JMD',
            'title' => 'Jamaican Dollar',
            'symbol' => '$',
            'subunit' => 'Cent',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '$',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '388']);
        Currency::create(['priority' => '100',
            'iso_code' => 'JOD',
            'title' => 'Jordanian Dinar',
            'symbol' => 'د.ا',
            'subunit' => 'Piastre',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '400']);
        Currency::create(['priority' => '6',
            'iso_code' => 'JPY',
            'title' => 'Japanese Yen',
            'symbol' => '¥',
            'subunit' => 'null',
            'subunit_to_unit' => '1',
            'symbol_first' => true,
            'html_entity' => '&#x00A5;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '392']);
        Currency::create(['priority' => '100',
            'iso_code' => 'KES',
            'title' => 'Kenyan Shilling',
            'symbol' => 'KSh',
            'subunit' => 'Cent',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '404']);
        Currency::create(['priority' => '100',
            'iso_code' => 'KGS',
            'title' => 'Kyrgyzstani Som',
            'symbol' => 'som',
            'subunit' => 'Tyiyn',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '417']);
        Currency::create(['priority' => '100',
            'iso_code' => 'KHR',
            'title' => 'Cambodian Riel',
            'symbol' => '៛',
            'subunit' => 'Sen',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '&#x17DB;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '116']);
        Currency::create(['priority' => '100',
            'iso_code' => 'KMF',
            'title' => 'Comorian Franc',
            'symbol' => 'Fr',
            'subunit' => 'Centime',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '174']);
        Currency::create(['priority' => '100',
            'iso_code' => 'KPW',
            'title' => 'North Korean Won',
            'symbol' => '₩',
            'subunit' => 'Chŏn',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '&#x20A9;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '408']);
        Currency::create(['priority' => '100',
            'iso_code' => 'KRW',
            'title' => 'South Korean Won',
            'symbol' => '₩',
            'subunit' => 'null',
            'subunit_to_unit' => '1',
            'symbol_first' => true,
            'html_entity' => '&#x20A9;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '410']);
        Currency::create(['priority' => '100',
            'iso_code' => 'KWD',
            'title' => 'Kuwaiti Dinar',
            'symbol' => 'د.ك',
            'subunit' => 'Fils',
            'subunit_to_unit' => '1000',
            'symbol_first' => true,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '414']);
        Currency::create(['priority' => '100',
            'iso_code' => 'KYD',
            'title' => 'Cayman Islands Dollar',
            'symbol' => '$',
            'subunit' => 'Cent',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '$',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '136']);
        Currency::create(['priority' => '100',
            'iso_code' => 'KZT',
            'title' => 'Kazakhstani Tenge',
            'symbol' => '〒',
            'subunit' => 'Tiyn',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '398']);
        Currency::create(['priority' => '100',
            'iso_code' => 'LAK',
            'title' => 'Lao Kip',
            'symbol' => '₭',
            'subunit' => 'Att',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '&#x20AD;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '418']);
        Currency::create(['priority' => '100',
            'iso_code' => 'LBP',
            'title' => 'Lebanese Pound',
            'symbol' => 'ل.ل',
            'subunit' => 'Piastre',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '&#x00A3;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '422']);
        Currency::create(['priority' => '100',
            'iso_code' => 'LKR',
            'title' => 'Sri Lankan Rupee',
            'symbol' => '₨',
            'subunit' => 'Cent',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '&#x0BF9;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '144']);
        Currency::create(['priority' => '100',
            'iso_code' => 'LRD',
            'title' => 'Liberian Dollar',
            'symbol' => '$',
            'subunit' => 'Cent',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '$',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '430']);
        Currency::create(['priority' => '100',
            'iso_code' => 'LSL',
            'title' => 'Lesotho Loti',
            'symbol' => 'L',
            'subunit' => 'Sente',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '426']);
        Currency::create(['priority' => '100',
            'iso_code' => 'LTL',
            'title' => 'Lithuanian Litas',
            'symbol' => 'Lt',
            'subunit' => 'Centas',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '440']);
        Currency::create(['priority' => '100',
            'iso_code' => 'LVL',
            'title' => 'Latvian Lats',
            'symbol' => 'Ls',
            'subunit' => 'Santīms',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '428']);
        Currency::create(['priority' => '100',
            'iso_code' => 'LYD',
            'title' => 'Libyan Dinar',
            'symbol' => 'ل.د',
            'subunit' => 'Dirham',
            'subunit_to_unit' => '1000',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '434']);
        Currency::create(['priority' => '100',
            'iso_code' => 'MAD',
            'title' => 'Moroccan Dirham',
            'symbol' => 'د.م.',
            'subunit' => 'Centime',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '504']);
        Currency::create(['priority' => '100',
            'iso_code' => 'MDL',
            'title' => 'Moldovan Leu',
            'symbol' => 'L',
            'subunit' => 'Ban',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '498']);
        Currency::create(['priority' => '100',
            'iso_code' => 'MGA',
            'title' => 'Malagasy Ariary',
            'symbol' => 'Ar',
            'subunit' => 'Iraimbilanja',
            'subunit_to_unit' => '5',
            'symbol_first' => true,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '969']);
        Currency::create(['priority' => '100',
            'iso_code' => 'MKD',
            'title' => 'Macedonian Denar',
            'symbol' => 'ден',
            'subunit' => 'Deni',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '807']);
        Currency::create(['priority' => '100',
            'iso_code' => 'MMK',
            'title' => 'Myanmar Kyat',
            'symbol' => 'K',
            'subunit' => 'Pya',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '104']);
        Currency::create(['priority' => '100',
            'iso_code' => 'MNT',
            'title' => 'Mongolian Tögrög',
            'symbol' => '₮',
            'subunit' => 'Möngö',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '&#x20AE;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '496']);
        Currency::create(['priority' => '100',
            'iso_code' => 'MOP',
            'title' => 'Macanese Pataca',
            'symbol' => 'P',
            'subunit' => 'Avo',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '446']);
        Currency::create(['priority' => '100',
            'iso_code' => 'MRO',
            'title' => 'Mauritanian Ouguiya',
            'symbol' => 'UM',
            'subunit' => 'Khoums',
            'subunit_to_unit' => '5',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '478']);
        Currency::create(['priority' => '100',
            'iso_code' => 'MUR',
            'title' => 'Mauritian Rupee',
            'symbol' => '₨',
            'subunit' => 'Cent',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '&#x20A8;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '480']);
        Currency::create(['priority' => '100',
            'iso_code' => 'MVR',
            'title' => 'Maldivian Rufiyaa',
            'symbol' => 'MVR',
            'subunit' => 'Laari',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '462']);
        Currency::create(['priority' => '100',
            'iso_code' => 'MWK',
            'title' => 'Malawian Kwacha',
            'symbol' => 'MK',
            'subunit' => 'Tambala',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '454']);
        Currency::create(['priority' => '100',
            'iso_code' => 'MXN',
            'title' => 'Mexican Peso',
            'symbol' => '$',
            'subunit' => 'Centavo',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '$',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '484']);
        Currency::create(['priority' => '100',
            'iso_code' => 'MYR',
            'title' => 'Malaysian Ringgit',
            'symbol' => 'RM',
            'subunit' => 'Sen',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '458']);
        Currency::create(['priority' => '100',
            'iso_code' => 'MZN',
            'title' => 'Mozambican Metical',
            'symbol' => 'MTn',
            'subunit' => 'Centavo',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '',
            'decimal_mark' => ',',
            'thousands_separator' => '.',
            'iso_numeric' => '943']);
        Currency::create(['priority' => '100',
            'iso_code' => 'NAD',
            'title' => 'Namibian Dollar',
            'symbol' => '$',
            'subunit' => 'Cent',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '$',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '516']);
        Currency::create(['priority' => '100',
            'iso_code' => 'NGN',
            'title' => 'Nigerian Naira',
            'symbol' => '₦',
            'subunit' => 'Kobo',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '&#x20A6;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '566']);
        Currency::create(['priority' => '100',
            'iso_code' => 'NIO',
            'title' => 'Nicaraguan Córdoba',
            'symbol' => 'C$',
            'subunit' => 'Centavo',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '558']);
        Currency::create(['priority' => '100',
            'iso_code' => 'NOK',
            'title' => 'Norwegian Krone',
            'symbol' => 'kr',
            'subunit' => 'Øre',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => 'kr',
            'decimal_mark' => ',',
            'thousands_separator' => '.',
            'iso_numeric' => '578']);
        Currency::create(['priority' => '100',
            'iso_code' => 'NPR',
            'title' => 'Nepalese Rupee',
            'symbol' => '₨',
            'subunit' => 'Paisa',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '&#x20A8;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '524']);
        Currency::create(['priority' => '100',
            'iso_code' => 'NZD',
            'title' => 'New Zealand Dollar',
            'symbol' => '$',
            'subunit' => 'Cent',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '$',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '554']);
        Currency::create(['priority' => '100',
            'iso_code' => 'OMR',
            'title' => 'Omani Rial',
            'symbol' => 'ر.ع.',
            'subunit' => 'Baisa',
            'subunit_to_unit' => '1000',
            'symbol_first' => true,
            'html_entity' => '&#xFDFC;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '512']);
        Currency::create(['priority' => '100',
            'iso_code' => 'PAB',
            'title' => 'Panamanian Balboa',
            'symbol' => 'B/.',
            'subunit' => 'Centésimo',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '590']);
        Currency::create(['priority' => '100',
            'iso_code' => 'PEN',
            'title' => 'Peruvian Nuevo Sol',
            'symbol' => 'S/.',
            'subunit' => 'Céntimo',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => 'S/.',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '604']);
        Currency::create(['priority' => '100',
            'iso_code' => 'PGK',
            'title' => 'Papua New Guinean Kina',
            'symbol' => 'K',
            'subunit' => 'Toea',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '598']);
        Currency::create(['priority' => '100',
            'iso_code' => 'PHP',
            'title' => 'Philippine Peso',
            'symbol' => '₱',
            'subunit' => 'Centavo',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '&#x20B1;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '608']);
        Currency::create(['priority' => '100',
            'iso_code' => 'PKR',
            'title' => 'Pakistani Rupee',
            'symbol' => '₨',
            'subunit' => 'Paisa',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '&#x20A8;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '586']);
        Currency::create(['priority' => '100',
            'iso_code' => 'PLN',
            'title' => 'Polish Złoty',
            'symbol' => 'zł',
            'subunit' => 'Grosz',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => 'z&#322;',
            'decimal_mark' => ',',
            'thousands_separator' => '',
            'iso_numeric' => '985']);
        Currency::create(['priority' => '100',
            'iso_code' => 'PYG',
            'title' => 'Paraguayan Guaraní',
            'symbol' => '₲',
            'subunit' => 'Céntimo',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '&#x20B2;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '600']);
        Currency::create(['priority' => '100',
            'iso_code' => 'QAR',
            'title' => 'Qatari Riyal',
            'symbol' => 'ر.ق',
            'subunit' => 'Dirham',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '&#xFDFC;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '634']);
        Currency::create(['priority' => '100',
            'iso_code' => 'RON',
            'title' => 'Romanian Leu',
            'symbol' => 'Lei',
            'subunit' => 'Bani',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '',
            'decimal_mark' => ',',
            'thousands_separator' => '.',
            'iso_numeric' => '946']);
        Currency::create(['priority' => '100',
            'iso_code' => 'RSD',
            'title' => 'Serbian Dinar',
            'symbol' => 'РСД',
            'subunit' => 'Para',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '941']);
        Currency::create(['priority' => '100',
            'iso_code' => 'RUB',
            'title' => 'Russian Ruble',
            'symbol' => 'р.',
            'subunit' => 'Kopek',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '&#x0440;&#x0443;&#x0431;',
            'decimal_mark' => ',',
            'thousands_separator' => '.',
            'iso_numeric' => '643']);
        Currency::create(['priority' => '100',
            'iso_code' => 'RWF',
            'title' => 'Rwandan Franc',
            'symbol' => 'FRw',
            'subunit' => 'Centime',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '646']);
        Currency::create(['priority' => '100',
            'iso_code' => 'SAR',
            'title' => 'Saudi Riyal',
            'symbol' => 'ر.س',
            'subunit' => 'Hallallah',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '&#xFDFC;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '682']);
        Currency::create(['priority' => '100',
            'iso_code' => 'SBD',
            'title' => 'Solomon Islands Dollar',
            'symbol' => '$',
            'subunit' => 'Cent',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '$',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '090']);
        Currency::create(['priority' => '100',
            'iso_code' => 'SCR',
            'title' => 'Seychellois Rupee',
            'symbol' => '₨',
            'subunit' => 'Cent',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '&#x20A8;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '690']);
        Currency::create(['priority' => '100',
            'iso_code' => 'SDG',
            'title' => 'Sudanese Pound',
            'symbol' => '£',
            'subunit' => 'Piastre',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '938']);
        Currency::create(['priority' => '100',
            'iso_code' => 'SEK',
            'title' => 'Swedish Krona',
            'symbol' => 'kr',
            'subunit' => 'Öre',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => ',',
            'thousands_separator' => '',
            'iso_numeric' => '752']);
        Currency::create(['priority' => '100',
            'iso_code' => 'SGD',
            'title' => 'Singapore Dollar',
            'symbol' => '$',
            'subunit' => 'Cent',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '$',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '702']);
        Currency::create(['priority' => '100',
            'iso_code' => 'SHP',
            'title' => 'Saint Helenian Pound',
            'symbol' => '£',
            'subunit' => 'Penny',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '&#x00A3;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '654']);
        Currency::create(['priority' => '100',
            'iso_code' => 'SKK',
            'title' => 'Slovak Koruna',
            'symbol' => 'Sk',
            'subunit' => 'Halier',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '703']);
        Currency::create(['priority' => '100',
            'iso_code' => 'SLL',
            'title' => 'Sierra Leonean Leone',
            'symbol' => 'Le',
            'subunit' => 'Cent',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '694']);
        Currency::create(['priority' => '100',
            'iso_code' => 'SOS',
            'title' => 'Somali Shilling',
            'symbol' => 'Sh',
            'subunit' => 'Cent',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '706']);
        Currency::create(['priority' => '100',
            'iso_code' => 'SRD',
            'title' => 'Surinamese Dollar',
            'symbol' => '$',
            'subunit' => 'Cent',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '968']);
        Currency::create(['priority' => '100',
            'iso_code' => 'SSP',
            'title' => 'South Sudanese Pound',
            'symbol' => '£',
            'subunit' => 'piaster',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '&#x00A3;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '728']);
        Currency::create(['priority' => '100',
            'iso_code' => 'STD',
            'title' => 'São Tomé and Príncipe Dobra',
            'symbol' => 'Db',
            'subunit' => 'Cêntimo',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '678']);
        Currency::create(['priority' => '100',
            'iso_code' => 'SVC',
            'title' => 'Salvadoran Colón',
            'symbol' => '₡',
            'subunit' => 'Centavo',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '&#x20A1;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '222']);
        Currency::create(['priority' => '100',
            'iso_code' => 'SYP',
            'title' => 'Syrian Pound',
            'symbol' => '£S',
            'subunit' => 'Piastre',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '&#x00A3;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '760']);
        Currency::create(['priority' => '100',
            'iso_code' => 'SZL',
            'title' => 'Swazi Lilangeni',
            'symbol' => 'L',
            'subunit' => 'Cent',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '748']);
        Currency::create(['priority' => '100',
            'iso_code' => 'THB',
            'title' => 'Thai Baht',
            'symbol' => '฿',
            'subunit' => 'Satang',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '&#x0E3F;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '764']);
        Currency::create(['priority' => '100',
            'iso_code' => 'TJS',
            'title' => 'Tajikistani Somoni',
            'symbol' => 'ЅМ',
            'subunit' => 'Diram',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '972']);
        Currency::create(['priority' => '100',
            'iso_code' => 'TMT',
            'title' => 'Turkmenistani Manat',
            'symbol' => 'T',
            'subunit' => 'Tenge',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '934']);
        Currency::create(['priority' => '100',
            'iso_code' => 'TND',
            'title' => 'Tunisian Dinar',
            'symbol' => 'TND',
            'subunit' => 'Millime',
            'subunit_to_unit' => '1000',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '788']);
        Currency::create(['priority' => '100',
            'iso_code' => 'TOP',
            'title' => 'Tongan Paʻanga',
            'symbol' => 'T$',
            'subunit' => 'Seniti',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '776']);
        Currency::create(['priority' => '100',
            'iso_code' => 'TRY',
            'title' => 'Turkish Lira',
            'symbol' => 'TL',
            'subunit' => 'kuruş',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => ',',
            'thousands_separator' => '.',
            'iso_numeric' => '949']);
        Currency::create(['priority' => '100',
            'iso_code' => 'TTD',
            'title' => 'Trinidad and Tobago Dollar',
            'symbol' => '$',
            'subunit' => 'Cent',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '$',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '780']);
        Currency::create(['priority' => '100',
            'iso_code' => 'TWD',
            'title' => 'New Taiwan Dollar',
            'symbol' => '$',
            'subunit' => 'Cent',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '$',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '901']);
        Currency::create(['priority' => '100',
            'iso_code' => 'TZS',
            'title' => 'Tanzanian Shilling',
            'symbol' => 'Sh',
            'subunit' => 'Cent',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '834']);
        Currency::create(['priority' => '100',
            'iso_code' => 'UAH',
            'title' => 'Ukrainian Hryvnia',
            'symbol' => '₴',
            'subunit' => 'Kopiyka',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '&#x20B4;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '980']);
        Currency::create(['priority' => '100',
            'iso_code' => 'UGX',
            'title' => 'Ugandan Shilling',
            'symbol' => 'USh',
            'subunit' => 'Cent',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '800']);
        Currency::create(['priority' => '1',
            'iso_code' => 'USD',
            'title' => 'United States Dollar',
            'symbol' => '$',
            'subunit' => 'Cent',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '$',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '840']);
        Currency::create(['priority' => '100',
            'iso_code' => 'UYU',
            'title' => 'Uruguayan Peso',
            'symbol' => '$',
            'subunit' => 'Centésimo',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '&#x20B1;',
            'decimal_mark' => ',',
            'thousands_separator' => '.',
            'iso_numeric' => '858']);
        Currency::create(['priority' => '100',
            'iso_code' => 'UZS',
            'title' => 'Uzbekistani Som',
            'symbol' => 'null',
            'subunit' => 'Tiyin',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '860']);
        Currency::create(['priority' => '100',
            'iso_code' => 'VEF',
            'title' => 'Venezuelan Bolívar',
            'symbol' => 'Bs F',
            'subunit' => 'Céntimo',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '',
            'decimal_mark' => ',',
            'thousands_separator' => '.',
            'iso_numeric' => '937']);
        Currency::create(['priority' => '100',
            'iso_code' => 'VND',
            'title' => 'Vietnamese Đồng',
            'symbol' => '₫',
            'subunit' => 'Hào',
            'subunit_to_unit' => '10',
            'symbol_first' => true,
            'html_entity' => '&#x20AB;',
            'decimal_mark' => ',',
            'thousands_separator' => '.',
            'iso_numeric' => '704']);
        Currency::create(['priority' => '100',
            'iso_code' => 'VUV',
            'title' => 'Vanuatu Vatu',
            'symbol' => 'Vt',
            'subunit' => 'null',
            'subunit_to_unit' => '1',
            'symbol_first' => true,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '548']);
        Currency::create(['priority' => '100',
            'iso_code' => 'WST',
            'title' => 'Samoan Tala',
            'symbol' => 'T',
            'subunit' => 'Sene',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '882']);
        Currency::create(['priority' => '100',
            'iso_code' => 'XAF',
            'title' => 'Central African Cfa Franc',
            'symbol' => 'Fr',
            'subunit' => 'Centime',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '950']);
        Currency::create(['priority' => '100',
            'iso_code' => 'XAG',
            'title' => 'Silver (Troy Ounce)',
            'symbol' => 'oz t',
            'subunit' => 'oz',
            'subunit_to_unit' => '1',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '961']);
        Currency::create(['priority' => '100',
            'iso_code' => 'XAU',
            'title' => 'Gold (Troy Ounce)',
            'symbol' => 'oz t',
            'subunit' => 'oz',
            'subunit_to_unit' => '1',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '959']);
        Currency::create(['priority' => '100',
            'iso_code' => 'XCD',
            'title' => 'East Caribbean Dollar',
            'symbol' => '$',
            'subunit' => 'Cent',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '$',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '951']);
        Currency::create(['priority' => '100',
            'iso_code' => 'XDR',
            'title' => 'Special Drawing Rights',
            'symbol' => 'SDR',
            'subunit' => '',
            'subunit_to_unit' => '1',
            'symbol_first' => false,
            'html_entity' => '$',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '960']);
        Currency::create(['priority' => '100',
            'iso_code' => 'XOF',
            'title' => 'West African Cfa Franc',
            'symbol' => 'Fr',
            'subunit' => 'Centime',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '952']);
        Currency::create(['priority' => '100',
            'iso_code' => 'XPF',
            'title' => 'Cfp Franc',
            'symbol' => 'Fr',
            'subunit' => 'Centime',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '953']);
        Currency::create(['priority' => '100',
            'iso_code' => 'YER',
            'title' => 'Yemeni Rial',
            'symbol' => '﷼',
            'subunit' => 'Fils',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '&#xFDFC;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '886']);
        Currency::create(['priority' => '100',
            'iso_code' => 'ZAR',
            'title' => 'South African Rand',
            'symbol' => 'R',
            'subunit' => 'Cent',
            'subunit_to_unit' => '100',
            'symbol_first' => true,
            'html_entity' => '&#x0052;',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '710']);
        Currency::create(['priority' => '100',
            'iso_code' => 'ZMK',
            'title' => 'Zambian Kwacha',
            'symbol' => 'ZK',
            'subunit' => 'Ngwee',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '894']);

        Currency::create(['priority' => '100',
            'iso_code' => 'ZMW',
            'title' => 'Zambian Kwacha',
            'symbol' => 'ZK',
            'subunit' => 'Ngwee',
            'subunit_to_unit' => '100',
            'symbol_first' => false,
            'html_entity' => '',
            'decimal_mark' => '.',
            'thousands_separator' => ',',
            'iso_numeric' => '967']);

        Country::create([
            'status' => 1,
            'title' => 'Afghanistan',
            'iso_2' => 'AF',
            'iso_3' => 'AFG',
            'iso_on' => '004',
            'currency_id' => 2
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Afrique du Sud',
            'iso_2' => 'ZA',
            'iso_3' => 'ZAF',
            'iso_on' => '710',
            'currency_id' => 162
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Albanie',
            'iso_2' => 'AL',
            'iso_3' => 'ALB',
            'iso_on' => '008',
            'currency_id' => 3
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Algérie',
            'iso_2' => 'DZ',
            'iso_3' => 'DZA',
            'iso_on' => '012',
            'currency_id' => 41
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Allemagne',
            'iso_2' => 'DE',
            'iso_3' => 'DEU',
            'iso_on' => '276',
            'currency_id' => 45
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Andorre',
            'iso_2' => 'AD',
            'iso_3' => 'ET',
            'iso_on' => '020',
            'currency_id' => 45
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Angola',
            'iso_2' => 'AO',
            'iso_3' => 'AGO',
            'iso_on' => '024',
            'currency_id' => 6
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Anguilla',
            'iso_2' => 'AI',
            'iso_3' => 'AIA',
            'iso_on' => '660',
            'currency_id' => 157
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Antarctique',
            'iso_2' => 'AQ',
            'iso_3' => 'ATA',
            'iso_on' => '010',
            'currency_id' => 147
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Antigua-et-Barbuda',
            'iso_2' => 'AG',
            'iso_3' => 'ATG',
            'iso_on' => '028',
            'currency_id' => 157
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Antilles néerlandaises',
            'iso_2' => 'AN',
            'iso_3' => 'ANT',
            'iso_on' => '530',
            'currency_id' => 5
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Arabie Saoudite',
            'iso_2' => 'SA',
            'iso_3' => 'SAU',
            'iso_on' => '682',
            'currency_id' => 120
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Argentine',
            'iso_2' => 'AR',
            'iso_3' => 'ARG',
            'iso_on' => '032',
            'currency_id' => 7
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Arménie',
            'iso_2' => 'AM',
            'iso_3' => 'ARM',
            'iso_on' => '051',
            'currency_id' => 4
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Aruba',
            'iso_2' => 'AW',
            'iso_3' => 'ABW',
            'iso_on' => '533',
            'currency_id' => 9
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Australie',
            'iso_2' => 'AU',
            'iso_3' => 'AUS',
            'iso_on' => '036',
            'currency_id' => 8
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Autriche',
            'iso_2' => 'AT',
            'iso_3' => 'AUT',
            'iso_on' => '040',
            'currency_id' => 45
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Azerbaïdjan',
            'iso_2' => 'AZ',
            'iso_3' => 'AZE',
            'iso_on' => '031',
            'currency_id' => 10
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Bahamas',
            'iso_2' => 'BS',
            'iso_3' => 'BHS',
            'iso_on' => '044',
            'currency_id' => 21
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Bahreïn',
            'iso_2' => 'BH',
            'iso_3' => 'BHR',
            'iso_on' => '048',
            'currency_id' => 15
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Bangladesh',
            'iso_2' => 'BD',
            'iso_3' => 'BGD',
            'iso_on' => '050',
            'currency_id' => 13
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Barbade',
            'iso_2' => 'BB',
            'iso_3' => 'BRB',
            'iso_on' => '052',
            'currency_id' => 12
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Belgique',
            'iso_2' => 'BE',
            'iso_3' => 'BEL',
            'iso_on' => '056',
            'currency_id' => 45
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Belize',
            'iso_2' => 'BZ',
            'iso_3' => 'BLZ',
            'iso_on' => '084',
            'currency_id' => 25
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Bénin',
            'iso_2' => 'BJ',
            'iso_3' => 'BEN',
            'iso_on' => '204',
            'currency_id' => 159
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Bermudes',
            'iso_2' => 'BM',
            'iso_3' => 'BMU',
            'iso_on' => '060',
            'currency_id' => 17
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Bhoutan',
            'iso_2' => 'BT',
            'iso_3' => 'BTN',
            'iso_on' => '064',
            'currency_id' => 22
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Biélorussie',
            'iso_2' => 'BY',
            'iso_3' => 'BLR',
            'iso_on' => '112',
            'currency_id' => 24
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Bolivie',
            'iso_2' => 'BO',
            'iso_3' => 'BOL',
            'iso_on' => '068',
            'currency_id' => 19
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Bosnie-Herzégovine',
            'iso_2' => 'BA',
            'iso_3' => 'BIH',
            'iso_on' => '070',
            'currency_id' => 11
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Botswana',
            'iso_2' => 'BW',
            'iso_3' => 'BWA',
            'iso_on' => '072',
            'currency_id' => 23
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Brésil',
            'iso_2' => 'BR',
            'iso_3' => 'BRA',
            'iso_on' => '076',
            'currency_id' => 20
        ]);


        Country::create([
            'status' => 1,
            'title' => 'British Virgin Islands',
            'iso_2' => 'VG',
            'iso_3' => 'VGB',
            'iso_on' => '092',
            'currency_id' => 48
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Brunei Darussalam',
            'iso_2' => 'BN',
            'iso_3' => 'BRN',
            'iso_on' => '096',
            'currency_id' => 18
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Bulgarie',
            'iso_2' => 'BG',
            'iso_3' => 'BGR',
            'iso_on' => '100',
            'currency_id' => 14
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Burkina Faso',
            'iso_2' => 'BF',
            'iso_3' => 'BFA',
            'iso_on' => '854',
            'currency_id' => 159
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Burundi',
            'iso_2' => 'BI',
            'iso_3' => 'BDI',
            'iso_on' => '108',
            'currency_id' => 16
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Cambodge',
            'iso_2' => 'KH',
            'iso_3' => 'KHM',
            'iso_on' => '116',
            'currency_id' => 72
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Cameroun',
            'iso_2' => 'CM',
            'iso_3' => 'CMR',
            'iso_on' => '120',
            'currency_id' => 154
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Canada',
            'iso_2' => 'CA',
            'iso_3' => 'CAN',
            'iso_on' => '124',
            'currency_id' => 26
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Cap-Vert',
            'iso_2' => 'CV',
            'iso_3' => 'CPV',
            'iso_on' => '132',
            'currency_id' => 36
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Chili',
            'iso_2' => 'CL',
            'iso_3' => 'CHL',
            'iso_on' => '152',
            'currency_id' => 30
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Chine',
            'iso_2' => 'CN',
            'iso_3' => 'CHN',
            'iso_on' => '156',
            'currency_id' => 31
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Christmas Island',
            'iso_2' => 'CX',
            'iso_3' => 'CXR',
            'iso_on' => '162',
            'currency_id' => 8
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Chypre',
            'iso_2' => 'CY',
            'iso_3' => 'CYP',
            'iso_on' => '196',
            'currency_id' => 45
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Colombie',
            'iso_2' => 'CO',
            'iso_3' => 'COL',
            'iso_on' => '170',
            'currency_id' => 32
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Comores',
            'iso_2' => 'KM',
            'iso_3' => 'COM',
            'iso_on' => '174',
            'currency_id' => 73
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Congo (Brazzaville)',
            'iso_2' => 'CG',
            'iso_3' => 'COG',
            'iso_on' => '178',
            'currency_id' => 154
        ]);


        Country::create([
            'status' => 1,
            'title' => 'République démocratique du Congo',
            'iso_2' => 'CD',
            'iso_3' => 'COD',
            'iso_on' => '180',
            'currency_id' => 27
        ]);


        Country::create([
            'status' => 1,
            'title' => 'République de Corée',
            'iso_2' => 'KR',
            'iso_3' => 'KOR',
            'iso_on' => '410',
            'currency_id' => 75
        ]);


        Country::create([
            'status' => 1,
            'title' => 'République populaire démocratique de Corée',
            'iso_2' => 'KP',
            'iso_3' => 'PRK',
            'iso_on' => '408',
            'currency_id' => 74
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Costa Rica',
            'iso_2' => 'CR',
            'iso_3' => 'CRI',
            'iso_on' => '188',
            'currency_id' => 33
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Côte d\'Ivoire',
            'iso_2' => 'CI',
            'iso_3' => 'CIV',
            'iso_on' => '384',
            'currency_id' => 159
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Croatie',
            'iso_2' => 'RH',
            'iso_3' => 'VRC',
            'iso_on' => '191',
            'currency_id' => 58
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Cuba',
            'iso_2' => 'CU',
            'iso_3' => 'CUB',
            'iso_on' => '192',
            'currency_id' => 34
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Danemark',
            'iso_2' => 'DK',
            'iso_3' => 'DNK',
            'iso_on' => '208',
            'currency_id' => 39
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Djibouti',
            'iso_2' => 'DJ',
            'iso_3' => 'DJI',
            'iso_on' => '262',
            'currency_id' => 38
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Dominique',
            'iso_2' => 'DM',
            'iso_3' => 'DMA',
            'iso_on' => '212',
            'currency_id' => 40
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Egypte',
            'iso_2' => 'EG',
            'iso_3' => 'EGY',
            'iso_on' => '818',
            'currency_id' => 42
        ]);


        Country::create([
            'status' => 1,
            'title' => 'El Salvador',
            'iso_2' => 'SV',
            'iso_3' => 'SLV',
            'iso_on' => '222',
            'currency_id' => 133
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Émirats arabes unis',
            'iso_2' => 'AE',
            'iso_3' => 'ARE',
            'iso_on' => '784',
            'currency_id' => 1
        ]);


        Country::create([
            'status' => 1,
            'title' => 'L\'île d\'Heard et des îles McDonald',
            'iso_2' => 'HM',
            'iso_3' => 'HMD',
            'iso_on' => '334',
            'currency_id' => 8
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Equateur',
            'iso_2' => 'CE',
            'iso_3' => 'ECU',
            'iso_on' => '218',
            'currency_id' => 147
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Érythrée',
            'iso_2' => 'ER',
            'iso_3' => 'ERI',
            'iso_on' => '232',
            'currency_id' => 43
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Espagne',
            'iso_2' => 'ES',
            'iso_3' => 'ESP',
            'iso_on' => '724',
            'currency_id' => 45
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Estonie',
            'iso_2' => 'EE',
            'iso_3' => 'HNE',
            'iso_on' => '233',
            'currency_id' => 45
        ]);


        Country::create([
            'status' => 1,
            'title' => 'États-Unis d\'Amérique',
            'iso_2' => 'Etats-Unis',
            'iso_3' => 'Etats-Unis',
            'iso_on' => '840',
            'currency_id' => 147
        ]);


        Country::create([
            'status' => 1,
            'title' => 'États-Unis Îles mineures éloignées',
            'iso_2' => 'UM',
            'iso_3' => 'UMI',
            'iso_on' => '581',
            'currency_id' => 147
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Éthiopie',
            'iso_2' => 'ET',
            'iso_3' => 'ETH',
            'iso_on' => '231',
            'currency_id' => 44
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Fédération de Russie',
            'iso_2' => 'RU',
            'iso_3' => 'RUS',
            'iso_on' => '643',
            'currency_id' => 118
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Fidji',
            'iso_2' => 'FJ',
            'iso_3' => 'FJI',
            'iso_on' => '242',
            'currency_id' => 46
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Finlande',
            'iso_2' => 'FI',
            'iso_3' => 'FIN',
            'iso_on' => '246',
            'currency_id' => 45
        ]);


        Country::create([
            'status' => 1,
            'title' => 'France',
            'iso_2' => 'FR',
            'iso_3' => 'FRA',
            'iso_on' => '250',
            'currency_id' => 45
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Gabon',
            'iso_2' => 'Géorgie',
            'iso_3' => 'GAB',
            'iso_on' => '266',
            'currency_id' => 154
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Gambie',
            'iso_2' => 'GM',
            'iso_3' => 'GMB',
            'iso_on' => '270',
            'currency_id' => 52
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Géorgie',
            'iso_2' => 'GE',
            'iso_3' => 'GEO',
            'iso_on' => '268',
            'currency_id' => 49
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Géorgie du Sud et les îles Sandwich du Sud',
            'iso_2' => 'GS',
            'iso_3' => 'GV',
            'iso_on' => '239',
            'currency_id' => 147
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Ghana',
            'iso_2' => 'GH',
            'iso_3' => 'GHA',
            'iso_on' => '288',
            'currency_id' => 50
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Gibraltar',
            'iso_2' => 'GI',
            'iso_3' => 'GIB',
            'iso_on' => '292',
            'currency_id' => 51
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Grèce',
            'iso_2' => 'GR',
            'iso_3' => 'GRC',
            'iso_on' => '300',
            'currency_id' => 45
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Grenade',
            'iso_2' => 'GD',
            'iso_3' => 'GRD',
            'iso_on' => '308',
            'currency_id' => 157
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Groenland',
            'iso_2' => 'GL',
            'iso_3' => 'GRL',
            'iso_on' => '304',
            'currency_id' => 39
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Guadeloupe',
            'iso_2' => 'GP',
            'iso_3' => 'GLP',
            'iso_on' => '312',
            'currency_id' => 45
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Guam',
            'iso_2' => 'GU',
            'iso_3' => 'GUM',
            'iso_on' => '316',
            'currency_id' => 147
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Guatemala',
            'iso_2' => 'GT',
            'iso_3' => 'GTM',
            'iso_on' => '320',
            'currency_id' => 54
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Guernesey',
            'iso_2' => 'GG',
            'iso_3' => 'GGY',
            'iso_on' => '831',
            'currency_id' => 48
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Guinée',
            'iso_2' => 'GN',
            'iso_3' => 'GIN',
            'iso_on' => '324',
            'currency_id' => 53
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Guinée équatoriale',
            'iso_2' => 'GQ',
            'iso_3' => 'GNQ',
            'iso_on' => '226',
            'currency_id' => 154
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Guinée-Bissau',
            'iso_2' => 'GW',
            'iso_3' => 'GNB',
            'iso_on' => '624',
            'currency_id' => 159
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Guyane',
            'iso_2' => 'GY',
            'iso_3' => 'GUY',
            'iso_on' => '328',
            'currency_id' => 55
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Guyane française',
            'iso_2' => 'GF',
            'iso_3' => 'FSI',
            'iso_on' => '254',
            'currency_id' => 45
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Haïti',
            'iso_2' => 'HT',
            'iso_3' => 'HTI',
            'iso_on' => '332',
            'currency_id' => 59
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Honduras',
            'iso_2' => 'HN',
            'iso_3' => 'HND',
            'iso_on' => '340',
            'currency_id' => 57
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Hong Kong Région administrative spéciale de la Chine',
            'iso_2' => 'HK',
            'iso_3' => 'HKG',
            'iso_on' => '344',
            'currency_id' => 56
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Hongrie',
            'iso_2' => 'HU',
            'iso_3' => 'HUN',
            'iso_on' => '348',
            'currency_id' => 60
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Ile de Man',
            'iso_2' => 'IM',
            'iso_3' => 'IMN',
            'iso_on' => '833',
            'currency_id' => 48
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Îles Aland',
            'iso_2' => 'AX',
            'iso_3' => 'ALA',
            'iso_on' => '248',
            'currency_id' => 45
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Iles Cayman',
            'iso_2' => 'KY',
            'iso_3' => 'CYM',
            'iso_on' => '136',
            'currency_id' => 77
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Îles Cocos (Keeling)',
            'iso_2' => 'CC',
            'iso_3' => 'CCK',
            'iso_on' => '166',
            'currency_id' => 8
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Îles Cook',
            'iso_2' => 'CK',
            'iso_3' => 'COK',
            'iso_on' => '184',
            'currency_id' => 106
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Îles Falkland (Malvinas)',
            'iso_2' => 'FK',
            'iso_3' => 'FLK',
            'iso_on' => '238',
            'currency_id' => 47
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Îles Féroé',
            'iso_2' => 'FO',
            'iso_3' => 'BOF',
            'iso_on' => '234',
            'currency_id' => 39
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Îles Mariannes du Nord',
            'iso_2' => 'MP',
            'iso_3' => 'MNP',
            'iso_on' => '580',
            'currency_id' => 147
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Iles Marshall',
            'iso_2' => 'MH',
            'iso_3' => 'MHL',
            'iso_on' => '584',
            'currency_id' => 147
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Îles Salomon',
            'iso_2' => 'SB',
            'iso_3' => 'SLB',
            'iso_on' => '090',
            'currency_id' => 121
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Îles Turques et Caïques',
            'iso_2' => 'TC',
            'iso_3' => 'TCA',
            'iso_on' => '796',
            'currency_id' => 147
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Îles Vierges américaines',
            'iso_2' => 'VI',
            'iso_3' => 'VIR',
            'iso_on' => '850',
            'currency_id' => 147
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Inde',
            'iso_2' => 'IN',
            'iso_3' => 'IND',
            'iso_on' => '356',
            'currency_id' => 63
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Indonésie',
            'iso_2' => 'ID',
            'iso_3' => 'IDN',
            'iso_on' => '360',
            'currency_id' => 61
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Irak',
            'iso_2' => 'IQ',
            'iso_3' => 'IRQ',
            'iso_on' => '368',
            'currency_id' => 64
        ]);


        Country::create([
            'status' => 1,
            'title' => 'République islamique d\'Iran',
            'iso_2' => 'IR',
            'iso_3' => 'IRN',
            'iso_on' => '364',
            'currency_id' => 65
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Irlande',
            'iso_2' => 'IE',
            'iso_3' => 'IRL',
            'iso_on' => '372',
            'currency_id' => 45
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Islande',
            'iso_2' => 'IS',
            'iso_3' => 'ISL',
            'iso_on' => '352',
            'currency_id' => 66
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Israël',
            'iso_2' => 'IL',
            'iso_3' => 'ISR',
            'iso_on' => '376',
            'currency_id' => 62
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Italie',
            'iso_2' => 'IT',
            'iso_3' => 'ITA',
            'iso_on' => '380',
            'currency_id' => 45
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Jamaïque',
            'iso_2' => 'JM',
            'iso_3' => 'JAM',
            'iso_on' => '388',
            'currency_id' => 67
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Japon',
            'iso_2' => 'JP',
            'iso_3' => 'JPN',
            'iso_on' => '392',
            'currency_id' => 69
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Jersey',
            'iso_2' => 'JE',
            'iso_3' => 'JEY',
            'iso_on' => '832',
            'currency_id' => 48
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Jordanie',
            'iso_2' => 'JO',
            'iso_3' => 'JOR',
            'iso_on' => '400',
            'currency_id' => 68
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Kazakhstan',
            'iso_2' => 'KZ',
            'iso_3' => 'KAZ',
            'iso_on' => '398',
            'currency_id' => 78
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Kenya',
            'iso_2' => 'KE',
            'iso_3' => 'KEN',
            'iso_on' => '404',
            'currency_id' => 70
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Kirghizistan',
            'iso_2' => 'KG',
            'iso_3' => 'KGZ',
            'iso_on' => '417',
            'currency_id' => 71
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Kiribati',
            'iso_2' => 'KI',
            'iso_3' => 'KIR',
            'iso_on' => '296',
            'currency_id' => 8
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Koweït',
            'iso_2' => 'KW',
            'iso_3' => 'KWT',
            'iso_on' => '414',
            'currency_id' => 76
        ]);


        Country::create([
            'status' => 1,
            'title' => 'L\'île Bouvet',
            'iso_2' => 'BV',
            'iso_3' => 'BVT',
            'iso_on' => '074',
            'currency_id' => 104
        ]);


        Country::create([
            'status' => 1,
            'title' => 'L\'île de Norfolk',
            'iso_2' => 'NF',
            'iso_3' => 'NFK',
            'iso_on' => '574',
            'currency_id' => 8
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Lesotho',
            'iso_2' => 'LS',
            'iso_3' => 'LSO',
            'iso_on' => '426',
            'currency_id' => 83
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Lettonie',
            'iso_2' => 'LV',
            'iso_3' => 'LVA',
            'iso_on' => '428',
            'currency_id' => 45
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Liban',
            'iso_2' => 'LB',
            'iso_3' => 'LBN',
            'iso_on' => '422',
            'currency_id' => 80
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Libéria',
            'iso_2' => 'LR',
            'iso_3' => 'LBR',
            'iso_on' => '430',
            'currency_id' => 82
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Libye',
            'iso_2' => 'LY',
            'iso_3' => 'LBY',
            'iso_on' => '434',
            'currency_id' => 86
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Liechtenstein',
            'iso_2' => 'LI',
            'iso_3' => 'LIE',
            'iso_on' => '438',
            'currency_id' => 28
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Lituanie',
            'iso_2' => 'LT',
            'iso_3' => 'LTU',
            'iso_on' => '440',
            'currency_id' => 45
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Luxembourg',
            'iso_2' => 'LU',
            'iso_3' => 'LUX',
            'iso_on' => '442',
            'currency_id' => 45
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Macao région administrative spéciale de la Chine',
            'iso_2' => 'MO',
            'iso_3' => 'MAC',
            'iso_on' => '446',
            'currency_id' => 93
        ]);


        Country::create([
            'status' => 1,
            'title' => 'République de Macédoine',
            'iso_2' => 'MK',
            'iso_3' => 'MKD',
            'iso_on' => '807',
            'currency_id' => 100
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Madagascar',
            'iso_2' => 'MG',
            'iso_3' => 'MDG',
            'iso_on' => '450',
            'currency_id' => 89
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Malaisie',
            'iso_2' => 'MY',
            'iso_3' => 'MYS',
            'iso_on' => '458',
            'currency_id' => 99
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Malawi',
            'iso_2' => 'MW',
            'iso_3' => 'MWI',
            'iso_on' => '454',
            'currency_id' => 97
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Maldives',
            'iso_2' => 'MV',
            'iso_3' => 'MDV',
            'iso_on' => '462',
            'currency_id' => 96
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Mali',
            'iso_2' => 'ML',
            'iso_3' => 'MLI',
            'iso_on' => '466',
            'currency_id' => 159
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Malte',
            'iso_2' => 'MT',
            'iso_3' => 'MLT',
            'iso_on' => '470',
            'currency_id' => 45
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Maroc',
            'iso_2' => 'MA',
            'iso_3' => 'MAR',
            'iso_on' => '504',
            'currency_id' => 87
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Martinique',
            'iso_2' => 'MQ',
            'iso_3' => 'MTQ',
            'iso_on' => '474',
            'currency_id' => 45
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Maurice',
            'iso_2' => 'MU',
            'iso_3' => 'MUS',
            'iso_on' => '480',
            'currency_id' => 95
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Mauritanie',
            'iso_2' => 'MR',
            'iso_3' => 'MRT',
            'iso_on' => '478',
            'currency_id' => 94
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Mayotte',
            'iso_2' => 'YT',
            'iso_3' => 'CEST',
            'iso_on' => '175',
            'currency_id' => 45
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Mexique',
            'iso_2' => 'MX',
            'iso_3' => 'MEX',
            'iso_on' => '484',
            'currency_id' => 98
        ]);


        Country::create([
            'status' => 1,
            'title' => 'États fédérés de Micronésie',
            'iso_2' => 'FM',
            'iso_3' => 'FSM',
            'iso_on' => '583',
            'currency_id' => 147
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Moldavie',
            'iso_2' => 'MD',
            'iso_3' => 'MDA',
            'iso_on' => '498',
            'currency_id' => 88
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Monaco',
            'iso_2' => 'MC',
            'iso_3' => 'AGC',
            'iso_on' => '492',
            'currency_id' => 45
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Mongolie',
            'iso_2' => 'MN',
            'iso_3' => 'MNG',
            'iso_on' => '496',
            'currency_id' => 92
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Monténégro',
            'iso_2' => 'ME',
            'iso_3' => 'MNE',
            'iso_on' => '499',
            'currency_id' => 45
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Montserrat',
            'iso_2' => 'MS',
            'iso_3' => 'MSR',
            'iso_on' => '500',
            'currency_id' => 157
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Mozambique',
            'iso_2' => 'MZ',
            'iso_3' => 'MOZ',
            'iso_on' => '508',
            'currency_id' => 100
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Myanmar',
            'iso_2' => 'MM',
            'iso_3' => 'MMR',
            'iso_on' => '104',
            'currency_id' => 91
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Namibie',
            'iso_2' => 'NA',
            'iso_3' => 'NAM',
            'iso_on' => '516',
            'currency_id' => 101
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Nauru',
            'iso_2' => 'NR',
            'iso_3' => 'NRU',
            'iso_on' => '520',
            'currency_id' => 8
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Népal',
            'iso_2' => 'NP',
            'iso_3' => 'NPL',
            'iso_on' => '524',
            'currency_id' => 105
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Nicaragua',
            'iso_2' => 'NI',
            'iso_3' => 'NIC',
            'iso_on' => '558',
            'currency_id' => 103
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Niger',
            'iso_2' => 'NE',
            'iso_3' => 'TNS',
            'iso_on' => '562',
            'currency_id' => 159
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Nigeria',
            'iso_2' => 'NG',
            'iso_3' => 'NGA',
            'iso_on' => '566',
            'currency_id' => 102
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Niue',
            'iso_2' => 'NU',
            'iso_3' => 'NIU',
            'iso_on' => '570',
            'currency_id' => 106
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Norvège',
            'iso_2' => 'NO',
            'iso_3' => 'NOR',
            'iso_on' => '578',
            'currency_id' => 104
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Nouvelle-Calédonie',
            'iso_2' => 'NC',
            'iso_3' => 'NCL',
            'iso_on' => '540',
            'currency_id' => 160
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Nouvelle-Zélande',
            'iso_2' => 'NZ',
            'iso_3' => 'NZL',
            'iso_on' => '554',
            'currency_id' => 106
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Oman',
            'iso_2' => 'l\'OM',
            'iso_3' => 'OMN',
            'iso_on' => '512',
            'currency_id' => 107
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Ouganda',
            'iso_2' => 'UG',
            'iso_3' => 'UGA',
            'iso_on' => '800',
            'currency_id' => 146
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Ouzbékistan',
            'iso_2' => 'UZ',
            'iso_3' => 'UZB',
            'iso_on' => '860',
            'currency_id' => 149
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Pakistan',
            'iso_2' => 'PK',
            'iso_3' => 'PAK',
            'iso_on' => '586',
            'currency_id' => 112
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Palau',
            'iso_2' => 'PW',
            'iso_3' => 'PLW',
            'iso_on' => '585',
            'currency_id' => 147
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Panama',
            'iso_2' => 'PA',
            'iso_3' => 'PAN',
            'iso_on' => '591',
            'currency_id' => 108
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Papouasie-Nouvelle-Guinée',
            'iso_2' => 'PG',
            'iso_3' => 'PNG',
            'iso_on' => '598',
            'currency_id' => 110
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Paraguay',
            'iso_2' => 'PY',
            'iso_3' => 'PRY',
            'iso_on' => '600',
            'currency_id' => 114
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Pays-Bas',
            'iso_2' => 'NL',
            'iso_3' => 'NLD',
            'iso_on' => '528',
            'currency_id' => 45
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Pérou',
            'iso_2' => 'PE',
            'iso_3' => 'PER',
            'iso_on' => '604',
            'currency_id' => 109
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Philippines',
            'iso_2' => 'PH',
            'iso_3' => 'PHL',
            'iso_on' => '608',
            'currency_id' => 111
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Pitcairn',
            'iso_2' => 'PN',
            'iso_3' => 'PCN',
            'iso_on' => '612',
            'currency_id' => 106
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Pologne',
            'iso_2' => 'PL',
            'iso_3' => 'POL',
            'iso_on' => '616',
            'currency_id' => 113
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Polynésie française',
            'iso_2' => 'PF',
            'iso_3' => 'PYF',
            'iso_on' => '258',
            'currency_id' => 160
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Portugal',
            'iso_2' => 'PT',
            'iso_3' => 'PRT',
            'iso_on' => '620',
            'currency_id' => 45
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Puerto Rico',
            'iso_2' => 'PR',
            'iso_3' => 'PRI',
            'iso_on' => '630',
            'currency_id' => 147
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Qatar',
            'iso_2' => 'QA',
            'iso_3' => 'QAT',
            'iso_on' => '634',
            'currency_id' => 115
        ]);


        Country::create([
            'status' => 1,
            'title' => 'République arabe syrienne (Syrie)',
            'iso_2' => 'SY',
            'iso_3' => 'SYR',
            'iso_on' => '760',
            'currency_id' => 134
        ]);


        Country::create([
            'status' => 1,
            'title' => 'République centrafricaine',
            'iso_2' => 'CF',
            'iso_3' => 'CAF',
            'iso_on' => '140',
            'currency_id' => 154
        ]);


        Country::create([
            'status' => 1,
            'title' => 'République démocratique populaire lao',
            'iso_2' => 'LA',
            'iso_3' => 'AJO',
            'iso_on' => '418',
            'currency_id' => 97
        ]);


        Country::create([
            'status' => 1,
            'title' => 'République dominicaine',
            'iso_2' => 'DO',
            'iso_3' => 'DOM',
            'iso_on' => '214',
            'currency_id' => 40
        ]);


        Country::create([
            'status' => 1,
            'title' => 'République tchèque',
            'iso_2' => 'CZ',
            'iso_3' => 'CZE',
            'iso_on' => '203',
            'currency_id' => 37
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Réunion',
            'iso_2' => 'RE',
            'iso_3' => 'REU',
            'iso_on' => '638',
            'currency_id' => 45
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Roumanie',
            'iso_2' => 'RO',
            'iso_3' => 'ROU',
            'iso_on' => '642',
            'currency_id' => 116
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Royaume-Uni',
            'iso_2' => 'GB',
            'iso_3' => 'GBR',
            'iso_on' => '826',
            'currency_id' => 48
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Rwanda',
            'iso_2' => 'RW',
            'iso_3' => 'RWA',
            'iso_on' => '646',
            'currency_id' => 119
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Sahara occidental',
            'iso_2' => 'EH',
            'iso_3' => 'ESH',
            'iso_on' => '732',
            'currency_id' => 87
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Saint-Barthélemy',
            'iso_2' => 'BL',
            'iso_3' => 'FR',
            'iso_on' => '652',
            'currency_id' => 45
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Saint-Kitts-et-Nevis',
            'iso_2' => 'KN',
            'iso_3' => 'KNA',
            'iso_on' => '659',
            'currency_id' => 157
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Saint-Marin',
            'iso_2' => 'SM',
            'iso_3' => 'SMR',
            'iso_on' => '674',
            'currency_id' => 45
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Saint-Martin (partie française)',
            'iso_2' => 'MF',
            'iso_3' => 'CRG',
            'iso_on' => '663',
            'currency_id' => 45
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Saint-Pierre-et-Miquelon',
            'iso_2' => 'PM',
            'iso_3' => 'SPM',
            'iso_on' => '666',
            'currency_id' => 45
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Saint-Siège (Cité du Vatican)',
            'iso_2' => 'VA',
            'iso_3' => 'TVA',
            'iso_on' => '336',
            'currency_id' => 45
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Saint-Vincent-et-les Grenadines',
            'iso_2' => 'VC',
            'iso_3' => 'CDV',
            'iso_on' => '670',
            'currency_id' => 157
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Sainte-Hélène',
            'iso_2' => 'SH',
            'iso_3' => 'SHN',
            'iso_on' => '654',
            'currency_id' => 126
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Sainte-Lucie',
            'iso_2' => 'LC',
            'iso_3' => 'LCA',
            'iso_on' => '662',
            'currency_id' => 157
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Samoa',
            'iso_2' => 'WS',
            'iso_3' => 'WSM',
            'iso_on' => '882',
            'currency_id' => 153
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Samoa américaines',
            'iso_2' => 'AS',
            'iso_3' => 'ASM',
            'iso_on' => '016',
            'currency_id' => 147
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Sao Tomé-et-Principe',
            'iso_2' => 'ST',
            'iso_3' => 'STP',
            'iso_on' => '678',
            'currency_id' => 132
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Sénégal',
            'iso_2' => 'SN',
            'iso_3' => 'SEN',
            'iso_on' => '686',
            'currency_id' => 159
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Serbie',
            'iso_2' => 'RS',
            'iso_3' => 'SRB',
            'iso_on' => '688',
            'currency_id' => 117
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Seychelles',
            'iso_2' => 'SC',
            'iso_3' => 'la CJS',
            'iso_on' => '690',
            'currency_id' => 122
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Sierra Leone',
            'iso_2' => 'SL',
            'iso_3' => 'SLE',
            'iso_on' => '694',
            'currency_id' => 128
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Singapour',
            'iso_2' => 'SG',
            'iso_3' => 'SGP',
            'iso_on' => '702',
            'currency_id' => 125
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Slovaquie',
            'iso_2' => 'SK',
            'iso_3' => 'SVK',
            'iso_on' => '703',
            'currency_id' => 45
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Slovénie',
            'iso_2' => 'SI',
            'iso_3' => 'SVN',
            'iso_on' => '705',
            'currency_id' => 45
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Somalie',
            'iso_2' => 'SO',
            'iso_3' => 'SOM',
            'iso_on' => '706',
            'currency_id' => 129
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Soudan',
            'iso_2' => 'SD',
            'iso_3' => 'SDN',
            'iso_on' => '736',
            'currency_id' => 123
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Sri Lanka',
            'iso_2' => 'LK',
            'iso_3' => 'LKA',
            'iso_on' => '144',
            'currency_id' => 81
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Sud-Soudan',
            'iso_2' => 'SS',
            'iso_3' => 'SSD',
            'iso_on' => '728',
            'currency_id' => 131
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Suède',
            'iso_2' => 'SE',
            'iso_3' => 'SWE',
            'iso_on' => '752',
            'currency_id' => 124
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Suisse',
            'iso_2' => 'CH',
            'iso_3' => 'CHE',
            'iso_on' => '756',
            'currency_id' => 28
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Suriname',
            'iso_2' => 'SR',
            'iso_3' => 'SUR',
            'iso_on' => '740',
            'currency_id' => 130
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Svalbard et Jan Mayen',
            'iso_2' => 'SJ',
            'iso_3' => 'SJM',
            'iso_on' => '744',
            'currency_id' => 104
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Swaziland',
            'iso_2' => 'SZ',
            'iso_3' => 'SWZ',
            'iso_on' => '748',
            'currency_id' => 135
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Tadjikistan',
            'iso_2' => 'TJ',
            'iso_3' => 'TJK',
            'iso_on' => '762',
            'currency_id' => 137
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Taiwan',
            'iso_2' => 'TW',
            'iso_3' => 'TWN',
            'iso_on' => '158',
            'currency_id' => 143
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Tanzanie',
            'iso_2' => 'TZ',
            'iso_3' => 'TZA',
            'iso_on' => '834',
            'currency_id' => 144
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Tchad',
            'iso_2' => 'TD',
            'iso_3' => 'TCD',
            'iso_on' => '148',
            'currency_id' => 154
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Terres australes françaises',
            'iso_2' => 'TF',
            'iso_3' => 'ATF',
            'iso_on' => '260',
            'currency_id' => 45
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Territoire britannique de l\'océan Indien',
            'iso_2' => 'IO',
            'iso_3' => 'IOT',
            'iso_on' => '086',
            'currency_id' => 147
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Territoires palestiniens (occupés)',
            'iso_2' => 'PS',
            'iso_3' => 'PSE',
            'iso_on' => '275',
            'currency_id' => 147
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Thaïlande',
            'iso_2' => 'TH',
            'iso_3' => 'THA',
            'iso_on' => '764',
            'currency_id' => 136
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Timor-Leste',
            'iso_2' => 'TL',
            'iso_3' => 'TLS',
            'iso_on' => '626',
            'currency_id' => 147
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Togo',
            'iso_2' => 'TG',
            'iso_3' => 'TGO',
            'iso_on' => '768',
            'currency_id' => 159
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Tokelau',
            'iso_2' => 'les savoirs traditionnels',
            'iso_3' => 'TKL',
            'iso_on' => '772',
            'currency_id' => 106
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Tonga',
            'iso_2' => 'TO',
            'iso_3' => 'TON',
            'iso_on' => '776',
            'currency_id' => 140
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Trinité-et-Tobago',
            'iso_2' => 'TT',
            'iso_3' => 'TTO',
            'iso_on' => '780',
            'currency_id' => 142
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Tunisie',
            'iso_2' => 'TN',
            'iso_3' => 'TUN',
            'iso_on' => '788',
            'currency_id' => 139
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Turkménistan',
            'iso_2' => 'TM',
            'iso_3' => 'TKM',
            'iso_on' => '795',
            'currency_id' => 138
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Turquie',
            'iso_2' => 'TR',
            'iso_3' => 'TUR',
            'iso_on' => '792',
            'currency_id' => 141
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Tuvalu',
            'iso_2' => 'TV',
            'iso_3' => 'TUV',
            'iso_on' => '798',
            'currency_id' => 8
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Ukraine',
            'iso_2' => 'UA',
            'iso_3' => 'UKR',
            'iso_on' => '804',
            'currency_id' => 145
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Uruguay',
            'iso_2' => 'UY',
            'iso_3' => 'URY',
            'iso_on' => '858',
            'currency_id' => 148
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Vanuatu',
            'iso_2' => 'VU',
            'iso_3' => 'VUT',
            'iso_on' => '548',
            'currency_id' => 152
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Venezuela (République bolivarienne du)',
            'iso_2' => 'VE',
            'iso_3' => 'VEN',
            'iso_on' => '862',
            'currency_id' => 150
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Viet Nam',
            'iso_2' => 'VN',
            'iso_3' => 'VNM',
            'iso_on' => '704',
            'currency_id' => 151
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Wallis-et-Futuna',
            'iso_2' => 'WF',
            'iso_3' => 'WLF',
            'iso_on' => '876',
            'currency_id' => 160
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Yémen',
            'iso_2' => 'YE',
            'iso_3' => 'YEM',
            'iso_on' => '887',
            'currency_id' => 161
        ]);


        Country::create([
            'status' => 1,
            'title' => 'Zambie',
            'iso_2' => 'ZM',
            'iso_3' => 'ZMB',
            'iso_on' => '894',
            'currency_id' => 163
        ]);



        Role::create(['title' => 'User']);
        Role::create(['title' => 'Administrator']);


        $userData = [
            'email' => 'ihebjabri015@gmail.com',
            'password' => bcrypt('123456'),
            'validation' => 'SBSBQSFQZFYJUYKGH?JKIYUKUYKHJ..YUKJGFDGFGJS',
            'name' => 'j.iheb', 
            'country_id' => 1,
            'city' => 'New York',
            'status' => 1
        ];

        $user = User::create($userData);
        User::find($user->id)->roles()->attach(1);
    

      
        

    }
}
