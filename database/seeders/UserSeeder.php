<?php

use App\Modules\User\Models\Role;
use App\Modules\User\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['title' => 'User']);
        Role::create(['title' => 'Administrator']);


        $userData = [
            'email' => 'ihebjabri015@gmail.com',
            'password' => bcrypt('123456'),
            'validation' => 'SBSBQSFQZFYJUYKGH?JKIYUKUYKHJ..YUKJGFDGFGJS',
            'name' => 'j.iheb'
        ];

        $user = User::create($userData);
        User::find($user->id)->roles()->attach(1);
    }
}
