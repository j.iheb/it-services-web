<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->id();
            $table->string('title'); 
            $table->float('amount'); 
            $table->text('description'); 
            $table->string('photo')->nullable(); 
            $table->unsignedBigInteger('service_id'); 
            $table->timestamps();
        });


        Schema::create('notes', function (Blueprint $table) {
            $table->id();
            $table->string('title'); 
            $table->float('content'); 
            $table->unsignedBigInteger('user_id'); 
            $table->timestamps();
        });




    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notes');
        Schema::dropIfExists('product');
    }
}
