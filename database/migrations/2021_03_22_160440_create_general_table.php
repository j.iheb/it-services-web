<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGeneralTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


    

        Schema::create('currencies', function(Blueprint $table) {
            $table->id('id');
            $table->integer('priority');
            $table->string('iso_code');
            $table->string('title');
            $table->string('symbol');
            $table->string('subunit');
            $table->integer('subunit_to_unit');
            $table->boolean('symbol_first');
            $table->string('html_entity');
            $table->string('decimal_mark');
            $table->string('thousands_separator');
            $table->integer('iso_numeric');
            $table->timestamps();
        });

        Schema::create('countries', function (Blueprint $table) {
            $table->id('id');
            $table->integer('status')->default(0);
            $table->string('title', 52);
            $table->string('iso_2', 25);
            $table->string('iso_3', 10);
            $table->string('iso_on', 5);
            $table->unsignedBigInteger('currency_id');
            $table->timestamps();
        });

        Schema::create('services', function (Blueprint $table) {
            $table->id();
            $table->string('title'); 
            $table->text('description'); 
            $table->string('title_ar'); 
            $table->text('description_ar'); 
            $table->string('photo'); 
            $table->integer('status')->default(0);
            $table->float('price');
            $table->timestamps();
        });


        Schema::create('service_requests', function(Blueprint $table){
            $table->id(); 
            $table->string('title'); 
            $table->text('description'); 
            $table->integer('status')->default(0); 
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('service_id');
            $table->integer('type'); 
            $table->integer('priority'); 
            $table->string('lang'); 
            $table->timestamps();

        }); 


    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       
        Schema::dropIfExists('service_requests');
        Schema::dropIfExists('services');
        Schema::dropIfExists('currencies');
        Schema::dropIfExists('countries');
      
        
    }
}
