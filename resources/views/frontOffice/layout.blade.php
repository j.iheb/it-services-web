<!DOCTYPE html>
<html id="ctl00_HTMLTag" lang="en">

@yield('head')



<body class="inner-page">



<div 

@if ($lang == "en")
  dir ="ltr"
@else 
 dir="rtl"
 
@endif 

>

@yield('header')
@yield('content')


@include('frontOffice.inc.footer')
@include('frontOffice.inc.scripts')
{!! Toastr::message() !!}

</div>
</body>
</html>
