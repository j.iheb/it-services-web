
<div class="side-content-wrap">

    <div class="sidebar-left open" data-perfect-scrollbar data-suppress-scroll-x="true">
        <ul class="navigation-left navigation-main">
            
            <li class="nav-item @if($current == 'dashboard') active @endif">
                <a class="nav-item-hold" href="{{ route('showUserInstructorDashboard') }}" >
                    <i class="nav-icon i-Bar-Chart"></i>
                    <span class="nav-text">{{ trans('lang.dashboard') }}</span>
                </a>
       
            </li>
            <li class="nav-item @if($current == 'biography') active @endif">
                <a class="nav-item-hold" href="{{ route('showUserUpdateBiography') }}">
                    <i class="nav-icon i-Professor"></i>
                    <span class="nav-text">{{ trans('lang.biography') }}</span>
                </a>
             
            </li>
            <li class="nav-item @if($current == 'course-owned') active @endif" >
                <a class="nav-item-hold" href="{{ route('showUserOwnedCourses') }}">
                    <i class="nav-icon i-Open-Book"></i>
                    <span class="nav-text">{{ trans('lang.courses') }}</span>
                </a>

            </li>
            <li class="nav-item @if($current == 'course') active @endif" >
                <a class="nav-item-hold" href="{{ route('showUserAddCourse') }}">
                    <i class="nav-icon i-Add-Window"></i>
                    <span class="nav-text">{{ ((isset($courseId) and $courseId !== -1) ? trans('lang.edit') : trans('lang.new')) . ' ' . trans('lang.course') }}</span>
                </a>
              
            </li>
            <li class="nav-item @if($current == 'webinar-owned') active @endif">
                <a class="nav-item-hold" href="{{ route('showUserOwnedWebinars') }}">
                    <i class="nav-icon i-Speach-Bubble-Dialog"></i>
                    <span class="nav-text">{{ trans('lang.my_webinars') }} </span>
                </a>
      
            </li>

            <li class="nav-item @if($current == 'webinar') active @endif">
                <a class="nav-item-hold" href="{{ route('showUserAddWebinar') }}">
                    <i class="nav-icon i-Mail-Add-"></i>
                    <span class="nav-text">{{ ((isset($webinar) and $webinar !== -1) ? trans('lang.edit') : trans('lang.new')) . ' ' . trans('lang.webinar') }}</span>
                </a>
 
            </li>
        </ul>
    </div>

    <div class="sidebar-left-secondary" data-perfect-scrollbar data-suppress-scroll-x="true">
        <!-- Submenu Dashboards -->
        <ul class="childNav" data-parent="courses">
            <li class="nav-item">
                <a class="" href="{{ route('showManagerCoursePendingList') }}">
                    <i class="nav-icon i-Loop"></i>
                    <span class="item-name">{{ trans('lang.under_review') }}</span>
                </a>
            </li>

        </ul>
        <ul class="childNav" data-parent="courses">
            <li class="nav-item">
                <a class="" href="{{ route('showManagerCourseApprovedList') }}">
                    <i class="nav-icon i-Full-View-Window"></i>
                    <span class="item-name">{{ trans('lang.online') }}</span>
                </a>
            </li>

        </ul>
        <ul class="childNav" data-parent="webinars">
            <li class="nav-item">
                <a class="" href="{{ route('showManagerWebinarPendingList') }}">
                    <i class="nav-icon i-Loop"></i>
                    <span class="item-name">{{ trans('lang.under_review') }}</span>
                </a>
            </li>

        </ul>
        <ul class="childNav" data-parent="webinars">
            <li class="nav-item">
                <a class="" href="{{ route('showManagerWebinarApprovedList') }}">
                    <i class="nav-icon i-Full-View-Window"></i>
                    <span class="item-name">{{ trans('lang.online') }}</span>
                </a>
            </li>

        </ul>

    </div>

    <div class="sidebar-overlay"></div>
</div>



