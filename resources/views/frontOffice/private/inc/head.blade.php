
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSRF Token -->
    <title>{{ config('app.name') }} - Administration - {{ $title }}</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet">
    <script src="{{ asset('plugins/jquery/jquery-v4.js') }}"></script>

   
    <link rel="stylesheet" href="{{ asset('plugins/perfect-scrollbar/perfect-scrollbar.css') }}">

    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset ('plugins/toastr/toastr.css') }}">

    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet">


       <!-- CSRF Token -->
       <title>{{ config('app.name') }} {{ $title }}</title>
       <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet">
       
       <script src="{{ asset('plugins/jquery/jquery-v4.js') }}"></script>
     
     
       <link rel="stylesheet" href="{{ asset('plugins/perfect-scrollbar/perfect-scrollbar.css') }}">
   
       <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.js') }}"></script>
       <link rel="stylesheet" type="text/css" href="{{ asset ('plugins/toastr/toastr.css') }}">
   
       <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
       <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
       
       <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
   
       
   
       <link rel='stylesheet' id='bootstrap-css'  href='{{ asset('plugins/bootstrap/bootstrap.css') }}' type='text/css' media='all' />
       <link rel='stylesheet' id='magnific-popup-css'  href='{{ asset('plugins/magnific-popup/magnific-popup.css') }}' type='text/css' media='all' />
       <link rel='stylesheet' id='owl-carousel-css'  href='{{ asset('plugins/owl-carousel/owl.carousel.css') }}' type='text/css' media='all' />
       <link rel='stylesheet' id='owl-theme-default-css'  href='{{ asset('plugins/owl-carousel/owl.theme.css') }}' type='text/css' media='all' />
       <link rel='stylesheet' id='slick-css'  href='{{ asset('plugins/slick/slick.css') }}' type='text/css' media='all' />
       <link rel='stylesheet' id='fontawesome5-css'  href='{{ asset('icons/fontawesome.css') }}' type='text/css' media='all' />
       <link rel='stylesheet' id='fontawesome-css'  href='{{ asset('icons/font2.css') }}' type='text/css' media='all' />
   
       <link rel='stylesheet' id='fancybox-css'  href='{{ asset('plugins/toastr/toastr.css') }}' type='text/css' media='all' />
       <link rel='stylesheet' id='fancybox-css'  href='{{ asset('plugins/fancybox/fancybox.css') }}' type='text/css' media='all' />
       <link rel='stylesheet' id='lernen-fonts-css'  href='//fonts.googleapis.com/css?family=Poppins%3A300%2C400%2C500%2C600%2C700&#038;ver=1' type='text/css' media='all' />
       <link rel='stylesheet' id='lernen-white-custom-style-css'  href='{{ asset('css/frontOffice/custom.css') }}' type='text/css' media='all' />
       <link rel='stylesheet' id='lernen-color1-css'  href='{{ asset('css/frontOffice/colors.css') }}' type='text/css' media='all' />
   
       <link rel='stylesheet' id='learn-press-bundle-css'  href='{{ asset('css/frontOffice/bundle.css') }}' type='text/css' media='all' />
       <link rel='stylesheet' id='learn-press-css'  href='{{ asset('css/frontOffice/default.css') }}' type='text/css' media='all' />
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.min.css">
       <script src="{{ asset('plugins/jquery/jquery-v4.js') }}"></script>
       <script type='text/javascript' src='{{ asset('plugins/jquery/jquery-migrate.js') }}'></script>
       <script type='text/javascript' src='{{ asset('plugins/moment/moment.js') }}'></script>
   
       <script type='text/javascript' src='{{ asset('js/frontOffice/default.js') }}'></script>
       <script type='text/javascript' src='{{ asset('plugins/underscore/underscore.js') }}'></script>
       <link rel="stylesheet" href="{{ asset('css/backOffice/custom.css') }}">
       <link rel="stylesheet" href="{{ asset('css/backOffice/mystyle.css') }}">
       <script type='text/javascript'>
           /* <![CDATA[ */
           var lpGlobalSettings = {"url":"","siteurl":"","ajax":"","theme":"","localize":{"button_ok":"OK","button_cancel":"Cancel","button_yes":"Yes","button_no":"No"}};
           /* ]]> */
       </script>
   
       <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.js') }}"></script>
       <script src="{{ asset('plugins/validator/jquery.validate.js') }}"></script>
   
   
       <script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.js"></script>
       <meta name="csrf-token" content="{{ csrf_token() }}">

</head>

<script>
    $.validator.addMethod("fileSizeMax", function(value, element, param) {
        return this.optional(element) || (element.files[0].size <= param)
    }, $.validator.messages.fileSizeMax);

    $.validator.addMethod( "extension", function( value, element, param ) {
        param = typeof param === "string" ? param.replace( /,/g, "|" ) : "png|jpe?g|gif";
        return this.optional( element ) || value.match( new RegExp( "\\.(" + param + ")$", "i" ) );
    }, $.validator.messages.extension);

    $.validator.addMethod("dateGreaterOrEqual", function(value, element, params) {
        if (!/Invalid|NaN/.test(new Date(value))) {
            return new Date(value) >= new Date($(params).val());
        }
        return isNaN(value) && isNaN($(params).val()) || (Number(value) > Number($(params).val()));
    },$.validator.messages.dateGreaterOrEqual);

    $.validator.addMethod("maxBirthday", function(value) {
        var max = moment().subtract(15,'y').toDate();
        var inputDate = moment(value, "YYYY-MM-DD").toDate();
        if (inputDate < max)
            return true;
        return false;
    }, $.validator.messages.maxBirthday);
</script>

