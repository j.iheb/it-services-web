<div class="sidenav pull-right">
    <div style="border: 1px solid #CCCCCC;border-radius: 9px;text-align: center;padding-top: 32px;
height: 130px;">
        <a href="{{ route('showUserInstructorDashboard') }}"
           style="@if($active == 'dashboard')color:#C70039; @else color:#646464; @endif">
            <i class="fa fa-dashboard" style="font-size: 40px;"></i>
            <h6 style="@if($active == 'dashboard')color:#C70039; @else color:#646464; @endif">{{ trans('lang.dashboard') }}</h6>
        </a>
    </div>
    <div style="border: 1px solid #CCCCCC;border-radius: 9px;text-align: center;padding-top: 32px;margin-top:5px;
height: 130px;">
        <a href="{{ route('showUserUpdateBiography') }}"
           style="@if($active == 'biography')color:#C70039; @else color:#646464; @endif">

            <i class="fa fa-user" style="font-size: 40px;"></i>
            <h6 style="@if($active == 'biography')color:#C70039; @else color:#646464; @endif">{{ trans('lang.biography') }}</h6>
        </a>

    </div>
    <div style="border: 1px solid #CCCCCC;border-radius: 9px;text-align: center;padding-top: 32px;margin-top:5px;
height: 130px;">
        <a href="{{ route('showUserOwnedCourses') }}"
           style="@if($active == 'owned-courses')color:#C70039; @else color:#646464; @endif">

            <i class="fa fa-book" style="font-size: 40px;"></i>
            <h6 style="@if($active == 'owned-courses')color:#C70039; @else color:#646464; @endif">{{ trans('lang.my_courses') }}</h6>
        </a>
    </div>
    <div style="border: 1px solid #CCCCCC;border-radius: 9px;text-align: center;padding-top: 32px;margin-top:5px;
height: 130px;">
        <a href="{{ route('showUserAddCourse') }}"
           style="@if($active == 'course')color:#C70039; @else color:#646464; @endif">
            <i class="fa fa-plus" style="font-size: 40px;"></i>
            <h6 style="@if($active == 'course')color:#C70039; @else color:#646464; @endif">{{ ((isset($courseId) and $courseId !== -1) ? trans('lang.edit') : trans('lang.new')) . ' ' . trans('lang.course') }}</h6>
        </a>
    </div>
    <div style="border: 1px solid #CCCCCC;border-radius: 9px;text-align: center;padding-top: 32px;margin-top:5px;
height: 130px;">
        <a href="{{ route('showUserOwnedWebinars') }}"
           style="@if($active == 'owned-webinar')color:#C70039; @else color:#646464; @endif">

            <i class="fa fa-volume-up" style="font-size: 40px;"></i>
            <h6 style="@if($active == 'owned-webinar')color:#C70039; @else color:#646464; @endif">{{ trans('lang.my_webinars') }}</h6>
        </a>
    </div>
    <div style="border: 1px solid #CCCCCC;border-radius: 9px;text-align: center;padding-top: 32px;margin-top:5px;
height: 130px;">
        <a href="{{ route('showUserAddWebinar') }}"
           style="@if($active == 'webinar')color:#C70039; @else color:#646464; @endif">
            <i class="fa fa-plus" style="font-size: 40px;"></i>
            <h6 style="@if($active == 'webinar')color:#C70039; @else color:#646464; @endif">{{ ((isset($webinar) and $webinar !== -1) ? trans('lang.edit') : trans('lang.new')) . ' ' . trans('lang.webinar') }}</h6>
        </a>
    </div>
</div>
