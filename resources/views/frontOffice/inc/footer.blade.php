<footer>
            <div class="top-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-md-4">
                            <div class="footer-widget">
                                <h2 class="footer-widget-title"> {{ ucfirst(trans('lang.mughrabi_consulting'))}} </h2>
                                <div class="footer-text">
                                    <p>

                                    {{ ucfirst(trans('lang.footer_sentence')) }}
                                    
                                   </p>
                                    <ul>
                                        <li><a href="{{route('showHome',['lang'=> $lang])}}">    {{ ucfirst(trans('lang.home')) }} </a></li>
                                        <li><a href="{{route('showAbout',['lang'=> $lang])}}">     {{ ucfirst(trans('lang.about_us')) }} </a></li>
                                        <li><a href="{{route('showJoinUs',['lang'=> $lang])}}">     {{ ucfirst(trans('lang.join_us')) }} </a></li>
                                  
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4">
                            <div class="footer-widget">
                                <h2 class="footer-widget-title"> {{ ucfirst(trans('lang.contact_information')) }} </h2>
                                <div class="footer-text">
                                    <p class="MyFont">
                                     {{ucfirst(trans('lang.footer_sentence_two'))}}    
                                  </p>
                                </div>
                                <div class="social-media">
                                    <a href="{{App\Modules\General\Models\SocialMedia::find(1)->facebook}}" target="_blank"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="{{App\Modules\General\Models\SocialMedia::find(1)->youtube}}" target="_blank"><i class="fa fa-youtube"></i></a>
                                    <a href="#"><i class="fa fa-instagram"></i></a>
                                </div>
                              {{--   <div id="ctl00_EmailListUserAdd_Palen1" >
                                    <div id="ctl00_EmailListUserAdd_UpdatePanelEmail">
                                        <div id="ctl00_EmailListUserAdd_pnlEmailList" >
                                            <div class="maillist">
                                                <h3>القائمة البريدية</h3>
                                                <p>أضف بريدك الإلكتروني لدينا ليصلك جديدنا</p>
                                                <div style="display: flex; align-items: center; margin-top: 20px; width: 300px; max-width: 100%;">
                                                    <input name="ctl00$EmailListUserAdd$Txt_Email" type="text" id="ctl00_EmailListUserAdd_Txt_Email" placeholder="youremail@mail.com" />
                                                    <input type="submit" name="ctl00$EmailListUserAdd$btnEmailAdd" value="إضافة" 
                                                        id="ctl00_EmailListUserAdd_btnEmailAdd" style="width: 80px; height: 40px; text-align: center; background: #5893dd; color: #ffffff; transition: all 0.3s; outline: none; border: none; padding: 0;" />
                                                </div>
                                                <p class="MyFont">
                                                    <span id="ctl00_EmailListUserAdd_lblMsg" style="font-weight: bold;"></span>
                                                    <span id="ctl00_EmailListUserAdd_RequiredFieldValidator2" class="color-red" style="color:Red;display:none;"></span>
                                                    <span id="ctl00_EmailListUserAdd_RegularExpressionValidator1" class="color-red" style="color:Red;display:none;">الرجاء إدخال بريد إلكتروني صحيح</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div> --}}
                            </div>
                        </div>
                       {{--   <div class="col-xs-12 col-md-4">
                            <div class="footer-widget">
                                <h2 class="footer-widget-title">تابعنا على تويتر</h2>
                                <div class="twitter-widget">
                                    <div class="c-line-left hide"></div>
                                    <div id="ctl00_pnlTwitterAr">

                                        <a class="twitter-timeline" data-lang="ar" data-height="260" data-width="280" data-tweet-limit="1" data-theme="dark" data-link-color="#57C8EB" href="#" data-chrome="noheader nofooter noscrollbar noborders transparent">Tweets by Manara4Reportss</a>
                                   
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
            <div class="bottom-footer">
                <div class="container">
                    <div class="col-xs-12 col-md-5">
                        <div class="copyrights"> {{ucfirst(trans('lang.copyright_mail'))}}  </div>
                    </div>
                    <div class="col-xs-12 col-md-7">
                        <div class="footer-imgs">
                            <ul class="list-unstyled list-inline pull-left">
                                <li>
                                    <img src="https://cdn4.iconfinder.com/data/icons/simple-peyment-methods/512/paypal-512.png" style="width: 60px;" alt="We accept mada Pay" ></li>
                                <li>
                                    <img src="https://www.manaraa.com/images/visa.jpg" alt="We accept MasterCard" ></li>
                                <li>
                                    <img src="https://aux2.iconspalace.com/uploads/western-union-icon-256.png" style="width: 60px;" alt="We accept PayPal" ></li>
                           
                            </ul>
                        </div>
                    </div>
                </div>  
            </div>
        </footer>
      
  