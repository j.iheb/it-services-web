
<!DOCTYPE html>
<html lang="en">
@yield('head')


<body @if ($lang == 'ar') class="rtl" @endif>

    <div class="main-wrapper">
         <!-- side bar -->
 
          @yield('sidebar')

        <div class="page-wrapper">

            <!-- partial:partials/_navbar.html -->
            @yield('header')


            @yield('content')

        </div>
    </div>

    @include('backOffice.inc.footer')
    @include('backOffice.inc.scripts')
  
 <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.js') }}"></script>
 
 <script>
    @foreach ($errors->all() as $error)
    toastr.error('{{ $error }}');
    @endforeach
</script>



    {!! Toastr::message() !!}
</body>
</html>    
