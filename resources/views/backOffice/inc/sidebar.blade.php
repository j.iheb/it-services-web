       <!-- partial:partials/_sidebar.html -->
       <nav class="sidebar">
            <div class="sidebar-header">
                <a href="#" class="sidebar-brand">
                    Lo<span>go</span>
                </a>
                <div class="sidebar-toggler not-active">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
            <div class="sidebar-body">
                <ul class="nav">
                    <li class="nav-item nav-category">الرئيسية</li>
                    <li class="nav-item">
                        <a href="{{route('showManagerHome',['lang' => $lang])}}" class="nav-link">
                            <i class="link-icon" data-feather="box"></i>
                            <span class="link-title">لوحة التحكم</span>
                        </a>
                    </li>
                    <li class="nav-item nav-category"> أقسام</li>

                    <li class="nav-item">
                        <a href="{{route('showManagerUsers',['lang' => $lang])}}" class="nav-link">
                            <i class="link-icon" data-feather="users"></i>
                            <span class="link-title">{{ucfirst(trans('lang.users'))}}</span>
                        </a>
                    </li>


                   
                    <li class="nav-item">
                        <a href="{{route('showManagerContact',['lang' => $lang])}}" class="nav-link">
                            <i class="link-icon" data-feather="message-square"></i>
                            <span class="link-title">{{ucfirst(trans('lang.messages'))}}</span>
                        </a>
                    </li>
                 

                    <li class="nav-item">
                        <a href="{{route('showManagerUsers',['lang' => $lang])}}" class="nav-link">
                            <i class="link-icon" data-feather="unlock"></i>
                            <span class="link-title">{{ucfirst(trans('lang.roles'))}}</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{route('showManagerOrders',['lang' => $lang])}}" class="nav-link">
                            <i class="link-icon" data-feather="layers"></i>
                            <span class="link-title">{{ucfirst(trans('lang.orders'))}}</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{route('showManagerServices',['lang' => $lang])}}" class="nav-link">
                            <i class="link-icon" data-feather="clipboard"></i>
                            <span class="link-title">{{ucfirst(trans('lang.services'))}}</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{route('showManagerUsers',['lang' => $lang])}}" class="nav-link">
                            <i class="link-icon" data-feather="box"></i>
                            <span class="link-title">منتجات</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{route('showManagerNotes',['lang' => $lang])}}" class="nav-link">
                            <i class="link-icon" data-feather="check-circle"></i>
                            <span class="link-title">ملاحظات</span>
                        </a>
                    </li>

                 

                  




                  






























                </ul>
            </div>
        </nav>