

	<!-- core:js -->
	<script src="{{asset('js/backOffice/core.js')}}"></script>
	<!-- endinject -->
  <!-- plugin js for this page -->
  <script src="{{asset('js/backOffice/Chart.min.js')}}"></script>
  <script src="{{asset('js/backOffice/jquery.flot.js')}}"></script>
  <script src="{{asset('js/backOffice/jquery.flot.resize.js')}}"></script>
  <script src="{{asset('js/backOffice/bootstrap-datepicker.min.js')}}"></script>
  <script src="{{asset('js/backOffice/apexcharts.min.js')}}"></script>

  <script src="{{asset('js/backOffice/progressbar.min.js')}}"></script>
	<!-- end plugin js for this page -->
	<!-- inject:js -->
	<script src="{{asset('js/backOffice/feather.min.js')}}"></script>
	<script src="{{asset('js/backOffice/template.js')}}"></script>
	<!-- endinject -->
  <!-- custom js for this page -->
  <script src="{{asset('js/backOffice/dashboard.js')}}"></script>
  <script src="{{asset('js/backOffice/datepicker.js')}}"></script>
	<!-- end custom js for this page -->
    
   


  <script src="https://www.nobleui.com/html/template/assets/vendors/jquery.flot/jquery.flot.js"></script>



  
<script>
    @foreach ($errors->all() as $error)
    toastr.error('{{ $error }}');
    @endforeach
</script>


