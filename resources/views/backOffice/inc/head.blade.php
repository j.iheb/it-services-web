
    <head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title> CRM</title>
	<!-- core:css -->
	<link rel="stylesheet" href="{{asset('css/backOffice/core.css')}}">
	<!-- endinject -->
  <!-- plugin css for this page -->
	<link rel="stylesheet" href="{{asset('css/backOffice/bootstrap-datepicker.min.css')}}">
	<!-- end plugin css for this page -->
	<!-- inject:css -->
	<link rel="stylesheet" href="https://www.nobleui.com/html/template/assets/fonts/feather-font/css/iconfont.css">
	<link rel="stylesheet" href="https://www.nobleui.com/html/template/assets/vendors/flag-icon-css/css/flag-icon.min.css">
	<!-- endinject -->
  <!-- Layout styles -->  
	<link rel="stylesheet" href="{{asset('css/backOffice/style.css')}}">
  <!-- End layout styles -->
  <link rel="stylesheet" type="text/css" href="{{ asset ('plugins/toastr/toastr.css') }}">

  



</head>

    


