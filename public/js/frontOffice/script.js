(function($) {
    function doAnimations(elems) { var animEndEv = "webkitAnimationEnd animationend";
        elems.each(function() { var $this = $(this),
                $animationType = $this.data("animation");
            $this.addClass($animationType).one(animEndEv, function() { $this.removeClass($animationType); }); }); }
    var $myCarousel = $("#carouselExampleIndicators"),
        $firstAnimatingElems = $myCarousel.find(".carousel-item:first").find("[data-animation ^= 'animated']");
    $myCarousel.carousel();
    doAnimations($firstAnimatingElems);
    $myCarousel.on("slide.bs.carousel", function(e) { var $animatingElems = $(e.relatedTarget).find("[data-animation ^= 'animated']");
        doAnimations($animatingElems); });
})(jQuery);
new WOW().init();
$(window).scroll(function() { fixed_header(); });
fixed_header();

function fixed_header() { if ($(window).scrollTop() > 40) { $('.main-nav').addClass('fixed');
        $('body.inner-page').addClass('fixed'); } else { $('.main-nav').removeClass('fixed');
        $('body.inner-page').removeClass('fixed'); } }
if ($('.counter-item').length) { countDown(); }

function countDown() { var a = 0;
    $(window).scroll(function() { var oTop = $('.counter-item').offset().top - window.innerHeight; if (a == 0 && $(window).scrollTop() > oTop) { $('.counter-number').each(function() { var $this = $(this),
                    countTo = $this.attr('data-count');
                $({ countNum: $this.text() }).animate({ countNum: countTo }, { duration: 2000, easing: 'swing', step: function() { $this.text(Math.floor(this.countNum)); }, complete: function() { $this.text(this.countNum); } }); });
            a = 1; } }); }
$('.three-slide-carousel').owlCarousel({ loop: true, margin: 30, nav: false, dots: true, rtl: true, responsive: { 0: { items: 1 }, 550: { items: 2 }, 768: { items: 2 }, 1000: { items: 3 }, } });
$('.single-slide-carousel').owlCarousel({ loop: true, margin: 0, nav: false, dots: true, rtl: true, items: 1, autoplay: true });
$('.testimonials-carousel').owlCarousel({ loop: true, margin: 0, nav: false, dots: true, rtl: true, items: 1 });
$(document).on('click', '.menu-close', function(e) { e.preventDefault();
    $('.navbar-content').removeClass('active'); });
$(document).on('click', '.menu-open', function(e) { e.preventDefault();
    $('.navbar-content').addClass('active'); });
if ($('.testimonial-other-item p').length) { $('.testimonial-other-item p').mCustomScrollbar(); }
$(document).on('click', '.faq-item a', function(e) { e.preventDefault();
    $(this).toggleClass('active');
    $(this).closest('.faq-item').toggleClass('active');
    $(this).closest('.faq-item').find('.content').slideToggle(); });
$(document).on('click', '.faq-ul a', function(e) { var $this = $(this); var $data_to = $(this).attr('data-to');
    e.preventDefault(); if (!$this.hasClass('active')) { $('.faq-ul a').removeClass('active');
        $this.addClass('active');
        $('.faq-items .faq-item').hide(); if ($data_to == 'all') { $('.faq-items .faq-item').fadeIn(); } else { $('.faq-items .faq-item[data-cat=' + $data_to + ']').fadeIn(); } } });
$(document).on('click', '.load-more-services', function(e) { e.preventDefault(); var $this = $(this);
    $this.addClass('disabled');
    $('.loader-wrapper').show();
    $.ajax({ url: "more-services.html", success: function(result) { $('.loader-wrapper').hide();
            setTimeout(function() { $(".other-services-wrap").append(result);
                $("html, body").animate({ scrollTop: ($(window).scrollTop() + 1) });
                $this.removeClass('disabled'); }, 5000); } }); });
$(document).on('change', '.urgent-order input', function() { if ($('#yes').is(':checked')) { $('.urgent-order-hint').show(); } else { $('.urgent-order-hint').hide(); } })