var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http, {
    cors: {
        origin: "*"
    }
});

var mysql = require('mysql');
var moment = require('moment');
const { Socket } = require('dgram');
var sockets = {};
var con = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'consulting-web'
});


con.connect(function(err) {
    if (err)
        throw err;
    console.log("Database Connected");
})


io.on('connection', function(socket) {
    if (!sockets[socket.handshake.query.user_id]) {
        sockets[socket.handshake.query.user_id] = [];
    }
    sockets[socket.handshake.query.user_id].push(socket);

    socket.broadcast.emit('user_connected', socket.handshake.query.user_id);

    con.query('UPDATE users SET status=1 WHERE id=' + socket.handshake.query.user_id + '', function(err, res) {
        if (err)
            throw err;
        console.log("User Connected", socket.handshake.query.user_id);
    });



    socket.on('disconnect', function(err) {
        socket.broadcast.emit('user_disconnected', socket.handshake.query.user_id);
        for (var index in sockets[socket.handshake.query.user_id]) {
            if (socket.id == sockets[socket.handshake.query.user_id][index].id) {
                sockets[socket.handshake.query.user_id].splice(index, 1);

            }
        }

        con.query('UPDATE users Set status=0 where id=' + socket.handshake.query.user_id + '', function(err, res) {
            if (err)
                throw err;
            console.log("User Disconnected", socket.handshake.query.user_id);
        });
    });
});


http.listen(3000);